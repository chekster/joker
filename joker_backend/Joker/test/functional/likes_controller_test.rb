require 'test_helper'

class LikesControllerTest < ActionController::TestCase
  setup do
    @like = likes(:one)
    @likee = likes(:three)
    @user = User.where(:guser => 'chekst@gmail.com').first
    if !@user.token.nil?
      @user.token.destroy
    end
    @user.create_token()
    @token = @user.token.token
    #session[:token] = 'sdfsdfsdfsdfsgfdsgdfgdfg'
  end

  test "should get index" do
    get :index, :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:likes)
  end

  test "should get new" do
    get :new, :format => 'json', token: @token
    assert_response :success
  end

  test "should create like" do
    assert_difference('Like.count') do
      post :create, like: { like: true, voice_id: 980190962, guser: @user.guser }, :format => 'json', token: @token
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( nil, obj['user_spam'])
    assert_not_equal( nil, obj['user_like'])
    assert_not_equal( nil, obj['spams'])
    assert_response :success
    #assert_redirected_to like_path(assigns(:like))
  end

  test "should update existing like on post" do
    assert_no_difference('Like.count') do
      post :create, like: { like: true, voice_id: 298486374, guser: @user.guser }, :format => 'json', token: @token
    end

    assert_response :success
  end

  test "should show like" do
    get :show, id: @like , :format => 'json', token: @token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @like, :format => 'json', token: @token
    assert_response :success
  end

  test "should update like" do
    put :update, id: @like, like: { like: @like.like, user_id: @like.user_id, voice_id: @like.voice_id }, :format => 'json', token: @token
    assert_response :success
    #assert_redirected_to like_path(assigns(:like))
  end

  test "should destroy like" do
    voice = Voice.find(@likee.voice_id)
    assert_equal( 2, voice.likes)

    assert_difference('Like.count', -1) do
      delete :destroy, id: @likee, :format => 'json', token: @token
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( 1, obj['likes'])
    assert_equal( nil, obj['user_like'])
    assert_equal( nil, obj['user_spam'])
    assert_not_equal( nil, obj['spams'])
    #assert_redirected_to likes_path
    assert_response :success
  end

  test "should destroy like as admin" do
    session[:user_id] = @user.id
    session[:role] = 'admin'
    assert_difference('Like.count', -1) do
      delete :destroy, id: @like, :format => 'html'
    end

    assert_redirected_to likes_path
  end

  test "should raise RuntimeError 'wrong user' on like destroy " do
    exception = assert_raise(RuntimeError) {
      delete :destroy, id: @like, :format => 'json', :guser => "guser2", token: @token
    }
    assert_equal 'destroy like, wrong user', exception.message
  end

  test "should raise RuntimeError 'like not found' on like destroy " do
    exception = assert_raise(ActiveRecord::RecordNotFound) {
      delete :destroy, id: 1, :format => 'json', :guser => "guser2", token: @token
    }
    assert_equal "Couldn't find Like with id=1", exception.message
  end
end
