require 'test_helper'

class AppsControllerTest < ActionController::TestCase
  setup do
    @app = apps(:one)
    @user = User.where(:guser => 'chekst@gmail.com').first
    if !@user.token.nil?
      @user.token.destroy
    end
    @user.create_token()
    @token = @user.token.token
    session[:token] = 'dsfsdgsdgdsfgdf'
  end

  test "should get index" do
    get :index, :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:apps)
  end

  test "should get new" do
    get :new, :format => 'json', token: @token
    assert_response :success
  end

  test "should create app" do
    session[:user_id] = @user.id
    assert_difference('App.count') do
      post :create, app: { client_id: @app.client_id, client_secret: @app.client_secret, system: @app.system, time: @app.time }, :format => 'html'
    end

    assert_redirected_to app_path(assigns(:app))
  end

  test "should show app" do
    get :show, id: @app, :format => 'json', token: @token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @app, :format => 'json', token: @token

    assert_response :success
  end

  test "should update app" do
    session[:user_id] = @user.id
    put :update, id: @app, app: { client_id: @app.client_id, client_secret: @app.client_secret, system: @app.system, time: @app.time }, :format => 'html'
    assert_redirected_to app_path(assigns(:app))
  end

=begin
  test "should destroy app" do
    assert_difference('App.count', -1) do
      delete :destroy, id: @app
    end

    assert_redirected_to apps_path
  end
=end
end
