require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  setup do
    @tag = tags(:one)
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      user.token.destroy
    end
    user.create_token()
    @token = user.token.token
  end

  test "should get index" do
    get :index, :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:tags)
  end

  test "should get new" do
    get :new, :format => 'json', token: @token
    assert_response :success
  end

  test "should create tag" do
    assert_difference('Tag.count') do
      post :create, tag: { tag: 'two' }, :format => 'json', token: @token
    end

    assert_response :success
    #assert_redirected_to tag_path(assigns(:tag))
  end

  test "should show tag" do
    get :show, id: @tag, :format => 'json', token: @token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tag, :format => 'json', token: @token
    assert_response :success
  end

  test "should update tag" do
    put :update, id: @tag, tag: { tag: @tag.tag }, :format => 'json', token: @token
    assert_response :success
    #assert_redirected_to tag_path(assigns(:tag))
  end

  test "should destroy tag" do
    assert_difference('Tag.count', -1) do
      delete :destroy, id: @tag, token: @token, :format => 'json'
    end

    assert_response :success
    #assert_redirected_to tags_path
  end

  test "should find 2 tags" do
    get :find, :findtag => 'jok' , :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:tags)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( 2, obj.length)

  end
end
