require 'test_helper'
require 'json'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
    @user2 = users(:two)
    @user3 = users(:three)
    @token_u2 = tokens(:one)
    @usere = users(:four)
    @unregistered_user = users(:unregistered)
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      user.token.destroy
    end
    user.create_token()
    @token = user.token.token
    session[:token] = 'sdfsdfsdfsdfsgfdsgdfgdfg'
  end

  test "should get index" do
    get :index, token: @token, :format => 'json'

    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new, token: @token, :format => 'json'

    assert_response :success
  end

  test "should create enabled user" do
    assert_difference('User.count') do
      post :create, user: { guser: 'sfsd@gmail.com', language: 'ru', name: 'dfgfg', password: 'dfdfgrgdf' }, token: @token, :format => 'json'

    end
    user = User.where(:guser => 'sfsd@gmail.com').first
    assert_not_equal( nil, user)
    assert_equal( true, user.enabled)
    assert_response :success
    secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
    a = ActiveSupport::MessageEncryptor.new(secret)
    decrypted_password = a.decrypt(user.password)
    assert_equal( decrypted_password, 'dfdfgrgdf')
    #assert_redirected_to user_path(assigns(:user))
  end

  test "should init session parameters and create token" do
    app_account = AppAccount
      .order('last_usage asc')
      .where("max_time > time + 30")
      .first

    assert_difference('Token.count', 1) do
      post :init_session, guser: @usere.guser, password: 'qwertygg', :format => 'json'
    end


    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['system'])
    assert_not_equal( nil, obj['client_id'])
    assert_not_equal( nil, obj['client_secret'])
    assert_not_equal( nil, obj['login'])
    assert_not_equal( nil, obj['password'])
    assert_not_equal( nil, obj['app_account_id'])
    app_account_id = obj['app_account_id']
    app_account = AppAccount.find(app_account_id)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_response :success
    assert_equal( app_account.id, obj['app_account_id'])

  end

  test "should not init session parameters and not create token" do
    assert_difference('Token.count', 0) do
      post :init_session, guser: @usere.guser, password: 'aaaaaaaaa', :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( false, obj['result'])
    assert_equal( nil, obj['token'])
    assert_response :success
  end

  test "should init session parameters and rewrite token" do
    app_account = AppAccount
      .order('last_usage asc')
      .where("max_time > time + 30")
      .first

    assert_difference('Token.count', 0) do
      post :init_session, guser: @user2.guser, password: 'qwertygg', :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['system'])
    assert_not_equal( nil, obj['client_id'])
    assert_not_equal( nil, obj['client_secret'])
    assert_not_equal( nil, obj['login'])
    assert_not_equal( nil, obj['password'])
    assert_not_equal( nil, obj['app_account_id'])
    app_account_id = obj['app_account_id']
    app_account = AppAccount.find(app_account_id)
    #time_diff = ((Time.new - app_account.last_usage) / 1.second).round
    #assert_equal( true, time_diff < 5)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_not_equal( @token_u2.token, obj['token'])
    assert_response :success
    assert_equal( app_account.id, obj['app_account_id'])
  end

  test "should init session parameters and rewrite token with version param" do
    app_account = AppAccount
      .order('last_usage asc')
      .where("max_time > time + 30")
      .first

    assert_difference('Token.count', 0) do
      post :init_session, guser: @user2.guser, password: 'qwertygg', :v => 9, :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['num_arr'])
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_not_equal( @token_u2.token, obj['token'])
    assert_response :success
  end

  test "should init session parameters on existing token" do
    app_account = AppAccount
      .order('last_usage asc')
      .where("max_time > time + 30")
      .first

    assert_difference('Token.count', 0) do
      post :init_session, guser: @user2.guser, token: @token_u2.token, :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['system'])
    assert_not_equal( nil, obj['client_id'])
    assert_not_equal( nil, obj['client_secret'])
    assert_not_equal( nil, obj['login'])
    assert_not_equal( nil, obj['password'])
    assert_not_equal( nil, obj['app_account_id'])
    app_account_id = obj['app_account_id']
    app_account = AppAccount.find(app_account_id)
    #time_diff = ((Time.new - app_account.last_usage) / 1.second).round
    #assert_equal( true, time_diff < 5)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_equal( @token_u2.token, obj['token'])
    assert_response :success
    assert_equal( app_account.id, obj['app_account_id'])
  end

  test "should not init session parameters on not existing token for user have no token" do
    assert_difference('Token.count', 0) do
      post :init_session, guser: @user.guser, token: 'sdfsdfsdfsdf', :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( false, obj['result'])
    assert_equal( nil, obj['token'])
    assert_response :success
  end

  test "should not init session parameters on not existing token" do
    assert_difference('Token.count', 0) do
      post :init_session, guser: @user2.guser, token: 'aaaaaaaaaa', :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( false, obj['result'])
    assert_equal( nil, obj['token'])
    assert_response :success
  end

  test "should show user" do
    get :show, id: @user, token: @token, :format => 'json'

    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user, token: @token, :format => 'json'

    assert_response :success
  end

  test "should update user" do
    o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
    name  =  (0...6).map{ o[rand(o.length)] }.join
    lang = ['en', 'ru', 'he'].sample
    put :update, id: 0, name: name, language: lang, token: @token_u2.token, :format => 'json'
    user = User.find(@token_u2.user_id)
    assert_response :success
    assert_equal( user.name, name)
    assert_equal( user.language, lang)
    #assert_redirected_to user_path(assigns(:user))
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @usere, token: @token, :format => 'json'
    end
=begin
    delete :destroy, id: @user
    @user = User.find(@user.id)
    assert_equal( false, @user.enabled)

=end
    assert_response :success
    #assert_redirected_to users_path
  end

  test "should not logout" do
    get :logout, token: @token, :format => 'json'
    assert_not_equal( nil, @token)
  end

  test "should logout" do
    get :logout, token: @token, :format => 'json'
    @token = nil
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      @token = user.token.token
    end
    assert_equal( nil, @token)
  end

  test "should not login" do
    @token = nil
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      user.token.destroy
    end
    get :login, :format => 'json'
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      @token = user.token.token
    end
    assert_equal( nil, @token)
  end

  test "should render login form" do
    get :login_form
    assert_response :success
  end

  test "should not render login form" do
    get :login_form, :format => 'json'
    assert_response 406
  end

  test "should not login with wrong user" do
    @token = nil
    get :login, :user => {:guser => 'chel.com', :password => 'qwty'}
    user = User.where(:guser => 'chel.com').first
    if !user.nil?
      if !user.token.nil?
        @token = user.token.token
      end
    end
    assert_equal( nil, @token)
  end

  test "should login" do
    @token = nil
    post :login, :user => {:guser => 'chekst@gmail.com', :password => 'qwerty'}
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.nil?
      if !user.token.nil?
        #TODO how to check session variables?
        @token = user.token.token
      end
    end
    assert_not_equal( nil, @token)
  end

  test "should change password" do
    post :change_password, token: @token_u2.token, oldpassword: 'qwertygg', newpassword: 'dsafsdf', :format => 'json'

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    user2 = User.where(:guser => 'guser2').first
    assert_not_equal( user2.password, @user2.password)
    assert_response :success
  end

  test "should create temporary password and hash for pass recovery" do
    #assert_equal( @user3.tmp_password, nil)
    #assert_equal( @user3.password_recovery_hash, nil)
    post :forgot_password, guser: @user3.guser, :format => 'json'

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    user = User.where(:guser => @user3.guser).first
    assert_not_equal( user.tmp_password, nil)
    assert_not_equal( user.password_recovery_hash, nil)
    assert_response :success
  end

  test "should insert tmp pass into real pass" do
    assert_not_equal( @usere.password, @usere.tmp_password)
    post :restore_password, id: @usere.id, h: @usere.password_recovery_hash, :format => 'html'

    user = User.where(:guser => @usere.guser).first
    assert_equal( user.password, @usere.tmp_password)
    assert_response :success
  end

  test "should not insert tmp pass into real pass" do
    assert_not_equal( @usere.password, @usere.tmp_password)
    post :restore_password, id: @usere.id, h: 'dsfcsfdfssvsdv', :format => 'html'

    user = User.where(:guser => @usere.guser).first
    assert_not_equal( user.password, @usere.tmp_password)
    assert_response :success
  end

  test "should update bot user to enabled" do
    post :create, user: { guser: 'guser5', language: 'ru', name: 'dfgfg', password: 'dfdfgrgdf' }, :format => 'json'
    user = User.where(:guser => 'guser5').first
    assert_not_equal( nil, user)
    assert_equal( true, user.enabled)
    assert_equal( 'dfgfg', user.name)
    assert_response :success
    secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
    a = ActiveSupport::MessageEncryptor.new(secret)
    decrypted_password = a.decrypt(user.password)
    assert_equal( decrypted_password, 'dfdfgrgdf')
  end

  test "should init session parameters and create token for bot user" do

    assert_difference('Token.count', 1) do
      post :init_session, guser: @unregistered_user.guser, :v => 10, :wor => 1, :format => 'json'
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['num_arr'])

    auth_json_str = obj['num_arr'].pack('C*').force_encoding('utf-8')
    auth_json = JSON.parse(auth_json_str)
    assert_not_equal( nil, auth_json["system"])
    assert_not_equal( nil, auth_json['client_id'])
    assert_not_equal( nil, auth_json['app_account_id'])
    assert_equal( nil, auth_json['client_secret'])
    assert_equal( nil, auth_json['login'])
    assert_equal( nil, auth_json['password'])
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_not_equal( nil, obj['language'])
    assert_response :success

    u = User.find(@unregistered_user.id)
    assert_equal( false, u.token.trusted)

  end

  test "should create bot user and token for him" do
    assert_difference('User.count') do
     post :init_session, guser: 'tesdfdgfdfgt@gmail.com', language: 'ru', :wor => 1, :v => 10, :format => 'json'
    end

    user = User.where(:guser => 'tesdfdgfdfgt@gmail.com').first
    assert_not_equal( nil, user)
    assert_equal( true, user.enabled)
    assert_equal( 'botbot', user.name)
    assert_equal( 'botbot', user.password)
    assert_equal( false, user.token.trusted)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['num_arr'])
    auth_json_str = obj['num_arr'].pack('C*').force_encoding('utf-8')
    auth_json = JSON.parse(auth_json_str)
    assert_not_equal( nil, auth_json["system"])
    assert_not_equal( nil, auth_json['client_id'])
    assert_not_equal( nil, auth_json['app_account_id'])
    assert_equal( nil, auth_json['client_secret'])
    assert_equal( nil, auth_json['login'])
    assert_equal( nil, auth_json['password'])
    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_not_equal( nil, obj['language'])
    assert_response :success
  end

  test "should init session parameters and create token with version" do
    app_account = AppAccount
      .order('last_usage asc')
      .where("max_time > time + 30")
      .first

    assert_difference('Token.count', 1) do
      post :init_session, guser: @usere.guser, password: 'qwertygg', :v => 10, :format => 'json'
    end

    user = User.find(@usere.id)
    assert_equal( true, user.token.trusted)


    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['num_arr'])
    auth_json_str = obj['num_arr'].pack('C*').force_encoding('utf-8')
    auth_json = JSON.parse(auth_json_str)
    assert_not_equal( nil, auth_json["system"])
    assert_not_equal( nil, auth_json['client_id'])
    assert_not_equal( nil, auth_json['app_account_id'])
    assert_not_equal( nil, auth_json['client_secret'])
    assert_not_equal( nil, auth_json['login'])
    assert_not_equal( nil, auth_json['password'])


    assert_equal( nil, obj['system'])
    assert_equal( nil, obj['client_id'])
    assert_equal( nil, obj['client_secret'])
    assert_equal( nil, obj['login'])
    assert_equal( nil, obj['password'])
    assert_equal( nil, obj['app_account_id'])
    app_account_id = auth_json['app_account_id']
    app_account = AppAccount.find(app_account_id)
    assert_not_equal( nil, obj['result'])
    assert_equal( true, obj['result'])
    assert_not_equal( nil, obj['token'])
    assert_response :success
    assert_equal( app_account.id, auth_json['app_account_id'])

  end

  test "should not register registered user" do
    assert_no_difference('User.count') do
      post :create, user: { guser: @user.guser, language: @user.language, name: 'dfgfg', password: 'dfdfgrgdf' }, :format => 'json'
    end
    assert_response :conflict
  end

  test "should not register bot user" do
    assert_no_difference('User.count') do
      post :create, user: { guser: @unregistered_user.guser, language: @unregistered_user.language, name: 'dfgfg', password: 'dfdfgrgdf' }, :format => 'json'
    end
    assert_response :success
  end

end
