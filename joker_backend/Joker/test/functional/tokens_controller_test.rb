require 'test_helper'

class TokensControllerTest < ActionController::TestCase
=begin
  setup do
    @token = tokens(:one)
    session[:token] = 'dfgdfgdfgfddfgdfg'
  end

  test "should get index" do
    get :index, :format => 'json', token: session[:token]
    assert_response :success
    assert_not_nil assigns(:tokens)
  end

  test "should get new" do
    get :new, :format => 'json', token: session[:token]
    assert_response :success
  end

  test "should create token" do
    #TODO only from init_session create token
    assert_difference('Token.count') do

      post :create, token: {user_id: 980190962 }, :format => 'json', token: session[:token]
    end

    assert_response :success
    #assert_redirected_to token_path(assigns(:token))
  end

  test "should show token" do
    get :show, id: @token, :format => 'json', token: session[:token]
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @token, :format => 'json', token: session[:token]
    assert_response :success
  end

  test "should update token" do
    put :update, id: @token, token: { token: @token.token, user_id: @token.user_id }, :format => 'json', token: session[:token]
    assert_response(422)
    #assert_redirected_to token_path(assigns(:token))
  end

  test "should destroy token" do
    assert_difference('Token.count', -1) do
      delete :destroy, id: @token
    end

    assert_redirected_to tokens_path
  end
=end
end
