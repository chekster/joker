require 'test_helper'

class VoicesControllerTest < ActionController::TestCase
  setup do
    @voice = voices(:one)
    @voice2 = voices(:two)
    @tag = tags(:one)
    @voice_without_likes_and_spams = voices(:four)
    user = User.where(:guser => 'chekst@gmail.com').first
    if !user.token.nil?
      user.token.destroy
    end
    user.create_token()
    @token = user.token.token

    user2 = User.where(:guser => 'guser2').first
    if !user2.token.nil?
      user2.token.destroy
    end
    user2.create_token()
    @token2 = user2.token.token

  end

  test "should get my voices" do
    get :my_voices, token: @token2, :format => 'json'
    assert_response :success
    assert_equal( 3, assigns(:voices).count)
    assert_not_nil assigns(:voices)
    assigns(:voices).each do |voice|
      assert_not_nil voice.spams
    end
  end

  test "should get index" do
    get :index, token: @token, :format => 'json'
    assert_response :success
    assigns(:voices).each do |voice|
      if voice.id == 298486374
        assert_not_nil voice.user_like
        assert_nil voice.user_spam
      end
      if voice.id == 980190962
        assert_nil voice.user_like
        assert_nil voice.user_spam
      end
    end
    assert_equal( 4, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_equal( 4, obj.length)
  end

  test "should get index with rand sorting" do
    get :index, token: @token, sort: 'rand', v: '15', :format => 'json'
    assert_response :success
    assigns(:voices).each do |voice|
      if voice.id == 298486374
        assert_not_nil voice.user_like
        assert_nil voice.user_spam
      end
      if voice.id == 980190962
        assert_nil voice.user_like
        assert_nil voice.user_spam
      end
    end
    assert_equal( 4, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end

  test "should get new" do
    get :new, token: @token, :format => 'json'
    assert_response :success
  end

  test "should create voice" do
    assert_difference('Voice.count') do
      post :create, voice: {system: 'soundcloud', url: 'sdfvdgdfgdg', app_account_id: 298486374, link: 'sdfgdfgdfg'}, token: @token, :format => 'json'
    end

    #assert_redirected_to voice_path(assigns(:voice))
    assert_response :success
  end

  test "should show voice" do
    get :show, id: @voice, token: @token, :format => 'json'
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @voice, token: @token, :format => 'json'
    assert_response :success
  end

  test "should update voice" do
    put :update, id: @voice, voice: { enabled: @voice.enabled }, token: @token, :format => 'json'
    #put :update, id: @voice, voice: { dislikes: @voice.dislikes, enabled: @voice.enabled, likes: @voice.likes, likes_sum: @voice.likes_sum, system: @voice.system, url: @voice.url, user_id: @voice.user_id }
    assert_response :success
    #assert_redirected_to voice_path(assigns(:voice))
  end

  test "should destroy voice" do
    delete :destroy, id: @voice_without_likes_and_spams, token: @token2, :format => 'json'
    v = Voice.find(@voice_without_likes_and_spams.id)
    assert_equal( true, v.deleted)
    assert_response :success
  end

  test "should not destroy voice" do
    delete :destroy, id: @voice_without_likes_and_spams, token: @token, :format => 'json'
    v = Voice.find(@voice_without_likes_and_spams.id)
    assert_equal( false, v.deleted)
    assert_response :success
  end

  test "should add tag to voice" do
    assert_equal( @voice2.tags.length, 1)
    assert_equal( Tag.count, 4)
    tags = ''
    @voice2.tags.each{|tag|
      tags = tags + tag.tag + ','
    }
    tags = tags + @tag.tag + ','
    post :update_tags, id: @voice2.id, tags: tags, token: @token, :format => 'json'
    v = Voice.find(@voice2.id)
    assert_equal( v.tags.length, 2)
    assert_equal( Tag.count, 4)
    assert_response :success
  end

  test "should create tag and add to voice" do
    assert_equal( @voice2.tags.length, 1)
    assert_equal( Tag.count, 4)
    tags = ''
    @voice2.tags.each{|tag|
      tags = tags + tag.tag + ','
    }
    tags = tags + 'hui' + ','
    post :update_tags, id: @voice2.id, tags: tags, token: @token, :format => 'json'
    v = Voice.find(@voice2.id)
    assert_equal( v.tags.length, 2)
    assert_equal( Tag.count, 5)
    assert_response :success
  end

  test "should get index with rand sorting and tags" do
    tags = 'test'
    get :index, token: @token, sort: 'rand', tags: tags, v: '15', :format => 'json'
    assert_response :success
    assert_equal( 1, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end

  test "should get index with like sorting" do
    get :index, token: @token, sort: 'like', v: '15', :format => 'json'
    assert_response :success
    assert_equal( 1, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_equal( 1, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end

  test "should get index with like sorting and tags" do
    tags = 'joke'
    get :index, token: @token, sort: 'like', tags: tags, v: '15', :format => 'json'
    assert_response :success
    assert_equal( 1, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_equal( 1, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end

  test "should get index with rand sorting and offset" do
    get :index, token: @token, sort: 'rand', v: '15', from: 2, :format => 'json'
    assert_response :success
    assert_equal( 2, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_equal( 0, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end

  test "should get index with rand sorting and tags and offset" do
    tags = 'joke'
    get :index, token: @token, sort: 'rand', tags: tags, v: '15', from: 2, :format => 'json'
    assert_response :success
    assert_equal( 2, assigns(:voices).count)
    assert_not_nil assigns(:voices)

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['count'])
    assert_equal( 0, obj['count'])
    assert_not_equal( nil, obj['voices'])
  end
end
