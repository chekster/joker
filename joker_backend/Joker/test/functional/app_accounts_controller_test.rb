require 'test_helper'

class AppAccountsControllerTest < ActionController::TestCase
  setup do
    @app_account = app_accounts(:one)
    @user = User.where(:guser => 'chekst@gmail.com').first
    if !@user.token.nil?
      @user.token.destroy
    end
    @user.create_token()
    @token = @user.token.token
    session[:token] = 'dfgdfgdfgfddfgdfg'
  end

  test "should get index" do
    get :index, :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:app_accounts)
  end

  test "should get new" do
    get :new, :format => 'json', token: @token
    assert_response :success
  end

  test "should create app_account" do
    session[:user_id] = @user.id
    assert_difference('AppAccount.count') do
      post :create, app_account: { app_id: @app_account.app_id, login: @app_account.login, password: @app_account.password, time: @app_account.time, last_usage: Time.new }, :format => 'html'
    end

    assert_redirected_to app_account_path(assigns(:app_account))
  end

  test "should show app_account" do
    get :show, id: @app_account, :format => 'json', token: @token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @app_account, :format => 'json', token: @token
    assert_response :success
  end

=begin
#TODO check why it not work
  test "should update app_account" do
    put :update, id: @app_account, app_account: { app_id: @app_account.app_id, login: @app_account.login, password: @app_account.password, time: @app_account.time }
    assert_redirected_to app_account_path(assigns(:app_account))
  end
=end

  test "should destroy app_account" do
    session[:user_id] = @user.id
    assert_difference('AppAccount.count', -1) do
      delete :destroy, id: @app_account, :format => 'html'
    end

    assert_redirected_to app_accounts_path
  end
end
