require 'test_helper'

class SpamsControllerTest < ActionController::TestCase
  setup do
    @spam = spams(:two)
    @spame = spams(:three)
    @user = User.where(:guser => 'chekst@gmail.com').first
    if !@user.token.nil?
      @user.token.destroy
    end
    @user.create_token()
    @token = @user.token.token
    session[:token] = 'sdfsdfsdfsdfsgfdsgdfgdfg'
  end

  test "should get index" do
    get :index, :format => 'json', token: @token
    assert_response :success
    assert_not_nil assigns(:spams)
  end

  test "should get new" do
    get :new, :format => 'json', token: @token
    assert_response :success
  end

  test "should create spam" do
    assert_difference('Spam.count') do
      post :create, spam: { voice_id: @spam.voice_id }, :format => 'json', token: @token
    end

    obj = ActiveSupport::JSON.decode(response.body)
    assert_not_equal( nil, obj['user_spam'])
    assert_not_equal( nil, obj['user_like'])
    assert_not_equal( nil, obj['spams'])
    #assert_redirected_to spam_path(assigns(:spam))
    assert_response :success
  end

  test "should show spam" do
    get :show, id: @spam, :format => 'json', token: @token
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @spam, :format => 'json', token: @token
    assert_response :success
  end

  test "should update spam" do
    put :update, id: @spam, spam: { user_id: @spam.user_id, voice_id: @spam.voice_id }, :format => 'json', token: @token
    assert_response :success
    #assert_redirected_to spam_path(assigns(:spam))
  end

  test "should destroy spam" do
    assert_difference('Spam.count', -1) do
      delete :destroy, id: @spame, :format => 'json', token: @token
    end

    obj = ActiveSupport::JSON.decode(response.body)
    #assert_not_equal( nil, obj['user_like'])
    assert_equal( nil, obj['user_spam'])
    assert_equal( nil, obj['user_like'])
    assert_not_equal( nil, obj['spams'])
    #assert_redirected_to spams_path
    assert_response :success
  end

  test "should destroy spam as admin" do
    session[:user_id] = @user.id
    session[:role] = 'admin'
    assert_difference('Spam.count', -1) do
      delete :destroy, id: @spam, :format => 'html'
    end

    assert_redirected_to spams_path
  end

  test "should raise RuntimeError 'wrong user' on spam destroy " do
    exception = assert_raise(RuntimeError) {
      delete :destroy, id: @spam, :format => 'json', :guser => "guser2", token: @token
    }
    assert_equal 'destroy spam, wrong user', exception.message
  end

  test "should raise RuntimeError 'spam not found' on spam destroy " do
    exception = assert_raise(ActiveRecord::RecordNotFound) {
      delete :destroy, id: 1, :format => 'json', :guser => "guser2", token: @token
    }
    assert_equal "Couldn't find Spam with id=1", exception.message
  end
end
