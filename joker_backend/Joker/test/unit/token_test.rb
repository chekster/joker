require 'test_helper'

class TokenTest < ActiveSupport::TestCase

  test "should not save token without user_id" do
    token = Token.new
    token.token = 'asd'
    assert !token.save, 'Saved token without user_id'
  end

  test "should create and save new token" do
    token = Token.new
    token.user_id = 980190962
    assert token.save, 'Not saved token'
    assert_not_equal( token.token, nil, 'Token not created' )
    assert_equal( token.trusted, true, 'trusted is false' )
  end

  test "should not save token for user with existing token" do
    token = Token.new
    token.user_id = 298486374
    token.token = 'asd'
    assert !token.save, 'Saved token for user with existing token'
  end

  test "should not save token for not existing user" do
    token = Token.new
    token.user_id = 2
    token.token = 'asd'
    assert !token.save, 'Saved token for not existing user'
  end

  test "should save token" do
    token = Token.new
    token.token = 'asd'
    token.user_id = 980190962
    assert token.save, 'Token not saved'
    t = Token.find(token.id)
    assert_equal( t.trusted, true, 'trusted is false' )
  end

  test "should save not trusted token" do
    token = Token.new
    token.token = 'asd'
    token.trusted = false
    token.user_id = 980190962
    assert token.save, 'Token not saved'
    t = Token.find(token.id)
    assert_equal( t.trusted, false, 'trusted is true' )
  end

end
