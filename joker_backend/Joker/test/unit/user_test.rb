require 'test_helper'

class UserTest < ActiveSupport::TestCase

  setup do
    @user = users(:one)
    @unregistered_user = users(:unregistered)
  end

  test "should not save user without guser" do
    user = User.new
    user.name = 'asd'
    user.password = 'asddfgdfg'
    user.language = 'ru'
    assert !user.save, 'Saved user without guser'
  end

  test "should not save user with same guser" do
    user = User.new
    user.name = 'asd'
    user.password = 'asddfgdfg'
    user.guser = 'guser1'
    user.language = 'ru'
    assert !user.save, 'Saved user with existing guser'
  end

  test "should not save user without password" do
    user = User.new
    user.name = 'asd'
    user.guser = 'guser5'
    user.language = 'ru'
    assert !user.save, 'Saved user without password'
  end

  test "should not save user without name" do
    user = User.new
    user.password = 'asddfgdfg'
    user.guser = 'guser6'
    user.language = 'ru'
    assert !user.save, 'Saved user without name'
  end

  test "should not save user with short name" do
    user = User.new
    user.name = 'a'
    user.password = 'asddfgdfg'
    user.guser = 'guser6'
    user.language = 'ru'
    assert !user.save, 'Saved user with short name'
  end

  test "should not save user with long name" do
    user = User.new
    user.name = 'acdfbgdfg,lsdkgldkfjgldfkjgldsfgdgdfgdfgdfgdf'
    user.password = 'asddfgdfg'
    user.guser = 'guser6'
    user.language = 'ru'
    assert !user.save, 'Saved user with long name'
  end

  test "should not save user with short password" do
    user = User.new
    user.name = 'asdfsd'
    user.password = 'as'
    user.guser = 'guser6'
    user.language = 'ru'
    assert !user.save, 'Saved user with short password'
  end

  test "should save user " do
    user = User.new
    user.name = 'asdfsd'
    user.password = 'asdgjdf'
    user.guser = 'guser16'
    user.language = 'ru'
    assert user.save, 'User not saved'
  end

  test "login should return true and not create token for html " do
    assert_equal( @user.token, nil)
    assert @user.login('qwertygg','html'), 'User not logged in'
    assert_equal( @user.token, nil)
  end

  test "login should return true and create token for json " do
    assert_equal( @user.token, nil)
    assert @user.login('qwertygg','json'), 'User not logged in'
    assert_not_equal( @user.token, nil)
  end

  test "login should return false" do
    assert !@user.login('qwerty','html'), 'User logged in'
  end

  test "is_unregistered function should return user status" do
    assert @unregistered_user.is_unregistered, 'User unregistered'
    assert !@user.is_unregistered, 'User registered'
  end

end
