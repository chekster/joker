require 'test_helper'

class SpamTest < ActiveSupport::TestCase

=begin
  test "should create new like for correct spam report" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.user_id = 980190962
    likes_count = Like.all.size
    spam.save
    assert_equal( Like.all.size, likes_count + 1, 'Like not created' )
  end
=end

=begin
  test "should change existing like to dislike" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.user_id = 980190962
    spam.save
    assert_equal( Like.find(298486374).like, false, 'Like not changed' )
  end
=end

  test "should save correct spam report with guser" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.guser = 'guser1'
    likes_count = Like.all.size
    assert spam.save, 'Not saved correct spam report'
  end

  test "should not save spam report with no existing guser" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.guser = 'guser3'
    assert !spam.save, 'Saved spam report with no existing guser'
  end

  test "should not save spam report with no existing user_id" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.user_id = 3
    assert !spam.save, 'Saved spam report with no existing user_id'
  end

  test "should not save spam report with no existing voice_id" do
    spam = Spam.new
    spam.voice_id = 2
    spam.guser = 'guser2'
    #assert !spam.save, 'Saved spam report with no existing voice_id'
    assert_raise( ActiveRecord::InvalidForeignKey ) { spam.save }
    #assert_raise( ActiveRecord::RecordNotFound ) { spam.save }
  end

=begin
  test "should increase dislikes count for voice" do
    spam = Spam.new
    spam.guser = 'guser1'
    spam.voice_id = 298486374
    spam.save
    assert_equal( 1, Voice.find(298486374).dislikes, 'Dislike count not increased' )
  end
=end

  test "should save correct spam report" do
    spam = Spam.new
    spam.voice_id = 298486374
    spam.user_id = 980190962
    assert spam.save, 'Not saved correct spam report'
  end

  test "should not save existing spam report" do
    spam = Spam.new
    spam.user_id = 298486374
    spam.voice_id = 980190962
    assert_raise( ActiveRecord::RecordNotUnique ) { spam.save }
  end

end
