require 'test_helper'

class TagTest < ActiveSupport::TestCase

  test "should not save empty tag" do
    tag = Tag.new
    assert !tag.save, 'Saved empty tag'
  end

  test "should save tag with tag" do
    tag = Tag.new
    tag.tag = 'dfgdfgdf'
    assert tag.save, 'Saved empty tag with tag'
  end

  test "should not save tag with existing tag" do
    tag = Tag.new
    tag.tag = 'joke'
    assert !tag.save, 'Saved tag with existing tag'
  end

  test "should not save tag with uppercase" do
    tag = Tag.new
    tag.tag = 'DWBHJ'
    tag.save
    assert_equal( 'dwbhj', tag.tag, 'Saved tag with uppercase' )
  end

end
