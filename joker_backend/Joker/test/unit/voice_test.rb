require 'test_helper'

class VoiceTest < ActiveSupport::TestCase

  test "should not save empty voice" do
    voice = Voice.new
    assert !voice.save, 'Saved empty voice'
  end

  test "should not save voice without user id" do
    voice = Voice.new
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice without user id'
  end

  test "should not save voice without url" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice without url'
  end

  test "should not save voice without system" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.url = 'url1'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice without system'
  end

  test "should not save voice without duration" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.url = 'url1'
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice without duration'
  end

  test "should not save voice without size" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.url = 'url1'
    voice.duration = 5
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice without size'
  end

  test "should not save voice with not existing user id" do
    voice = Voice.new
    voice.user_id = 1
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice with not existing user id'
  end

  test "should not save voice with not existing system" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'wrewrweter'
    voice.url = 'http://soundcloud.com'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice with not existing system'
  end


  test "should not save voice with existing url" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'url1'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice with existing url'
  end

  test "should not save voice with no existing guser and  user_id" do
    voice = Voice.new
    voice.user_id = 1
    voice.guser = 'guser3'
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice with no existing guser and user_id'
  end

  test "should not save voice with no existing guser" do
    voice = Voice.new
    voice.guser = 'guser3'
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert !voice.save, 'Saved voice with no existing guser'
  end

  test "should not save voice without app_account_id" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    assert_raise( ActiveRecord::InvalidForeignKey ) { voice.save }
  end

  test "should not save voice with no existing app_account_id" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 1
    assert_raise( ActiveRecord::InvalidForeignKey ) { voice.save }
  end

  test "should save voice with existing user id" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert voice.save, 'Not saved voice with existing user id'
  end

  test "should save voice with existing guser" do
    voice = Voice.new
    voice.guser = 'guser1'
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    assert voice.save, 'Not saved voice with existing guser'
  end

  test "should not push two same tags to voice" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.com'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    voice.save
    voice.tags.push Tag.where(:tag => 'joke').first
    assert_raise( ActiveRecord::RecordNotUnique ) { voice.tags.push Tag.where(:tag => 'joke').first }
  end

  test "should save new tags passed in voice" do
    all_tags = Tag.all
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.comr'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    tags = Array.new(2) { Hash.new }
    tags[0]['tag'] = 'joke'
    tags[1]['tag'] = 'felix'
    voice.tags_access = tags
    voice.save
    assert_equal( Tag.all.size, all_tags.size + 1, 'New tag not saved' )
  end

  test "should remove tag from voice if it not passed when update" do
    voice = Voice.find(980190962)
    voice_tags_count = voice.tags.size
    tags = Array.new(1) { Hash.new }
    tags[0]['tag'] = 'joke'
    voice.tags_access = tags
    voice.duration = 5
    voice.size = 0.004
    voice.save
    assert_equal( voice.tags.size, voice_tags_count - 1, 'Tag not removed' )
  end

  test "should set language to voice from user" do
    user = User.find(298486374)
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.comr'
    voice.link = 'http://soundcloud.comr'
    tags = Array.new(1) { Hash.new }
    tags[0]['tag'] = 'joke'
    voice.tags_access = tags
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    voice.save
    assert_equal( voice.language, user.language, 'Language not setted from user' )
  end

  test "should increase AppAcount and App time for voice duration" do
    app_account_time = AppAccount.find(298486374).time
    app_time = AppAccount.find(298486374).app.time
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.url = 'http://soundcloud.comr'
    voice.link = 'http://soundcloud.comr'
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    voice.save
    assert_equal( app_account_time + 5, AppAccount.find(298486374).time, 'AppAcount time not increased' )
    assert_equal( app_time + 5, AppAccount.find(298486374).app.time, 'App time not increased' )
  end

  test "should decrease AppAcount and App time for voice duration" do
    app_account_time = AppAccount.find(298486374).time
    app_time = AppAccount.find(298486374).app.time
    voice = voices(:four)
    duration = voice.duration
    voice.destroy
    assert_equal( app_account_time - duration, AppAccount.find(298486374).time, 'AppAcount time not decreased' )
    assert_equal( app_time - duration, AppAccount.find(298486374).app.time, 'App time not decreased' )
  end

  test "should not set voice language from user" do
    voice = Voice.new
    voice.user_id = 298486374
    voice.system = 'soundcloud'
    voice.language = 'ru'
    voice.url = 'http://soundcloud.comr'
    tags = Array.new(1) { Hash.new }
    tags[0]['tag'] = 'joke'
    voice.tags_access = tags
    voice.duration = 5
    voice.size = 0.004
    voice.app_account_id = 298486374
    voice.save
    assert_equal( voice.language, 'ru', 'Language setted from user' )
  end

  test "user_spam should be nil, user_like should be false" do
    voice = Voice.find(980190962)
    user = User.find(980190962)
    voice.calc_current_user_votes(user)
    assert_equal( voice.user_spam.nil?, true, 'user_spam not nil' )
    assert_equal( voice.user_like.like, false, 'user_like not false' )
  end

  test "user_spam should not be nil, user_like should be nil" do
    voice = Voice.find(980190962)
    user = User.find(298486374)
    voice.calc_current_user_votes(user)
    assert_equal( voice.user_spam.nil?, false, 'user_spam nil' )
    assert_equal( voice.user_like.nil?, true, 'user_like not nil' )
  end

  test "spams should be nil until calc_spams" do
    voice = Voice.find(980190962)
    assert_equal( voice.spams.nil?, true, 'spams not nil' )
    voice.calc_spams
    assert_equal( voice.spams.nil?, false, 'spams nil' )
  end

end
