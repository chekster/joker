require 'test_helper'

class LikeTest < ActiveSupport::TestCase

  test "should save correct like" do
    like = Like.new
    like.user_id = 298486374
    like.voice_id = 980190962
    like.like = true
    assert like.save, 'Not saved correct like'
  end

  test "should save correct like with guser" do
    like = Like.new
    like.guser = 'guser1'
    like.voice_id = 298486374
    like.like = true
    assert like.save, 'Not saved correct like'
  end

  test "should not save existing like with guser" do
    like = Like.new
    like.guser = 'guser2'
    like.voice_id = 298486374
    like.like = true
    assert_raise( ActiveRecord::RecordNotUnique ) { like.save }
  end

  test "should not save like with no existing guser" do
    like = Like.new
    like.guser = 'guser3'
    like.voice_id = 298486374
    like.like = true
    assert !like.save, 'Saved like with no existing guser'
  end

  test "should not save like with no existing user_id" do
    like = Like.new
    like.user_id = 3
    like.voice_id = 298486374
    like.like = true
    assert !like.save, 'Saved like with no existing user_id'
  end

  test "should not save like with no existing voice_id" do
    like = Like.new
    like.guser = 'guser2'
    like.voice_id = 2
    like.like = true
    assert_raise( ActiveRecord::InvalidForeignKey ) { like.save }
    #assert_raise( ActiveRecord::RecordNotFound ) { like.save }
  end

  test "should not save existing like" do
    like = Like.new
    like.user_id = 298486374
    like.voice_id = 298486374
    like.like = true
    assert_raise( ActiveRecord::RecordNotUnique ) { like.save }
  end

  test "should not update user_id for like" do
    like = Like.find(980190962)
    like.user_id = 980190962
    like.save
    assert_equal( like.user_id, 298486374, 'User id updated for like' )
  end

  test "should not update voice_id for like" do
    like = Like.find(980190962)
    like.voice_id = 980190962
    like.save
    assert_equal( like.voice_id, 298486374, 'Voice id updated for like' )
  end

  test "should incrise voice likes" do
    like = Like.new
    like.guser = 'guser1'
    like.voice_id = 298486374
    like.like = true
    voice = Voice.find(298486374)
    voice_likes = voice.likes
    like.save
    voice1 = Voice.find(298486374)
    assert_equal( voice1.likes, voice_likes + 1, 'Voice likes not incrised' )
  end

  test "should decrise voice dislikes and incrise likes_sum" do
    like = Like.find(298486374)
    voice_id = like.voice_id
    voice_likes_sum = Voice.find(like.voice_id).likes_sum
    voice_dislikes = Voice.find(like.voice_id).dislikes
    like.destroy
    voice_likes_sum1 = Voice.find(voice_id).likes_sum
    voice_dislikes1 = Voice.find(voice_id).dislikes
    assert_equal( voice_dislikes, voice_dislikes1 + 1, 'Voice dislikes not decrised' )
    assert_equal( voice_likes_sum, voice_likes_sum1 - 1, 'Voice likes_sum not incrised' )
  end

  test "should decrise voice likes and decrise likes_sum" do
    like = Like.find(980190962)
    voice_id = like.voice_id
    voice_likes_sum = Voice.find(like.voice_id).likes_sum
    voice_likes = Voice.find(like.voice_id).likes
    like.destroy
    voice_likes_sum1 = Voice.find(voice_id).likes_sum
    voice_likes1 = Voice.find(voice_id).likes
    assert_equal( voice_likes, voice_likes1 + 1, 'Voice likes not decrised' )
    assert_equal( voice_likes_sum, voice_likes_sum1 + 1, 'Voice likes_sum not decrised' )
  end

end
