# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130118133840) do

  create_table "app_accounts", :force => true do |t|
    t.string   "login",                     :null => false
    t.string   "password",                  :null => false
    t.integer  "app_id",                    :null => false
    t.integer  "time",       :default => 0, :null => false
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "max_time",   :default => 0, :null => false
    t.datetime "last_usage",                :null => false
  end

  add_index "app_accounts", ["app_id"], :name => "app_accounts_app_id_fk"

  create_table "apps", :force => true do |t|
    t.string   "system",        :default => "soundcloud", :null => false
    t.string   "client_id",                               :null => false
    t.string   "client_secret",                           :null => false
    t.integer  "time",          :default => 0,            :null => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
  end

  create_table "likes", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "voice_id",   :null => false
    t.boolean  "like",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "likes", ["user_id"], :name => "user_key"
  add_index "likes", ["voice_id", "user_id"], :name => "voice_id", :unique => true

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "spams", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "voice_id",   :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "spams", ["user_id"], :name => "spams_ibfk_1"
  add_index "spams", ["voice_id", "user_id"], :name => "voice_id", :unique => true

  create_table "tags", :force => true do |t|
    t.string   "tag"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tags", ["tag"], :name => "tag", :unique => true

  create_table "tags_voices", :force => true do |t|
    t.integer "tag_id",   :null => false
    t.integer "voice_id", :null => false
  end

  add_index "tags_voices", ["tag_id"], :name => "tag"
  add_index "tags_voices", ["voice_id", "tag_id"], :name => "voice_id", :unique => true

  create_table "tokens", :force => true do |t|
    t.string   "token"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.boolean  "trusted",    :default => true,  :null => false
  end

  add_index "tokens", ["user_id"], :name => "tokens_user_id_fk", :unique => true

  create_table "users", :force => true do |t|
    t.string   "guser"
    t.datetime "created_at"
    t.datetime "updated_at",                   :null => false
    t.string   "language",                     :null => false
    t.boolean  "enabled",    :default => true, :null => false
    t.string   "password",                     :null => false
    t.string   "name",                         :null => false
    t.string   "tmp_password"
    t.string   "password_recovery_hash"
  end

  add_index "users", ["guser"], :name => "guser", :unique => true

  create_table "voices", :force => true do |t|
    t.string   "url"
    t.string   "system"
    t.integer  "user_id",                                        :null => false
    t.integer  "likes",                       :default => 0,     :null => false
    t.integer  "dislikes",                    :default => 0,     :null => false
    t.integer  "likes_sum",                   :default => 0,     :null => false
    t.boolean  "enabled",                     :default => true,  :null => false
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.string   "language",                                       :null => false
    t.integer  "duration",       :limit => 2, :default => 0,     :null => false
    t.float    "size",                        :default => 0.0,   :null => false
    t.integer  "app_account_id",              :default => 1,     :null => false
    t.boolean  "deleted",                     :default => false, :null => false
    t.boolean  "stream_deleted",              :default => false, :null => false
    t.string   "link"
    t.boolean  "downloaded",                  :default => false,  :null => false
  end

  add_index "voices", ["app_account_id"], :name => "voices_app_account_id_fk"
  add_index "voices", ["url"], :name => "url", :unique => true
  add_index "voices", ["user_id"], :name => "voices_user_id_fk"
  add_index "voices", ["link"], :name => "link", :unique => true

  add_foreign_key "app_accounts", "apps", :name => "app_accounts_app_id_fk"

  add_foreign_key "likes", "users", :name => "likes_user_id_fk"
  add_foreign_key "likes", "voices", :name => "likes_voice_id_fk"

  add_foreign_key "spams", "users", :name => "spams_user_id_fk"
  add_foreign_key "spams", "voices", :name => "spams_voice_id_fk"

  add_foreign_key "tags_voices", "tags", :name => "tag", :dependent => :delete
  add_foreign_key "tags_voices", "voices", :name => "voice", :dependent => :delete

  add_foreign_key "tokens", "users", :name => "tokens_user_id_fk"

  add_foreign_key "voices", "app_accounts", :name => "voices_app_account_id_fk"
  add_foreign_key "voices", "users", :name => "voices_user_id_fk"

end
