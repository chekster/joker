class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :check_token

  def check_token
    #puts 'check token'
    #Rails.logger.info 'qqq' #for server
    #Rails.logger.info params[:controller].to_s
    #Rails.logger.info params[:action].to_s
    #Rails.logger.info 'check_token url ' + request.url
    #puts 'check_token format ' + request.format # local
    if !params[:role].nil?
      params[:role] = nil
    end
    if request.format == 'html'
      if !session[:user_id].nil?
      else
        redirect_to '/users/login_form'
      end
    elsif request.format == 'json'
      if params[:token].nil?
        #raise ActionController::RoutingError.new('Not Found')
        #TODO give here right message and code
        render :text => "404 Not Found", :status => 404
      else
        token = Token.where(:token => params[:token]).first
        if token.nil?
          render :text => "404 Not Found", :status => 404
        else
          user = User.find(token.user_id)
          if user.nil?
            render :text => "404 Not Found", :status => 404
          else
            params[:guser] = user.guser
            params[:user] = user
          end
        end
      end
      if params[:controller] == 'tokens'
        render :text => "404 Not Found", :status => 404
      end
    else
      render :text => "404 Not Found", :status => 404
    end
  end

end
