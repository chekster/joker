class LikesController < ApplicationController
  # GET /likes
  # GET /likes.json
  def index
    @likes = Like.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @likes }
    end
  end

  # GET /likes/1
  # GET /likes/1.json
  def show
    @like = Like.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @like }
    end
  end

  # GET /likes/new
  # GET /likes/new.json
  def new
    @like = Like.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @like }
    end
  end

  # GET /likes/1/edit
  def edit
    @like = Like.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @like }
    end
  end

  # POST /likes
  # POST /likes.json
  def create
    user = params[:user]
    found_like = Like.where(:user_id => user.id, :voice_id => params[:like][:voice_id]).first
    if found_like.nil?
      @like = Like.new(params[:like])
      @like.user_id = user.id
    else
      @like = found_like
    end
    @like.like = params[:like][:like]

    respond_to do |format|
      if @like.save
        voice = Voice.find(params[:like][:voice_id])
        voice.calc_current_user_votes(user)
        voice.calc_spams
        format.html { redirect_to @like, notice: 'Like was successfully created.' }
        #format.json { render json: @like, status: :created, location: @like }
        format.json { render json: voice.to_json(methods: [:user_like,:user_spam,:spams]), status: :created, location: @like }
      else
        format.html { render action: "new" }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /likes/1
  # PUT /likes/1.json
  def update
    @like = Like.find(params[:id])

    respond_to do |format|
      if @like.update_attributes(params[:like])
        format.html { redirect_to @like, notice: 'Like was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /likes/1
  # DELETE /likes/1.json
  def destroy
    @like = Like.find(params[:id])
    ok_for_destroy = false
    voice_id = @like.voice_id
    if request.format == 'html'
      if session[:role] != 'admin'
        raise "destroy like, wrong user"
      else
        ok_for_destroy = true
      end
    elsif request.format == 'json'
      user = params[:user]
      if user.id == @like.user_id
        ok_for_destroy = true
      else
        raise "destroy like, wrong user"
      end
    else
    end

    if ok_for_destroy
      @like.destroy
      voice = Voice.find(voice_id)
    end

    if request.format == 'json' and ok_for_destroy
      voice.calc_current_user_votes(user)
      voice.calc_spams
    end

    respond_to do |format|
      format.html { redirect_to likes_url }
      #format.json { head :no_content }
      format.json { render json: voice.to_json(methods: [:user_like,:user_spam,:spams]), status: :ok }
    end
  end
end
