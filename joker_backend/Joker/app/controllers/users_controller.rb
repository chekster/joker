class UsersController < ApplicationController
  skip_before_filter :check_token, :only => [:create, :init_session, :login_form, :login, :forgot_password, :restore_password]

  # GET /forgot_password
  def restore_password
    #TODO add to view Joker image with css
    @result = false
    if !params[:id].nil?
      @user = User.find(params[:id])
      if !@user.nil?
        if @user.password_recovery_hash == params[:h]
          @user.password = @user.tmp_password
          @user.save
          @result = true
          secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
          c = ActiveSupport::MessageEncryptor.new(secret)
          @password = c.decrypt(@user.tmp_password)
        end
      end
    end
    render action: "restore_password"
  end

  # POST /forgot_password.json
  def forgot_password
    result = false
    if !params[:guser].nil?
      user = User.where(:guser => params[:guser].downcase).first
      if !user.nil?
        cs = [*'0'..'9', *'a'..'z', *'A'..'Z']
        pass = 8.times.map { cs.sample }.join
        secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
        a = ActiveSupport::MessageEncryptor.new(secret)
        user.tmp_password = a.encrypt(pass)
        uuid = UUID.new
        user.password_recovery_hash = uuid.generate
        user.save
        UserMailer.forgot_password_email(user).deliver
        result = true
      end
    end
    respond_to do |format|
      if result
        format.html { redirect_to users_path }
        format.json { render json: {'result' => result} }
      else
        format.html { render action: "login_form" }
        format.json { render json: {'result' => result} }
      end
    end
  end

  # POST /change_password
  # POST /change_password.json
  def change_password
    if !params[:guser].nil?
      @user = User.where(:guser => params[:guser].downcase).first
    end
    result = false
    if !@user.nil?
      if !params[:oldpassword].nil?
        secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
        c = ActiveSupport::MessageEncryptor.new(secret)
        decrypted_password = c.decrypt(@user.password)
        #puts 'pass = ' + @user.password
        #puts 'dec pass = ' + decrypted_password
        if params[:oldpassword] == decrypted_password
          encrypted_password = c.encrypt(params[:newpassword])
          #puts 'new pass = ' + params[:newpassword]
          #puts 'new enc pass = ' + encrypted_password
          @user.password = encrypted_password
          result = @user.save
        else
          @user.errors.add(:password, 'User incorrect password')
        end
      else
        @user.errors.add(:password, 'User password is empty')
      end
    else
      @user = User.new
      @user.errors.add(:guser, 'User not found')
    end
    respond_to do |format|
      if result
        format.json { render json: {'result' => result} }
        format.html { redirect_to users_path }
      else
        format.html { render action: "login_form" }
        format.json { render json: {'result' => result} }
      end
    end
  end

  #TODO logout
  # GET /users/logout
  def logout
    result = false
    if request.format == 'html'
      if !session[:user_id].nil?
        session[:user_id] = nil
        #session[:token] = nil
        result = true
      end
    else
      #user = User.where(:guser => params[:guser]).first
      user = params[:user]
      if !user.token.nil?
        user.token.destroy
        result = true
      end
    end

    respond_to do |format|
      format.html { redirect_to users_path }
      format.json { render json: {'result' => result} }
    end
  end

  # POST /users/login
  def login
    result = false
    if request.format == 'html'
      if !params[:user][:guser].nil?
        if params[:user][:guser].downcase == 'chekst@gmail.com'
          user = User.where(:guser => params[:user][:guser].downcase).first
          if !user.nil?
            if !params[:user][:password].nil?
              result = user.login(params[:user][:password], request.format)
              if result
                session[:user_id] = user.id
                session[:role] = 'admin'
              end
            else
              user.errors.add(:password, 'User not authorized')
            end
          end
        end
      end
    end

    respond_to do |format|
      if result
        format.html { redirect_to users_path }
      else
        @user = User.new
        format.html { render action: "login_form" }
      end
    end
  end

  # GET /users/login
  # GET /users/login.json
  def login_form
    @user = User.new
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /init_session
  # POST /init_session.json
  def init_session
    if !params[:guser].nil?
      @user = User.where(:guser => params[:guser].downcase).first
    end
    result = false
    system = false
    user_registered = false
    #TODO check csrf ajax security and when use sessin store
    #Check if user exist and not bot
    if !@user.nil? and params[:wor].nil?
      if !params[:password].nil?
        result = @user.login(params[:password], request.format)
        user_registered = true
      elsif !params[:token].nil?
        if !@user.token.nil?
          if @user.token.token == params[:token] and @user.token.trusted
            result = true
            if !@user.is_unregistered
              user_registered = true
            end
          else
            @user.errors.add(:token, 'User not authorized')
          end
        else
          @user.errors.add(:token, 'User not authorized')
        end
      else
        @user.errors.add(:password, 'User not authorized')
        @user.errors.add(:token, 'User not authorized')
      end
    else
      #Init session for unregistered user
      #Only if it json
      if request.format == 'json' and !params[:wor].nil?
        #Create bot user (not registred)
        if @user.nil?
          #@user = User.new(params[:user])
          @user = User.new
          @user.guser = params[:guser]
          @user.language = params[:language]
          @user.password = 'botbot'
          @user.name = 'botbot'
          @user.enabled = true
          if @user.save
            #Create for him token
            #TODO add to token trusted field, to separate athorized logins and not athorized
            @user.create_token(:trusted => false)
            result = true
          end
        else
          if !@user.token.nil?
            @user.token.destroy
          end
          @user.create_token(:trusted => false)
          result = true
        end
      end
    end
    if result
      account_time = 7200 #max size of uploading in SC
      app = nil
      app_account = nil

      app_account = AppAccount
        .order('last_usage asc')
        .where("max_time > time + 30")
        .first
      #TODO get app_account exectly in the moment of recording
      app = App.find(app_account.app_id)

      if !app_account.nil?
        system = true
      end
    end
    if system and !params[:v].nil?
      if user_registered
        data_for_enc = { :system => app.system,
                          :client_id => app.client_id,
                          :client_secret => app.client_secret,
                          :login => app_account.login,
                          :password => app_account.password,
                          :app_account_id => app_account.id}.to_json.to_s
      else
         data_for_enc = { :system => app.system,
                          :client_id => app.client_id,
                          :app_account_id => app_account.id}.to_json.to_s
      end
      secure_bytes = data_for_enc.bytes
    end
    respond_to do |format|
      if result and system
        if !params[:v].nil?
          if user_registered
            format.json { render json: {'token' => @user.token.token,
                                        'name' => @user.name,
                                        'language' => @user.language,
                                        'result' => result,

                                        'num_arr' => secure_bytes} }
          else
            format.json { render json: {'token' => @user.token.token,
                                        'language' => @user.language,
                                        'result' => result,

                                        'num_arr' => secure_bytes} }
          end
        else
          format.json { render json: {'system' => app.system,
                                      'client_id' => app.client_id,
                                      'client_secret' => app.client_secret,
                                      'login' => app_account.login,
                                      'password' => app_account.password,
                                      'app_account_id' => app_account.id,

                                      'token' => @user.token.token,
                                      'name' => @user.name,
                                      'language' => @user.language,
                                      'result' => result} }
        end
        format.html { redirect_to users_path }
      else
        format.html { render action: "login_form" }
        format.json { render json: {'result' => result} }
      end
    end
  end

  #Get app_account and app, with most free space
  def get_most_free_app_account(account_time)
    app = App.order('time ASC').first
    app_account = nil
    app.app_accounts.each do |account|
      if account_time > (account.time - 30) # 30 sec max size of 1 record
        app_account = account
        account_time = account.time
      end
    end
    return app, app_account
  end

  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @like }
    end
  end

  # POST /users
  # POST /users.json
  def create
    result = false
    if !params[:user][:guser].nil?
      @user = User.where(:guser => params[:user][:guser].downcase).first
    end
    if @user.nil? or @user.name == 'botbot'
      if @user.nil?
        @user = User.new(params[:user])
      else
        @user.name = params[:user][:name]
        @user.language = params[:user][:language]
      end
      secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
      a = ActiveSupport::MessageEncryptor.new(secret)
      encrypted_password = a.encrypt(params[:user][:password])
      @user.password = encrypted_password
      @user.enabled = true
      if @user.save
        result = true
      end
    end

    respond_to do |format|
      if result
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        if @user.nil?
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        else
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :conflict }
        end
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    result = true
    if !params[:guser].nil? and request.format == 'json'
      @user = User.where(:guser => params[:guser].downcase).first
      if !params[:name].nil?
        @user.name = params[:name]
      end
      if !params[:language].nil?
        @user.language = params[:language]
      end
      result = @user.save
    elsif !params[:id].nil? and request.format == 'html'
      @user = User.find(params[:id])
      result = @user.update_attributes(params[:user])
    end
    if @user.nil? and result
      @user = User.new
      @user.errors.add(:guser, 'User not found')
      result = false
    end

    respond_to do |format|
      if result
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])

    @user.destroy
    #@user.enabled = false
    #@user.save

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

end
