class AppAccountsController < ApplicationController
  # GET /app_accounts
  # GET /app_accounts.json
  def index
    @app_accounts = AppAccount.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @app_accounts }
    end
  end

  # GET /app_accounts/1
  # GET /app_accounts/1.json
  def show
    @app_account = AppAccount.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @app_account }
    end
  end

  # GET /app_accounts/new
  # GET /app_accounts/new.json
  def new
    @app_account = AppAccount.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @app_account }
    end
  end

  # GET /app_accounts/1/edit
  def edit
    @app_account = AppAccount.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @app_account }
    end
  end

  # POST /app_accounts
  # POST /app_accounts.json
  def create
    @app_account = AppAccount.new(params[:app_account])
    @app_account.last_usage = Time.new

    respond_to do |format|
      if @app_account.save
        format.html { redirect_to @app_account, notice: 'App account was successfully created.' }
        format.json { render json: @app_account, status: :created, location: @app_account }
      else
        format.html { render action: "new" }
        format.json { render json: @app_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /app_accounts/1
  # PUT /app_accounts/1.json
  def update
    @app_account = AppAccount.find(params[:id])

    respond_to do |format|
      if @app_account.update_attributes(params[:app_account])
        format.html { redirect_to @app_account, notice: 'App account was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @app_account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app_accounts/1
  # DELETE /app_accounts/1.json
  def destroy
    @app_account = AppAccount.find(params[:id])
    @app_account.destroy

    respond_to do |format|
      format.html { redirect_to app_accounts_url }
      format.json { head :no_content }
    end
  end
end
