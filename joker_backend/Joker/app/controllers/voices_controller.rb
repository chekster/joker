class VoicesController < ApplicationController

  # GET /my_voices
  # GET /my_voices.json
  def my_voices
    user = User.where(:guser => params[:guser]).first
    if !user.nil?
      @voices = Voice.where(:user_id => user.id, :deleted => false)
      @voices.each do |voice|
        voice.calc_spams
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      #format.json { render json: @voices.to_json(methods: [:user_like,:user_spam]) }
      format.json { render json: @voices.to_json(methods: [:spams, :tags]) }
    end
  end


  # GET /voices
  # GET /voices.json
  def index
    count = 0
    if request.format == 'json'
      user = params[:user]
      sort = 'rand'
      if !params[:sort].nil?
        sort = params[:sort]
      end
      from = 0
      if !params[:from].nil?
        from = params[:from]
      end
      n = 20
      voices_ids = []
      by_tags = false
      if !params[:tags].nil? and !params[:tags].empty?
        by_tags = true
        tags_names = params[:tags].to_s.split(",")
        tags = Tag.where(["tag IN (?)", tags_names])
        if !tags.empty?
          tags.each{ |tag|
            if voices_ids.empty?
              voices_ids = tag.voice_ids
            else
              voices_ids.concat(tag.voice_ids)
            end
          }
          voices_ids.uniq!
        end
      end

      if !by_tags
        if (from == '0' or from == 0) and (sort == 'rand' or sort == 'best' or sort == 'new')
            count =  Voice.where(:enabled => true, :deleted => false).count
        end
        if sort == 'rand'
          @voices = Voice.order("RAND()")
                          .where(:enabled => true, :deleted => false)
                          .limit(n).offset(from)
        elsif sort == 'best'
          @voices = Voice.order("likes_sum DESC")
                          .where(:enabled => true, :deleted => false)
                          .limit(n).offset(from)
        elsif sort == 'new'
          @voices = Voice.order("created_at DESC")
                          .where(:enabled => true, :deleted => false)
                          .limit(n).offset(from)
        elsif sort == 'like'
          user_likes = Like.where(:user_id => user.id, :like => true)
          liked_voices_ids = user_likes.collect { |l| l.voice_id }
          if (from == '0' or from == 0)
            count =  Voice.where(["id IN (?)", liked_voices_ids])
                            .where(:enabled => true, :deleted => false).count
          end
          @voices = Voice.order("likes_sum DESC")
                          .where(["id IN (?)", liked_voices_ids])
                          .where(:enabled => true, :deleted => false)
                          .limit(n).offset(from)
        end
      else
        if (from == '0' or from == 0) and (sort == 'rand' or sort == 'best' or sort == 'new')
            count =  Voice.where(:enabled => true, :deleted => false)
                          .where(["id IN (?)", voices_ids]).count
        end
        if sort == 'rand'
          @voices = Voice.order("RAND()")
                          .where(:enabled => true, :deleted => false)
                          .where(["id IN (?)", voices_ids])
                          .limit(n).offset(from)
        elsif sort == 'best'
          @voices = Voice.order("likes_sum DESC")
                          .where(:enabled => true, :deleted => false)
                          .where(["id IN (?)", voices_ids])
                          .limit(n).offset(from)
        elsif sort == 'new'
          @voices = Voice.order("created_at DESC")
                          .where(:enabled => true, :deleted => false)
                          .where(["id IN (?)", voices_ids])
                          .limit(n).offset(from)
        elsif sort == 'like'
          user_likes = Like.where(:user_id => user.id, :like => true)
          liked_voices_ids = user_likes.collect { |l| l.voice_id }
          voices_ids = voices_ids & liked_voices_ids
          if (from == '0' or from == 0)
            count =  Voice.where(["id IN (?)", voices_ids])
                            .where(:enabled => true, :deleted => false).count
          end
          @voices = Voice.order("likes_sum DESC")
                          .where(["id IN (?)", voices_ids])
                          .where(:enabled => true, :deleted => false)
                          .limit(n).offset(from)
        end
      end

      calc_user_likes_and_spams_for_voices_array(@voices, user)

      selected_voices_ids = @voices.collect { |v| v.id }
      selected_voices_spams = Spam.where(["voice_id IN (?)", selected_voices_ids])

      users_ids = @voices.collect { |v| v.user_id }
      users_ids.uniq!
      users = User.where(["id IN (?)", users_ids])

      @voices.each do |voice|
        if voice.spams.nil?
          voice.spams = 0
        end
        selected_voices_spams.each do |spam|
          if spam.voice_id == voice.id
            voice.spams = voice.spams + 1
          end
        end
        users.each do |user|
          if user.id == voice.user_id
            voice.user_name = user.name
            break
          end
        end
      end

      #TODO store all votes on client side calculate it on user side
    else
      @enabled = false
      @deleted = false
      @downloaded = false
      @stream_deleted = false
      if !params[:search].nil?
        if !params[:enabled].nil?
          @enabled = params[:enabled]
        end
        if !params[:deleted].nil?
          @deleted = params[:deleted]
        end
        if !params[:downloaded].nil?
          @downloaded = params[:downloaded]
        end
        if !params[:stream_deleted].nil?
          @stream_deleted = params[:stream_deleted]
        end
        @voices = Voice
          .where(:enabled => @enabled, :deleted => @deleted, :downloaded => @downloaded, :stream_deleted => @stream_deleted).paginate(:page => params[:page], :per_page => 20)
      else
        @voices = Voice.paginate(:page => params[:page], :per_page => 20)
      end
      users_ids = @voices.collect { |v| v.user_id }
      users = User.where(["id IN (?)", users_ids])
      @voices.each do |voice|
        users.each do |user|
          if user.id == voice.user_id
            voice.user_name = user.guser + ' (' + user.name + ')'
            break
          end
        end
      end
      app_account_ids = @voices.collect { |v| v.app_account_id }
      app_accounts = AppAccount.where(["id IN (?)", app_account_ids])
      @voices.each do |voice|
        app_accounts.each do |app_account|
          if app_account.id == voice.app_account_id
            voice.app_account_name = app_account.login + ' ' + app_account.password
            break
          end
        end
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      if !params[:v].nil?
        output = '{"count":' + count.to_s + ',
                    "voices":' + @voices.to_json(methods: [:user_like,:user_spam,:user_name,:spams,:tags]) + '}'
        format.json { render json: output }
      else
        format.json { render json: @voices.to_json(methods: [:user_like,:user_spam,:user_name,:spams])}
      end
    end
  end

  def calc_user_likes_and_spams_for_voices_array(voices, user)
    ids = voices.collect { |v| v.id }
    user_likes = Like
                    .where(:user_id => user.id)
                    .where(["voice_id IN (?)", ids])
    user_spams = Spam
                    .where(:user_id => user.id)
                    .where(["voice_id IN (?)", ids])
    voices.each do |voice|
      user_likes.each do |user_like|
        if user_like.voice_id == voice.id
          voice.user_like = user_like
          break
        end
      end
      user_spams.each do |user_spam|
        if user_spam.voice_id == voice.id
          voice.user_spam = user_spam
          break
        end
      end
    end
  end

  # GET /voices/1
  # GET /voices/1.json
  def show
    @voice = Voice.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @voice }
    end
  end

  # GET /voices/new
  # GET /voices/new.json
  def new
    @voice = Voice.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @voice }
    end
  end

  # GET /voices/1/edit
  def edit
    @voice = Voice.find(params[:id])
    @tags = ''
    @voice.tags.each {|tag|
      @tags = @tags + tag.tag + ','
    }

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @voice }
    end
  end

  # POST /voices
  # POST /voices.json
  def create
    @voice = Voice.new(params[:voice])
    @voice.guser = params[:guser]

    app_account = AppAccount.find(@voice.app_account_id)
    app_account.last_usage = Time.new
    app_account.save

    respond_to do |format|
      if @voice.save
        format.html { redirect_to @voice, notice: 'Voice was successfully created.' }
        format.json { render json: @voice, status: :created, location: @voice }
      else
        format.html { render action: "new" }
        format.json { render json: @voice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /voices/1
  # PUT /voices/1.json
  def update
    @voice = Voice.find(params[:id])
    result = false
    if request.format == 'json'
      @voice.tags_access = params[:voice][:tags_access]
      result = @voice.save
    else
      result = @voice.update_attributes(params[:voice])
    end
    respond_to do |format|
      if result
        format.html { redirect_to @voice, notice: 'Voice was successfully updated.' }
        format.json { render json: @voice, status: :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @voice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /voices/1
  # PUT /voices/1.json
  def update_tags
    @voice = Voice.find(params[:id])
    result = true
    tags = params[:tags].split(",")
    tags.each {|tag|
      tag_obj = Tag.where(:tag => tag).first
      if tag_obj.nil?
        @voice.tags.create({tag: tag})
      else
        if !@voice.tags.exists?(tag_obj)
          @voice.tags << tag_obj
        end
      end
    }
    respond_to do |format|
      if result
        format.html { redirect_to @voice, notice: 'Voice was successfully updated.' }
        #TODO maybe return here voice json
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @voice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /voices/1
  # DELETE /voices/1.json
  def destroy
    @voice = Voice.find(params[:id])
    if request.format == 'json'
      if !@voice.deleted and params[:user].id == @voice.user_id
        @voice.deleted = true
        @voice.save
      end
    else
      #TODO to think how to do it for admin
      @voice.destroy
    end

    respond_to do |format|
      format.html { redirect_to voices_url }
      format.json { head :no_content }
    end
  end
end
