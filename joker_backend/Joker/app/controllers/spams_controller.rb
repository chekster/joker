class SpamsController < ApplicationController
  # GET /spams
  # GET /spams.json
  def index
    @spams = Spam.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @spams }
    end
  end

  # GET /spams/1
  # GET /spams/1.json
  def show
    @spam = Spam.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @spam }
    end
  end

  # GET /spams/new
  # GET /spams/new.json
  def new
    @spam = Spam.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @spam }
    end
  end

  # GET /spams/1/edit
  def edit
    @spam = Spam.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @like }
    end
  end

  # POST /spams
  # POST /spams.json
  def create
    @spam = Spam.new(params[:spam])
    @spam.guser = params[:guser]

    respond_to do |format|
      if @spam.save
        user = User.where(:guser => params[:guser]).first
        voice = Voice.find(params[:spam][:voice_id])
        voice.calc_current_user_votes(user)
        voice.calc_spams
        format.html { redirect_to @spam, notice: 'Spam was successfully created.' }
        #format.json { render json: @spam, status: :created, location: @spam }
        format.json { render json: voice.to_json(methods: [:user_like,:user_spam,:spams]), status: :created, location: @spam }
      else
        format.html { render action: "new" }
        format.json { render json: @spam.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /spams/1
  # PUT /spams/1.json
  def update
    @spam = Spam.find(params[:id])

    respond_to do |format|
      if @spam.update_attributes(params[:spam])
        format.html { redirect_to @spam, notice: 'Spam was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @spam.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spams/1
  # DELETE /spams/1.json
  def destroy
    @spam = Spam.find(params[:id])
    ok_for_destroy = false
    if request.format == 'html'
      if session[:role] != 'admin'
        raise "destroy spam, wrong user"
      else
        ok_for_destroy = true
      end
    elsif request.format == 'json'
      user = params[:user]
      if user.id == @spam.user_id
        voice = Voice.find(@spam.voice_id)
        ok_for_destroy = true
      else
        raise "destroy spam, wrong user"
      end
    else
    end

    if ok_for_destroy
      @spam.destroy
    end

    if request.format == 'json' and ok_for_destroy
      voice.calc_current_user_votes(user)
      voice.calc_spams
    end

    respond_to do |format|
      format.html { redirect_to spams_url }
      #format.json { head :no_content }
      format.json { render json: voice.to_json(methods: [:user_like,:user_spam,:spams]), status: :ok }
    end
  end
end
