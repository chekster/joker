class UserMailer < ActionMailer::Base

  def forgot_password_email(user)
    #TODO add languges support
    secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
    c = ActiveSupport::MessageEncryptor.new(secret)
    @password = c.decrypt(user.tmp_password)
    @user = user
    @hash = Settings.base_url + 'users/' + user.id.to_s + '/restore_password?h=' + user.password_recovery_hash
    mail(:from => Settings.from_email, :to => user.guser, :subject => "Password recovery for Joker")
  end
end
