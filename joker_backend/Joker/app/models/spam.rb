class Spam < ActiveRecord::Base
  attr_accessible :user_id, :voice_id, :guser
  attr_accessor :guser
  belongs_to :user
  belongs_to :voice
  validates :voice_id,
    :presence => {:message => 'voice id is required'}
  validate :validate_user_id

  after_create :on_create_spam_report
  before_update :on_before_update_spam

  def validate_user_id
    user = User.find(self.user_id)
  rescue
    user = User.where(:guser => self.guser).first
    if user.nil?
      if self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user is not valid")
      end
      if !self.user_id.nil? and self.guser.nil?
        errors.add(:user_id, "user is not valid")
      end
      if self.user_id.nil? and self.guser.nil?
        errors.add(:guser, "must be user_id or guser")
      end
      if !self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user_id and guser are not exists")
      end
    else
      self.user_id = user.id
    end
  end

  protected
  def on_create_spam_report
=begin
    voice = Voice.find(self.voice_id)
    if !voice.nil?
      like = Like.where(:voice_id => self.voice_id, :user_id => self.user_id).first
      if like.nil?
        like = Like.new
        like.user_id = self.user_id
        like.voice_id = self.voice_id
        like.like = false
        like.save
      else
        if like.like
          like.like = false
          like.save
        end
      end
    else
      logger.warning "Spam : #{self.id} has no voice"
    end
=end
  end

  #Safe user_id and voice_id from updating
  protected
  def on_before_update_spam
    if self.user_id_changed?
      self.user_id = self.user_id_was
    end
    if self.voice_id_changed?
      self.voice_id = self.voice_id_was
    end
  end

end
