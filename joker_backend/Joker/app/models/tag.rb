class Tag < ActiveRecord::Base
  attr_accessible :tag
  has_and_belongs_to_many :voices
  validates :tag,
    :presence => { :message => "tag is required" },
    :uniqueness => {:message => 'tag already exists'}

  before_save :downcase_tags

  protected
  def downcase_tags
    self.tag = self.tag.downcase
  end

end
