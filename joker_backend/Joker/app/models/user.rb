class User < ActiveRecord::Base
  attr_accessible :guser, :language, :enabled, :password, :name, :tmp_password, :password_recovery_hash
  has_many :likes, :foreign_key => 'user_id'#, :dependent => :delete_all
  has_many :spams, :foreign_key => 'user_id'#, :dependent => :delete_all
  has_many :voices, :foreign_key => 'user_id'#, :dependent => :delete_all
  has_one :token, :class_name => "Token", :foreign_key => 'user_id'#, :dependent => :delete_all
  validates :guser,
    :presence => { :message => "gmail identifier is required" },
    :uniqueness => {:message => 'gmail identifier is already exists'}
  validates :language,
    :presence => { :message => "language is required" }
  validates :password,
    :presence => { :message => "password is required" }
  validates :name,
    :presence => { :name => "name is required" }

  validate :validate_name, :validate_password

  def is_unregistered
    result = false
    if self.password == 'botbot' and self.name = 'botbot'
      result = true
    end
    return result
  end

  def login(password, format)
    result = false
    secret = Digest::SHA1.hexdigest(Joker::Application::SECRET_KEY)
    c = ActiveSupport::MessageEncryptor.new(secret)
    decrypted_password = c.decrypt(self.password)
    if password == decrypted_password
      result = true
      if !self.tmp_password.nil?
        self.tmp_password = nil
        self.password_recovery_hash = nil
        self.save
      end
      if !format.nil? and format == 'json'
        if !self.token.nil?
          self.token.destroy
        end
        self.create_token()
      end
    else
      self.errors.add(:password, 'User not authorized')
    end
    return result
  end

  def validate_name
    if !self.name.nil?
      if self.name.length > 16
          errors.add(:name, "name too long")
      end
      if self.name.length < 2
          errors.add(:name, "name too short")
      end
    else
        errors.add(:name, "name is required")
    end
  end

  def validate_password
    if !self.password.nil?
=begin
      if self.password.length > 20
          errors.add(:password, "password too long")
      end
=end
      if self.password.length < 6
          errors.add(:password, "password too short")
      end
    else
        errors.add(:password, "password is requried")
    end
  end

end
