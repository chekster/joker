class Token < ActiveRecord::Base
  attr_accessible :token, :user_id, :trusted
  belongs_to :user

  #TODO do not create token from browser

  validates :user_id,
    :presence => { :message => "user_id is required" },
    :uniqueness => {:message => 'token for this user already exists'}

  validate :validate_user_id

  before_create :init_token
  before_update :restrict_updating

  def validate_user_id
    user = User.find(self.user_id)
  rescue
    errors.add(:user_id, "user_id is not valid")
  end

  protected
  def init_token
    uuid = UUID.new
    self.token = uuid.generate
  end

  protected
  def restrict_updating
    return false
  end

end
