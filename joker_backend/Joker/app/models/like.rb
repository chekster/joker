class Like < ActiveRecord::Base
  attr_accessible :like, :user_id, :voice_id, :guser
  attr_accessor :guser
  belongs_to :user
  belongs_to :voice
  validates :voice_id,
    :presence => {:message => 'voice id is required'}
  validate :validate_user_id

  before_save :set_default_like
  after_create :on_create_like__set_voice_likes
  before_update :on_before_update_like
  after_update :on_update_like__set_voice_likes
  after_destroy :calc_voice_likes

  def validate_user_id
    user = User.find(self.user_id)
  rescue
    user = User.where(:guser => self.guser).first
    if user.nil?
      if self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user is not valid")
      end
      if !self.user_id.nil? and self.guser.nil?
        errors.add(:user_id, "user is not valid")
      end
      if self.user_id.nil? and self.guser.nil?
        errors.add(:guser, "must be user_id or guser")
      end
      if !self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user_id and guser are not exists")
      end
    else
      self.user_id = user.id
    end
  end

  protected
  def set_default_like
    if self.like.nil?
      self.like = true
    end
  end

  protected
  def on_create_like__set_voice_likes
    voice = Voice.find(self.voice_id)
    if !voice.nil?
      if self.like
        voice.increment(:likes)
        voice.increment(:likes_sum)
      else
        voice.increment(:dislikes)
        voice.decrement(:likes_sum)
      end
      voice.save
    else
      logger.warning "Like : #{self.id} has no voice"
    end
    #self.remove_spam_report_on_positive_like
  end

  #Safe user_id and voice_id from updating
  protected
  def on_before_update_like
    if self.user_id_changed?
      self.user_id = self.user_id_was
    end
    if self.voice_id_changed?
      self.voice_id = self.voice_id_was
    end
  end

  protected
  def on_update_like__set_voice_likes
    if self.like_changed?
      voice = Voice.find(self.voice_id)
      if !voice.nil?
        if self.like
          voice.increment(:likes)
          voice.decrement(:dislikes)
          voice.increment(:likes_sum)
          voice.increment(:likes_sum)
        else
          voice.increment(:dislikes)
          voice.decrement(:likes)
          voice.decrement(:likes_sum)
          voice.decrement(:likes_sum)
        end
        voice.save
      else
        logger.warning "Like : #{self.id} has no voice"
      end
      #self.remove_spam_report_on_positive_like
    end
  end

  def remove_spam_report_on_positive_like
    if self.like
      spam = Spam.where(:voice_id => self.voice_id, :user_id => self.user_id).first
      if !spam.nil?
        spam.destroy
      end
    end
  end

  protected
  def calc_voice_likes
    voice = Voice.find(self.voice_id)
    if !voice.nil?
      if self.like
        voice.decrement(:likes)
        voice.decrement(:likes_sum)
      else
        voice.decrement(:dislikes)
        voice.increment(:likes_sum)
      end
      voice.save
    else
      logger.warning "Like : #{self.id} has no voice"
    end
  end

end
