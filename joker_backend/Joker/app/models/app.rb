class App < ActiveRecord::Base
  attr_accessible :client_id, :client_secret, :system, :time
  has_many :app_accounts, :foreign_key => 'app_id'
  validates :client_id,
    :presence => {:message => 'client_id is required'}
  validates :client_secret,
    :presence => {:message => 'client_secret is required'}
end
