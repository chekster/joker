class AppAccount < ActiveRecord::Base
  attr_accessible :app_id, :login, :password, :time, :max_time, :last_usage
  belongs_to :app
  has_many :voices, :foreign_key => 'app_account_id'
  #TODO add unique index for app_id<=>login beter app.system<=>login

  validates :login,
    :presence => {:message => 'login is required'}
  validates :password,
    :presence => {:message => 'password is required'}

  after_update :update_app

  def update_app
    if self.time_changed?
      self.app.time = self.app.time + self.time - self.time_was
      self.app.save
    end
  end
end
