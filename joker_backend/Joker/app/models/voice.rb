class Voice < ActiveRecord::Base
  attr_accessible :dislikes, :enabled, :likes, :likes_sum, :system, :url, :link, :user_id, :language, :size, :duration, :app_account_id, :deleted, :stream_deleted, :tags_access, :guser, :downloaded

  attr_accessor :tags_access, :guser, :user_like, :user_spam, :spams, :user_name, :app_account_name
  belongs_to :user
  belongs_to :app_account

  has_many :likes_array, :class_name => "Like", :foreign_key => 'voice_id'#, :dependent => :delete_all
  has_many :spams_array, :class_name => "Spam", :foreign_key => 'voice_id'#, :dependent => :delete_all
  has_and_belongs_to_many :tags
  validates :system,
    :presence => {:message => 'system is required'},
    :inclusion => ['soundcloud']
  validates :url,
    :presence => {:message => 'url is required'},
    :uniqueness => {:message => 'url is already exists'}
  validates :link,
    :presence => {:message => 'link is required'},
    :uniqueness => {:message => 'link is already exists'}
  validates :duration,
    :presence => {:message => 'duration is required'}
  validates :size,
    :presence => {:message => 'size is required'}
  validate :validate_url,
    :validate_user_id

  before_save :set_defaults
  after_save :save_tags
  after_create :update_app_account
  before_update :on_before_update
  after_destroy :on_after_destroy


  def calc_spams
    self.spams = Spam.where(:voice_id => self.id).count
  end

  def get_user_name
    self.user_name = User.find(self.user_id).name
  end

  def calc_current_user_votes(user)
    self.user_like = Like.where(:user_id => user.id,:voice_id => self.id).first
    self.user_spam = Spam.where(:user_id => user.id,:voice_id => self.id).first
  end

  def validate_user_id
    user = User.find(self.user_id)
  rescue
    user = User.where(:guser => self.guser).first
    if user.nil?
      if self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user is not valid")
      end
      if !self.user_id.nil? and self.guser.nil?
        errors.add(:user_id, "user is not valid")
      end
      if self.user_id.nil? and self.guser.nil?
        errors.add(:guser, "must be user_id or guser")
      end
      if !self.user_id.nil? and !self.guser.nil?
        errors.add(:guser, "user_id and guser are not exists")
      end
    else
      self.user_id = user.id
    end
  end

  def validate_url
    #TODO add url validation
    #http://www.simonecarletti.com/code/public_suffix/
    #http://stackoverflow.com/questions/7167895/whats-a-good-way-to-validate-links-urls-in-rails-3

    errors.add(:url, "url is not valid") if false
    #logger.debug "Person attributes hash: #{@person.attributes.inspect}"
  end

  protected
  def set_defaults
    user = set_user_from_input_parameters
    if self.language.nil?
      self.language = user.language
    end
  end

  def set_user_from_input_parameters
    user = User.find(self.user_id)
    return user
  rescue
    user = User.where(:guser => self.guser).first
    if !user.nil?
      self.user_id = user.id
    end
    return user
  end

  protected
  def update_app_account
    if !self.app_account.nil?
      self.app_account.time = self.app_account.time + self.duration
      self.app_account.save
    end
  end

  protected
  def save_tags
    if !self.tags_access.nil?
      self.tags_access.each do |tag|
        finded_tag = Tag.where(:tag => tag['tag'].downcase).first
        if finded_tag.nil?
          new_tag = Tag.new
          new_tag.tag = tag['tag'].downcase
          new_tag.save
          self.tags.push new_tag
        else
          if !self.tags.exists?(finded_tag)
            self.tags.push finded_tag
          end
        end
      end
      self.tags.each do |voice_tag|
        exist = false
        self.tags_access.each do |virt_tag|
          if voice_tag.tag == virt_tag['tag'].downcase
            exist = true
          end
        end
        if !exist
          self.tags.delete voice_tag
        end
      end
    end
  end

  protected
  def on_before_update
    if self.stream_deleted_changed?
      if !self.stream_deleted_was
        self.app_account.time = self.app_account.time - self.duration
        self.app_account.save
      else
        self.stream_deleted = self.stream_deleted_was
      end
    end
  end

  protected
  def on_after_destroy
    if !self.stream_deleted_was
      self.app_account.time = self.app_account.time - self.duration
      self.app_account.save
    end
  end

end
