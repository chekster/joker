package com.jokerjok;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.cordova.api.CallbackContext;
import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Endpoints;
import com.soundcloud.api.Params;
import com.soundcloud.api.Request;
import com.soundcloud.api.Token;

import com.soundcloud.api.Http;

public class SoundCloudWrapper {
	private static final String LOG_TAG = "SoundCloudWrapper";
	private static Token token = null;
	private static Integer tokenExperingSeconds = null;
	private static Date tokenStartTime = null;
	private static ApiWrapper wrapper;
	private static JSONObject voiceObj;
	
	public static boolean checkToken() {
    	if (token != null) {
    		return true;	
		} else {
    		return false;	
		}		
	}
	
    public static void initSCToken(JSONArray args, CallbackContext callbackContext, Context ctx) {
    	//Log.e(LOG_TAG, "initSCToken");
    	if (tokenExperingSeconds == null) {
    		Resources r = ctx.getApplicationContext().getResources();
    		String environment = r.getString(R.string.environment);
    		if (environment.equals("staging")) {
    			tokenExperingSeconds = Integer.parseInt(r.getString(R.string.token_expiring_staging));
    		} else {
    			tokenExperingSeconds = Integer.parseInt(r.getString(R.string.token_expiring_staging));
    		}
		}
    	boolean argsOk = true;
    	Object[] params = new Object[5];
		params[0] = VoiceRecorder.client_id;
		params[1] = VoiceRecorder.client_secret;
		params[2] = VoiceRecorder.login;
		params[3] = VoiceRecorder.password;
		params[4] = ctx;
		
    	if(argsOk){
    		if (tokenStartTime != null) {
    			Date newTokenStartTime = new Date();
    			long diff = newTokenStartTime.getTime() - tokenStartTime.getTime();
    			long diffSeconds = diff / 1000 % 60;
    			if (diffSeconds > tokenExperingSeconds) {
                	new GetTokenTask().execute(params);
    			}
    		} else{
            	new GetTokenTask().execute(params);
    		}
    		callbackContext.success("{'status':1}");
        } else {
        	callbackContext.error("{'status':0}");
        } 
    }	
        
    //private static class GetTokenTask extends AsyncTask<String, Void, String> {
	private static class GetTokenTask extends AsyncTask<Object, Void, String> {
		//protected String doInBackground(String... params) {
	    protected String doInBackground(Object... params) {
			String result = "1";
			// final ApiWrapper
			wrapper = new ApiWrapper(params[0].toString() /* client_id */,
									 params[1].toString() /* client_secret */, 
									 null /* redirect URI */, 
									 null /* token */);
			try {
				//Esli zaprawivat' token sliwkom bystro to on mozhet ne uspet' prinyat' znacjenie i zapustitsya ewe raz
				token = wrapper.login(params[2].toString() /* login */, params[3].toString() /* password */);
				tokenStartTime = new Date();
	    		//Context ctx = (Context)params[4];
				//Log.e(LOG_TAG, "GetTokenTask token  " + token);
				//wrapper.toFile(WRAPPER_SER);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = "0";
			}
			return result;
	    }

	    protected void onPostExecute(String err) {
	    	if(err.equals("0")) {
	    		token = null;
	    		tokenStartTime = null;
	    	}
	    }
	}    

	private static class UploadFileTask extends AsyncTask<Object, Void, HttpResponse> {
	    protected HttpResponse doInBackground(Object... params) {
	    	/*
	    	try {
				Log.e(LOG_TAG, "UploadFileTask " + voiceObj.getString("fileStatus"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			*/	    	
	    	HttpResponse result=null;
	    	try {
	        	
	    		Context ctx = (Context)params[0];	    		
	    		File file = null;
	    		String title = "";
				try {					
					file = new File(voiceObj.getString("fileLocation"));
					title = voiceObj.getString("user_name") + " " + voiceObj.getString("duration");
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
	            if (!file.exists()) throw new IOException("The file `"+file+"` does not exist");
	            if (title == "") {
                    title = file.getName();
				}


				
	            //System.err.println("Uploading " + title);

	             
	            result = wrapper.post(Request.to(Endpoints.TRACKS)
	                        .add(Params.Track.TITLE, title)
	                        //.add(Params.Track.TAG_LIST, "demo upload")
	                        .add(Params.Track.DOWNLOADABLE, "1")	                        
	                        .withFile(Params.Track.ASSET_DATA, file)
	                        .setProgressListener(new Request.TransferProgressListener() {
	                            @Override public void transferred(long amount) {
	                                System.err.print(".");
	                            }
	                        }));

	            String json = Http.formatJSON(Http.getString(result));	            
	        	try {
					JSONObject jsonResponseObj = new JSONObject(json);
					voiceObj.put("url", jsonResponseObj.get("stream_url"));
					voiceObj.put("link", jsonResponseObj.get("permalink_url"));
					double bytes = file.length();
					double kilobytes = (bytes / 1024);
					double megabytes = (kilobytes / 1024);	            
					voiceObj.put("size", megabytes);
					FileOutputStream fos;
		        	ctx.deleteFile(voiceObj.getString("fileStatus"));
					fos = ctx.openFileOutput(voiceObj.getString("fileStatus"), ctx.MODE_PRIVATE);
	    			fos.write(voiceObj.toString().getBytes());
	    	    	fos.close();
	    	    	VoiceRecorder.updateVoiceList(voiceObj);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	    		} catch (FileNotFoundException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		} catch (IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	    	} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return result;
	    }

	    protected void onPostExecute(HttpResponse resp) {
        }
	}		

    public static void uploadFile(JSONObject voiceJsonObj, CallbackContext callbackContext, Context ctx) {
    	boolean argsOk = true;
    	voiceObj = voiceJsonObj;
    	/*
    	try {
			Log.e(LOG_TAG, "uploadFile " + voiceJsonObj.getString("fileStatus"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
    	Object[] params = new Object[1];
		params[0] = ctx;	    		
    	if(argsOk){
        	new UploadFileTask().execute(params);
    		callbackContext.success("{'status':1}");
        } else {
        	callbackContext.error("{'status':0}");
        } 
    }	
}