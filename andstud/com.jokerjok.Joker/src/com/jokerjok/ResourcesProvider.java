package com.jokerjok;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.util.Log;

public class ResourcesProvider extends CordovaPlugin {
	private static final String LOG_TAG = "ResourcesProvider";

	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		//Log.e(LOG_TAG, "ResourcesProvider " +  action);
        if (action.equals("getStringResourcesArray")) {
            this.getStringResourcesArray(callbackContext, args);
            return true;	
        } else if (action.equals("getArraysResourcesArray")) {
            this.getArraysResourcesArray(callbackContext, args);
            return true;	
        }	
        return false;
    }

	private void getStringResourcesArray(CallbackContext callbackContext, JSONArray args) {
		try {
			//Log.e(LOG_TAG, "getStringResourcesArray ");
			Resources r = cordova.getActivity().getApplicationContext().getResources();
			String resorcesKeyValue = null;						
	        for(int i=0; i<args.length(); i++){
				String resourceName = args.get(i).toString();					
				int resorceId = r.getIdentifier(getResourceName(resourceName, r), "string", "com.jokerjok");
				if(resorceId > 0) {
					if(resorcesKeyValue == null) {
						resorcesKeyValue = "'" + resourceName + "':'" + r.getString(resorceId) + "'";
					} else {
						resorcesKeyValue = resorcesKeyValue + ",'" + resourceName + "':'" + r.getString(resorceId) + "'";
					}
				}
	        }
	        try {
				PackageInfo pInfo = cordova.getActivity().getApplicationContext().getPackageManager().getPackageInfo(cordova.getActivity().getApplicationContext().getPackageName(), 0);
				resorcesKeyValue = resorcesKeyValue + ",'version':'" + pInfo.versionCode + "'";				
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	if(resorcesKeyValue != null){
	            callbackContext.success("{'status':1, 'value':{" + resorcesKeyValue + "}}");
	        } else {
	        	callbackContext.error("{'status':0}");
	        } 			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String getResourceName(String resourceName, Resources r) {
		String resorce = "";		
		if (resourceName.equals("backend_url")) {
			String env = r.getString(R.string.environment);
			resorce = resourceName + "_" + env;			
		} else {
			resorce = resourceName;			
		}		
		return resorce;		
	}

 	private void getArraysResourcesArray(CallbackContext callbackContext, JSONArray args) {
		//Log.e(LOG_TAG, "getArraysResourcesArray ");
		try {
			Resources r = cordova.getActivity().getApplicationContext().getResources();
			String resorcesKeyValue = null;						
			//Log.e(LOG_TAG, "getArraysResourcesArray arg.l " + args.length());
	        for(int i=0; i<args.length(); i++){
				String resourceName = args.get(i).toString();						
				int resorceId = r.getIdentifier(resourceName, "array", "com.jokerjok");
				
				String[] resorceValue = null;			
				if(resorceId > 0) {
					resorceValue = r.getStringArray(resorceId);			

		    		String valuesArray = null;
					for (String v : resorceValue) {
						if (valuesArray == null) {
							valuesArray = "'" + v + "'";
						} else {
							valuesArray = valuesArray + ",'" + v + "'";
						}
					}
					valuesArray = "[" + valuesArray + "]";
				
					if(resorcesKeyValue == null) {
						resorcesKeyValue = "'" + resourceName + "':" + valuesArray;
					} else {
						resorcesKeyValue = resorcesKeyValue + ",'" + resourceName + "':" + valuesArray;
					}
				
				}				
	        }						
	    	if(resorcesKeyValue != null){
	            callbackContext.success("{'status':1, 'value':{" + resorcesKeyValue + "}}");
	        } else {
	        	callbackContext.error("{'status':0}");
	        } 			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}