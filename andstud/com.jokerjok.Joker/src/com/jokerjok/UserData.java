package com.jokerjok;


import java.util.Locale;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;

import android.util.Log;

public class UserData extends CordovaPlugin {
	private static final String LOG_TAG = "joker.UserData";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		//Log.e(LOG_TAG, "UserData execute " +  action);
		if (action.equals("getId")) {
            this.getId(callbackContext);
            return true;
        } else if (action.equals("getLanguage")) {
            this.getLanguage(callbackContext);
            return true;		
        } else if (action.equals("getEmails")) {
        	this.getEmails(callbackContext);
        	return true;
        } else if (action.equals("getClientID")) {
        	this.getClientID(callbackContext, args);
        	return true;
        }
        return false;
    }

    private void getId(CallbackContext callbackContext) {  
    	
    	AccountManager accountManager = AccountManager.get(cordova.getActivity().getApplicationContext());
    	Account[] accounts = accountManager.getAccountsByType("com.google");
    	String id = null;
    	for (Account a: accounts) {
    	    if (a.name.contains("@gmail.com")) {
    	        id = a.name;
    	    }
    	}
        if (id != null && id.length() > 0) { 
            callbackContext.success("{'status':1, 'id':'" + id + "'}");
        } else {
        	callbackContext.error("{'status':0}");
        }                
    }
    

    private void getEmails(CallbackContext callbackContext) {    	
    	AccountManager accountManager = AccountManager.get(cordova.getActivity().getApplicationContext());
    	Account[] accounts = accountManager.getAccountsByType("com.google");
    	String id = null;
    	for (Account a: accounts) {
    	    if (a.name.contains("@gmail.com")) {
    	    	if(id == null) {
        	        id = "'" + a.name + "'";
    	    	} else {
        	        id = id + ",'" + a.name + "'";
    	    	}
    	    }
    	}
        if (id != null && id.length() > 0) { 
            callbackContext.success("{'status':1, 'emails':[" + id + "]}");
        } else {
        	callbackContext.error("{'status':0}");
        }                
    }    
    
    private void getLanguage(CallbackContext callbackContext) {
    	//TODO add settings for app language
    	String lang = Locale.getDefault().getLanguage();
        if (lang != null && lang.length() > 0) { 
        	callbackContext.success("{'status':1, 'language':'" + lang + "'}");
        } else {
            callbackContext.error("{'status':0}");
        }                
    }    

    private void getClientID(CallbackContext callbackContext, JSONArray args) {
		String outputData = "";
		try {
			if (!args.get(0).toString().equals("null")) {

				String encryptedData = args.get(0).toString();
				String[] byteValues = encryptedData.substring(1,
						encryptedData.length() - 1).split(",");
				byte[] bytes = new byte[byteValues.length];
				for (int i = 0, len = bytes.length; i < len; i++) {
					bytes[i] = Byte.valueOf(byteValues[i].trim());
				}
				outputData = new String(bytes);

				JSONObject systemData = new JSONObject(outputData);
				//
				VoiceRecorder.system = systemData.getString("system");
				VoiceRecorder.client_id = systemData.getString("client_id");
				VoiceRecorder.app_account_id = systemData
						.getString("app_account_id");
				if (systemData.length() > 3) {					
					VoiceRecorder.client_secret = systemData
							.getString("client_secret");
					VoiceRecorder.login = systemData.getString("login");
					VoiceRecorder.password = systemData.getString("password");
				}
				callbackContext.success("{'status':1, 'client_id':'"
						+ VoiceRecorder.client_id + "', 'app_account_id':'"
						+ VoiceRecorder.app_account_id + "'}");
			} else {
				callbackContext.error("err");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}