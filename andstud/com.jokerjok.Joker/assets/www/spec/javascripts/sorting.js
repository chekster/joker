describe('Sorting', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
        });
    });
    it('should content 13 methods', function() {
        runs(function() {
            var len = Object.keys(Sorting).length;
            expect(len).toEqual(13);
        });
    });
    describe('after showing', function() {
        beforeEach(function() {
            runs(function() {
                Sorting.clear();
                Sorting.show();
                waits(1000);
            });
        });
        it('sorting should not be a null', function() {
            runs(function() {
                expect(Sorting.getSorting()).not.toEqual(null);
            });
        });
        it('sorting_background should not be a null', function() {
            runs(function() {
                expect(Sorting.getBackground()).not.toEqual(null);
            });
        });
        it('sorting show should set css dispaly to block', function() {
            runs(function() {
                Sorting.show();
            });
            waits(1000);
            runs(function() {
                expect(Sorting.getSorting().css('display')).toEqual('block');
                expect(Sorting.getState()).toEqual('show');
            });
        });
        it('sorting hide should set css dispaly to none', function() {
            runs(function() {
                Sorting.hide();
            });
            waits(1000);
            runs(function() {
                expect(Sorting.getContainer().css('display')).toEqual('none');
                expect(Sorting.getState()).toEqual('hide');
            });
        });
        it('random button should call random func', function() {
            runs(function() {
                spyOn(Sorting, 'random');
                $('#sorting_rand_btn').click();
            });
            runs(function() {
                expect(Sorting.random).toHaveBeenCalled();
            });
        });
        it('best button should call best func', function() {
            runs(function() {
                spyOn(Sorting, 'best');
                $('#sorting_best_btn').click();
            });
            runs(function() {
                expect(Sorting.best).toHaveBeenCalled();
            });
        });
        it('new button should call newj func', function() {
            runs(function() {
                spyOn(Sorting, 'newj');
                $('#sorting_new_btn').click();
            });
            runs(function() {
                expect(Sorting.newj).toHaveBeenCalled();
            });
        });
        it('like button should call like func', function() {
            runs(function() {
                spyOn(Sorting, 'like');
                $('#sorting_like_btn').click();
            });
            runs(function() {
                expect(Sorting.like).toHaveBeenCalled();
            });
        });
    });
});
