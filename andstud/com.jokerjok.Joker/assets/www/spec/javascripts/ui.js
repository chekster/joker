describe('app.ui', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize();
    });
    it('should content 10 methods', function() {
        waits(3000);
        runs(function() {
            voice = Playlist.getVoice();
            var len = Object.keys(app.ui).length;
            expect(len).toEqual(10);
        });
    });
});
