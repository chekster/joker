describe('my voices page', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize(true);
            waits(3000);
        });
    });
    describe('initialization and binding', function() {

        it('page should exist', function() {
            expect(!jQuery.isEmptyObject(app.ui.myVoicesPage)).toEqual(true);
        });
        it('page should content 10 methods', function() {
            var len = Object.keys(app.ui.myVoicesPage).length;
            expect(len).toEqual(10);
        });

        describe('widgets state', function() {
            beforeEach(function() {
                runs(function() {
                    MyVoicesPage.clear();
                    MyVoicesPage.init();
                    MyVoicesPage.onShow();
                    waits(500);
                });
            });
            it('controls bar should exist', function() {
                expect(!jQuery.isEmptyObject($('#myvoices_page_controls_bar'))).toEqual(true);
            });
            it('controls bar should contain 2 buttons after page binding', function() {
                expect($('#myvoices_page_controls_bar').children().length).toEqual(2);
            });
        });

        describe('buttons state', function() {
        });

        describe('buttons events', function() {
            beforeEach(function() {
                runs(function() {
                    MyVoicesPage.clear();
                    MyVoicesPage.init();
                    MyVoicesPage.onShow();
                    waits(500);
                });
            });
            it('play button should call playButtonClick func', function() {
                $('#myvoices_page_play_button').click();
                expect(MyVoicesPage.getRunnedFunc()).toEqual('playButtonClick');
            });
            it('delete button should call deleteButtonClick func', function() {
                $('#myvoices_page_cancel_button').click();
                expect(MyVoicesPage.getRunnedFunc()).toEqual('deleteButtonClick');
            });
        });
    });
});


