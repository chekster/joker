describe('main page', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize();
    });
    describe('initialization and binding', function() {

        it('page should exist', function() {
            expect(!jQuery.isEmptyObject(app.ui.mainPage)).toEqual(true);
        });
        it('page should content 20 methods', function() {
            var len = Object.keys(app.ui.mainPage).length;
            expect(len).toEqual(20);
        });

        describe('widgets state', function() {
            beforeEach(function() {
                waits(3000); //this is need for wait ajax request from initialization
            });
            /*
            it('mainPage voice should be not null', function() {
                waits(1000); //this is need for wait ajax request from initialization
                runs(function() {
                    expect(app.ui.mainPage.getVoice()).toNotEqual(null);
                });
            });
            */
            it('navigation bar should exist', function() {
                expect(!jQuery.isEmptyObject($('#navigation_bar'))).toEqual(true);
            });
            it('controls bar should exist', function() {
                expect(!jQuery.isEmptyObject($('#main_page_controls_bar'))).toEqual(true);
            });
            it('navigation bar should contain 1 button after page binding', function() {
                expect($('#navigation_bar').children().length).toEqual(1);
            });
            it('votes counters bar should exist', function() {
                expect(!jQuery.isEmptyObject($('#main_page_votes_counters_bar'))).toEqual(true);
            });
            /*
            it('controls bar should contain 3 buttons after page binding', function() {
                expect($('#main_page_controls_bar').children().length).toEqual(3);
            });
            */
            it('vote bar should contain 3 buttons after page binding', function() {
                expect($('#main_page_vote_bar').children().length).toEqual(3);
            });
            it('votes couters bar should contain 3 buttons after page binding', function() {
                expect($('#main_page_votes_counters_bar>div').children().length).toEqual(3);
            });
            it('timer should exist', function() {
                expect(!jQuery.isEmptyObject($('#main_page_content .timer_div'))).toEqual(true);
            });
        });

        describe('buttons state', function() {
            it('Recording button should be visible', function() {
                var displayStyle = helper.getComputedStyle('#go_recording_page', 'display');
                expect(displayStyle).not.toEqual('none');
            });
            /*
            it('playing buttons states', function() {
                waits(5000); //this is need for wait ajax request from initialization
                runs(function() {
                    expect($('#main_page_previous_button').closest('div').attr('aria-disabled')).toEqual('true');
                    expect($('#main_page_play_button').closest('div').attr('aria-disabled')).toEqual('false');
                    expect($('#main_page_next_button').closest('div').attr('aria-disabled')).toEqual('false');
                    if ( (app.ui.mainPage.getVoice().user_like != null) || (app.ui.mainPage.getVoice().user_spam != null)) {
                        expect($('#main_page_spam_button').closest('div').attr('aria-disabled')).toEqual('false');
                        expect($('#main_page_unlike_button').closest('div').attr('aria-disabled')).toEqual('false');
                        expect($('#main_page_like_button').closest('div').attr('aria-disabled')).toEqual('false');

                        if ( app.ui.mainPage.getVoice().user_like != null ) {
                            if ( app.ui.mainPage.getVoice().user_like.like ) {
                                expect($('#main_page_like_button').attr('data-theme')).toEqual('b');
                                expect($('#main_page_unlike_button').attr('data-theme')).toEqual('c');
                            } else {
                                expect($('#main_page_like_button').attr('data-theme')).toEqual('c');
                                expect($('#main_page_unlike_button').attr('data-theme')).toEqual('b');
                            };
                        };
                        if ( app.ui.mainPage.getVoice().user_spam != null ) {
                            expect($('#main_page_spam_button').attr('data-theme')).toEqual('b');
                        } else {
                            expect($('#main_page_spam_button').attr('data-theme')).toEqual('c');
                        };
                    } else {
                        expect($('#main_page_spam_button').closest('div').attr('aria-disabled')).toEqual('true');
                        expect($('#main_page_unlike_button').closest('div').attr('aria-disabled')).toEqual('true');
                        expect($('#main_page_like_button').closest('div').attr('aria-disabled')).toEqual('true');
                    };
                });
            });
            */
        });

        describe('buttons events', function() {

            it('recording button should call goRecordingButtonClick func', function() {
                $('#go_recording_page').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('goRecordingButtonClick');
            });
            it('sorting button should call sortTypeButtonClick func', function() {
                $('#main_page_sort_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('sortTypeButtonClick');
            });
            /*
            it('previous button should call previousButtonClick func', function() {
                $('#main_page_previous_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('previousButtonClick');
            });
            it('play button should call playButtonClick func', function() {
                $('#main_page_play_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('playButtonClick');
            });
            it('next button should call nextButtonClick func', function() {
                $('#main_page_next_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('nextButtonClick');
            });
            */

            it('like button should call likeButtonClick func', function() {
                /*
                runs(function() {
                    spyOn(app.ui.mainPage, 'likeButtonClick');
                    $('#main_page_like_button').click();
                });
                runs(function() {
                    expect(app.ui.mainPage.likeButtonClick).toHaveBeenCalled();
                });
                */
                $('#main_page_like_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('likeButtonClick');
            });
            it('unlike button should call unlikeButtonClick func', function() {
                /*
                runs(function() {
                    spyOn(app.ui.mainPage, 'unlikeButtonClick');
                    $('#main_page_unlike_button').click();
                });
                runs(function() {
                    expect(app.ui.mainPage.unlikeButtonClick).toHaveBeenCalled();
                });
                */
                $('#main_page_unlike_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('unlikeButtonClick');
            });
            it('spam button should call spamButtonClick func', function() {
                /*
                runs(function() {
                    spyOn(app.ui.mainPage, 'spamButtonClick');
                    $('#main_page_spam_button').click();
                });
                runs(function() {
                    expect(app.ui.mainPage.spamButtonClick).toHaveBeenCalled();
                });
                */
                $('#main_page_spam_button').click();
                expect(app.ui.mainPage.getRunnedFunc()).toEqual('spamButtonClick');
            });
            /*
            it('mainPage spamButtonClick should call to voice addSpamReport or removeSpamReport', function() {
                waits(1000); //this is need for wait ajax request from initialization
                var user_spam = null;
                runs(function() {
                    user_spam = app.ui.mainPage.getVoice().user_spam;
                    if (user_spam == null) {
                        spyOn(app.ui.mainPage.getVoice(), 'addSpamReport');
                    } else {
                        spyOn(app.ui.mainPage.getVoice(), 'removeSpamReport');
                    };
                    app.ui.mainPage.spamButtonClick();
                });
                runs(function() {
                    if (user_spam == null) {
                        expect(app.ui.mainPage.getVoice().addSpamReport).toHaveBeenCalled();
                    } else {
                        expect(app.ui.mainPage.getVoice().removeSpamReport).toHaveBeenCalled();
                    };
                });
            });

            it('mainPage unlikeButtonClick should call to voice removeLike or addLike', function() {
                waits(1000); //this is need for wait ajax request from initialization
                var user_like = null;
                runs(function() {
                    user_like = app.ui.mainPage.getVoice().user_like;
                    if (user_like != null) {
                        if (!user_like.like) {
                            spyOn(app.ui.mainPage.getVoice(), 'removeLike');
                        } else {
                            spyOn(app.ui.mainPage.getVoice(), 'addLike');
                        }
                    } else {
                        spyOn(app.ui.mainPage.getVoice(), 'addLike');
                    }
                    app.ui.mainPage.unlikeButtonClick();
                });
                runs(function() {
                    if (user_like != null) {
                        if (!user_like.like) {
                            expect(app.ui.mainPage.getVoice().removeLike).toHaveBeenCalled();
                        } else {
                            expect(app.ui.mainPage.getVoice().addLike).toHaveBeenCalled();
                        }
                    } else {
                        expect(app.ui.mainPage.getVoice().addLike).toHaveBeenCalled();
                    }
                });
            });

            it('mainPage likeButtonClick should call to voice removeLike or addLike', function() {
                waits(1000); //this is need for wait ajax request from initialization
                var user_like = null;
                runs(function() {
                    user_like = app.ui.mainPage.getVoice().user_like;
                    if (user_like != null) {
                        if (user_like.like) {
                            spyOn(app.ui.mainPage.getVoice(), 'removeLike');
                        } else {
                            spyOn(app.ui.mainPage.getVoice(), 'addLike');
                        }
                    } else {
                        spyOn(app.ui.mainPage.getVoice(), 'addLike');
                    }
                    app.ui.mainPage.likeButtonClick();
                });
                runs(function() {
                    if (user_like != null) {
                        if (user_like.like) {
                            expect(app.ui.mainPage.getVoice().removeLike).toHaveBeenCalled();
                        } else {
                            expect(app.ui.mainPage.getVoice().addLike).toHaveBeenCalled();
                        }
                    } else {
                        expect(app.ui.mainPage.getVoice().addLike).toHaveBeenCalled();
                    }
                });
            });

            it('call to playButtonClick func should set player status to "true", twice to "false"', function() {
                waits(1000); //this is need for wait ajax request from initialization
                runs(function() {
                    app.ui.mainPage.playButtonClick();
                });
                runs(function() {
                    expect($('#main_page_play_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-pause')).toEqual(true);
                    expect(app.player.playStatus).toEqual(true);
                    app.ui.mainPage.playButtonClick();
                });
                runs(function() {
                    expect($('#main_page_play_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-play')).toEqual(true);
                    expect(app.player.playStatus).toEqual(false);
                });
            });

            it('call playButtonClick func should call timer.clear, player.play, change player status and buttons state', function() {
                runs(function() {
                    waits(3000); //this is need for wait ajax request from initialization
                });

                runs(function() {
                    spyOn(app.ui.mainPage.getTimer(), 'clear');
                    app.ui.mainPage.playButtonClick();
                });
                runs(function() {
                    expect(app.ui.mainPage.getTimer().clear).toHaveBeenCalled();
                    expect($('.ui-icon-pause').length).toEqual(1);
                    expect($('.ui-icon-play').length).toEqual(0);
                    expect(app.player.playStatus).toEqual(true);
                    app.ui.mainPage.playButtonClick();
                    expect($('.ui-icon-pause').length).toEqual(0);
                    expect($('.ui-icon-play').length).toEqual(1);
                    expect(app.player.playStatus).toEqual(false);
                });

                runs(function() {
                    app.ui.mainPage.playButtonClick();
                    expect($('.ui-icon-pause').length).toEqual(1);
                    expect($('.ui-icon-play').length).toEqual(0);
                    expect(app.player.playStatus).toEqual(true);
                    app.ui.mainPage.nextButtonClick();
                    app.ui.mainPage.nextButtonClick();
                    expect($('.ui-icon-pause').length).toEqual(0);
                    expect($('.ui-icon-play').length).toEqual(1);
                    expect(app.player.playStatus).toEqual(false);
                    app.ui.mainPage.playButtonClick();
                    expect($('.ui-icon-pause').length).toEqual(1);
                    expect($('.ui-icon-play').length).toEqual(0);
                    expect(app.player.playStatus).toEqual(true);
                    app.ui.mainPage.previousButtonClick();
                    expect($('.ui-icon-pause').length).toEqual(0);
                    expect($('.ui-icon-play').length).toEqual(1);
                    expect(app.player.playStatus).toEqual(false);
                 });

                runs(function() {
                    spyOn(app.player, 'play');
                    app.ui.mainPage.playButtonClick();
                });
                runs(function() {
                    expect(app.player.play).toHaveBeenCalled();
                    app.ui.mainPage.playButtonClick();
                });

            });

            it('navigation to second voice must set previous btn to enabled', function() {
                waits(1000); //this is need for wait ajax request from initialization
                runs(function() {
                    if (Playlist.voicesCount() > 0) {
                        app.ui.mainPage.nextButtonClick();
                        expect($('#main_page_previous_button').closest('div').attr('aria-disabled')).toEqual('false');
                    } else {
                        expect(true).toEqual(false);
                    }
                });
            });
            it('navigation to last voice must set next btn to disabled', function() {
                waits(1000); //this is need for wait ajax request from initialization
                runs(function() {
                    if (Playlist.voicesCount() > 0) {
                        for (var i = 0; i < (Playlist.voicesCount() - 1); i++) {
                            app.ui.mainPage.nextButtonClick();
                        }
                        expect($('#main_page_next_button').closest('div').attr('aria-disabled')).toEqual('true');
                    } else {
                        expect(true).toEqual(false);
                    }
                });
            });
            */
        });

        describe('timer widget', function() {
            it('timer should content 5 methods', function() {
                var len = Object.keys(app.ui.recordingPage.timer).length;
                expect(len).toEqual(5);
            });
            it('bind method should insert 1 div with class "timer_div"', function() {
                var res = false;
                app.ui.recordingPage.content.empty();
                if(app.ui.recordingPage.content.children().length == 0) {
                    app.ui.recordingPage.timer.bind(app.ui.recordingPage.content);
                    if(app.ui.recordingPage.content.children().length == 1) {
                        res = $(app.ui.recordingPage.content.children()[0]).hasClass('timer_div');
                    }
                }
                expect(res).toEqual(true);
            });
            it('tick(78) method should change inner text to "1:18"', function() {
                app.ui.mainPage.getTimer().clear();
                app.ui.mainPage.getTimer().tick(78);
                expect($('#main_page_content .timer_div')[0].innerText).toEqual('1:18');
            });
            it('clear(78,"down") and tick() method should change inner text to "1:17"', function() {
                app.ui.mainPage.getTimer().clear(78,'down');
                expect($('#main_page_content .timer_div')[0].innerText).toEqual('1:18');
                app.ui.mainPage.getTimer().tick();
                expect($('#main_page_content .timer_div')[0].innerText).toEqual('1:17');
            });
        });

    });
});


