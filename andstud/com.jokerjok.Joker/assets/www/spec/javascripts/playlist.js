describe('play list', function() {
    it('should exist', function() {
        expect(!jQuery.isEmptyObject(Playlist)).toEqual(true);
    });
    it('should content 18 methods', function() {
        var len = Object.keys(Playlist).length;
        expect(len).toEqual(18);
    });
    describe('functionality', function() {
        beforeEach(function() {
            runs(function() {
                loadFixtures("index.html");
                app.initialize();
            });
            waits(1000);
            runs(function() {
                app.user.initSession('chekst@gmail.com','qwerty',null,null);
            });
        });
        it('load without callback should save previous index', function() {
            waits(2000);
            runs(function() {
                Playlist.loadList();
            });
            waits(3000)
            runs(function() {
                expect(Playlist.getVoiceIndex()).toEqual(null);
            });
        });
        it('load should run callback', function() {
            waits(2000);
            var checking = false
            runs(function() {
                Playlist.loadList(function() {checking = true;});
            });
            waits(3000)
            runs(function() {
                expect(checking).toEqual(true);
                expect(Playlist.getVoiceIndex()).toEqual(0);
            });
        });
        it('voicesCount, getVoice, getNextVoice, anyPreviousVoice, anyNextVoice', function() {
            //expect(Playlist.voicesCount()).toBeGreaterThan(0);
            expect(Playlist.anyPreviousVoice()).toEqual(false);
            if (Playlist.voicesCount() > 0) {
                expect(Playlist.anyNextVoice()).toEqual(true);
            } else {
                expect(Playlist.anyNextVoice()).toEqual(false);
            }
            var v = new Voice('','','');
            var voice = Playlist.getNextVoice();
            if (Playlist.voicesCount() > 0) {
                expect(voice.constructor).toEqual(v.constructor);
                expect(Playlist.anyPreviousVoice()).toEqual(true);
                // > 2 because alredy moved one time for next voice
                if (Playlist.voicesCount() > 2) {
                    expect(Playlist.anyNextVoice()).toEqual(true);
                } else {
                    expect(Playlist.anyNextVoice()).toEqual(false);
                }
            } else {
                expect(voice).toEqual(null);
                expect(Playlist.anyPreviousVoice()).toEqual(false);
                expect(Playlist.anyNextVoice()).toEqual(false);
            }
            for (var i = 0; i < (Playlist.voicesCount() - 2); i++) {
                voice = Playlist.getNextVoice();
            }
            expect(Playlist.anyNextVoice()).toEqual(false);
            if (Playlist.voicesCount() > 0) {
                expect(Playlist.anyPreviousVoice()).toEqual(true);
            } else {
                expect(Playlist.anyPreviousVoice()).toEqual(false);
            }
        });
    });
});
