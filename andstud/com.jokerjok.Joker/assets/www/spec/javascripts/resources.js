describe('resources', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize();
        waits(3000);
    });
    it('should content 6 methods', function() {
        runs(function() {
            var len = Object.keys(Resources).length;
            expect(len).toEqual(6);
        });
    });
    it('strings should be length = 50', function() {
        runs(function() {
            var len = Object.keys(Resources.getStrings()).length;
            expect(len).toEqual(50);
        });
    });
    it('arrays should be length = 2', function() {
        runs(function() {
            var len = Object.keys(Resources.getArrays()).length;
            expect(len).toEqual(2);
        });
    });
});
