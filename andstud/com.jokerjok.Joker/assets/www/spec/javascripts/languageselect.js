describe('Language select', function() {
    beforeEach(function() {
        runs(function() {
            app.initialize(true);
            waits(1000);
        });
    });
    it('should content 2 methods', function() {
        var len = Object.keys(LanguageSelect).length;
        expect(len).toEqual(2);
    });
    it('options should not be empty', function() {
        expect(LanguageSelect.getOptions()).not.toEqual(null);
        expect(LanguageSelect.getOptions()).not.toEqual('');
    });
});
