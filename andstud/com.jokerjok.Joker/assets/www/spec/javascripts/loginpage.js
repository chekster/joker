describe('loginpage', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize(true);
        waits(3000);
    });
    /*
    */
    it('should content 4 methods', function() {
        runs(function() {
            var len = Object.keys(LoginPage).length;
            expect(len).toEqual(4);
        });
    });
    it('login button should call submit func', function() {
        runs(function() {
            spyOn(LoginPage, 'submit');
            $('#login_ok').click();
        });
        runs(function() {
            expect(LoginPage.submit).toHaveBeenCalled();
        });
    });
    /*
    it('login form submit should call submit func', function() {
        runs(function() {
            spyOn(LoginPage, 'submit');
            $('#login_form').submit();
        });
        runs(function() {
            expect(LoginPage.submit).toHaveBeenCalled();
        });
    });
    */
    it('forgot password button should call forgotPassword func', function() {
        runs(function() {
            spyOn(LoginPage, 'forgotPassword');
            $('#login_forgot_password').click();
        });
        runs(function() {
            expect(LoginPage.forgotPassword).toHaveBeenCalled();
        });
    });
    it('login_email should exist', function() {
        expect(!jQuery.isEmptyObject($('login_email'))).toEqual(true);
    });
    it('login_email_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('login_email_lbl'))).toEqual(true);
    });
    it('login_password_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('login_password_lbl'))).toEqual(true);
    });
    it('login_password should exist', function() {
        expect(!jQuery.isEmptyObject($('login_password'))).toEqual(true);
    });
    it('on submit should show ajax loader, run callbacks, hide ajax loader', function() {
        var suc = false;
        var fal = false;
        runs(function() {
            $('#login_password').val('qsfhhfg');
            LoginPage.submit(function () {suc = true;}, function () {fal = true;});
            //expect(AjaxLoader.getState()).toEqual('show');
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
            expect(AjaxLoader.getState()).toEqual('hide');
        });
    });
});
