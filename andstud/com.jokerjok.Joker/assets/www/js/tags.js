Tags = function() {
    //TODO dynamic id's, or destroy it after living page
    //TODO words from resources
    var tags_html = $('<div class="tags_div invisible" id="tags_div"><form id="add_tag_form"><label for="add_tag_text">Add tag</label><input type="text" id="add_tag_text"/><input type="submit" value="Invisible tex" id="add_tag_button" data-inline="true" data-icon="plus" data-iconpos="notext" /></form><p id="tags_field"></p><div/>');
    var tags = [];
    var add_form = null;
    var add_button = null;
    var add_text = null;
    var tags_field = null;
    var tags_div = null;
    var onAddButtonClick = function() {
        //TODO on enter push also run it
        var tag_text = add_text.val();
        if(tag_text.length >= 3) {
            add_text.val('');
            tags.push(tag_text);
            var tag_button = $('<input type="submit" value="' + tag_text + '" data-mini="true" data-inline="true" data-icon="delete" data-iconpos="right" />');
            tag_button.click(function() {
                var remove_tag = $(this).val();
                tags = jQuery.grep(tags, function(tag) {
                    return tag != remove_tag;
                });
                $(this).closest('.ui-btn').remove();
            });
            tags_field.append(tag_button).trigger('create');
        }
        return false;
    };
    var onAddTextChange = function() {
        var tag_text = add_text.val();
        if(tag_text.length >= 3) {
            add_button.button('enable');
        } else {
            add_button.button('disable');
        }
    };
    return {
        addButtonClick: function() {
            onAddButtonClick();
        },
        bind: function(holder, str_test) {
            var that = this;
            holder.append(tags_html);
            add_form = $("#add_tag_form");
            add_text = $("#add_tag_text");
            add_button = $('#add_tag_button');
            tags_field = $('#tags_field');
            tags_div = $('#tags_div');
            add_button.trigger('create');
            //TODO disable button on create
            add_text.unbind("change").bind('change',onAddTextChange);
            add_button.unbind("click").click(function() {that.addButtonClick();});
            add_form.unbind("submit").submit(function() {that.addButtonClick();});
        },
        clear: function() {
            add_text.val('');
            tags = [];
            $(tags_field).empty();
        },
        hide: function() {
            //tags_div.hide();
            tags_div.addClass('invisible');
        },
        show: function() {
            //tags_div.show();
            tags_div.removeClass('invisible');
        },
        getTags: function() {
            return tags;
        }
    }
}

