var ChangePassword =(function(){
	var instantiated;

	var init = function() {
        var inited = false;
        var change_password_container = null;
        var change_password = null;
        var change_password_content = null;
        var change_password_background = null;
        var state = null;
        var password = null;
        var old_password = null;
        var confirm_password = null;
        var ok = null;


        var change_password_html = function() {
            var html = "<div>" +

                            "<label for='change_password_old_password' id='change_password_old_password_lbl'>" +
                                Resources.get('old_password_lbl') +
                            "</label>" +
                            "<input type='password' id='change_password_old_password' value='' />" +

                            "<label for='change_password_password' id='change_password_password_lbl'>" +
                                Resources.get('new_password_lbl') +
                            "</label>" +
                            "<input type='password' id='change_password_password' value='' />" +

                            "<label for='change_password_confirm_password' id='change_password_confirm_password_lbl'>" +
                                Resources.get('confirm_password_lbl') +
                            "</label>" +
                            "<input type='password' id='change_password_confirm_password' value='' />" +

                            "<input type='button' value='" + Resources.get('ok_btn_caption') +
                            "' id='change_password_ok' data-theme='b' />" +

                        "<div/>";
            return html;
        };

        var setSize = function() {
            scroll(0, 0);
            var top = ($(window).height() - $(change_password_content).outerHeight())/2;
            $(change_password_content).css('top',top + 'px');
        };

        return {
            clear: function() {
                inited = false;
                change_password_container = null;
                change_password = null;
                change_password_content = null;
                change_password_background = null;
                state = null;
                password = null;
                old_password = null;
                confirm_password = null;
                ok = null;
            },
            getState: function() {
                return state;
            },
            getBackground: function() {
                return change_password_background;
            },
            getContainer: function() {
                return change_password_container;
            },
            getPasswordPanel: function() {
                return change_password;
            },
            init: function() {
                if (!inited) {
                    change_password_container = $('#change_password_container');
                    change_password = $('#change_password');
                    change_password_background = $('#change_password_background');
                    change_password.append(change_password_html()).trigger('create');
                    var that = this;
                    password = $('#change_password_password');
                    old_password = $('#change_password_old_password');
                    confirm_password = $('#change_password_confirm_password');
                    ok = $('#change_password_ok');
                    change_password_content = change_password.children('div')[0];
                    ok.click(function() {that.changePassword();});
                    setSize();

                    inited = true;
                };
            },
            changePassword: function() {
                result = true;
                if (confirm_password.val() != password.val()) {
                    app.notify(Resources.get('confirm_password_incorrect'));
                    result = false;
                } else {
                    app.user.changePassword(
                                    old_password.val(),
                                    password.val(),
                                    function () {
                                        ChangePassword.hide();
                                    },
                                    function () {
                                        app.notify(Resources.get('old_password_incorrect'));
                                        result = false;
                                    }
                    );
                }
                return result;
            },
            show: function() {
                if (!inited) {
                    this.init();
                };
                state = 'show';
                password.val('');
                old_password.val('');
                change_password_container.css('display','block');
            },
            hide: function() {
                state = 'hide';
                change_password_container.css('display','none');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
