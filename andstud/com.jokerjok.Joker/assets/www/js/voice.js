Voice = function(_url,_guser,_language) {
    this.timestamp=getDateTime();
    this.system='soundcloud';
    this.url=_url;
    this.guser=_guser;
    this.user_name='';
    this.language=_language;
    this.duration=0;
    this.size=0;
    this.likes=0;
    this.dislikes=0;
    this.likes_sum=0;
    this.id=getTimeStamp();
    this.tags_access=[{'tag':'joke'}];
    this.SM=null;
    this.loading=false;
    this.loaded=false;
    this.onLoad=null;
    this.onFinish=null;
    this.listened=false;
    this.post = function() {
        this.app_account_id = app.user.system.app_account_id;
        this.token = app.user.token;
        var voiceJSON = JSON.stringify(this);
        var voice = this;
        app.ajax(
            '/voices.json',
            'POST',
            voiceJSON,
            function(data) {
                voice.remove();
            },
            function(stat) {
            },
            function(stat, error) {
                //TODO something if not posted
                log('voice not posted ' + stat + ', err ' + error);
            }
        );
    }
    this.addSpamReport = function(callback) {
        if(this.id >0 ) {
            var voice = this;
            var spamReportObj = {
                                token:app.user.token,
                                spam:{
                                    guser:app.user.guser,
                                    voice_id:voice.id
                                }
                          };
            var spamReportJSON = JSON.stringify(spamReportObj);
            app.ajax(
                '/spams.json',
                'POST',
                spamReportJSON,
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    //TODO something if not posted
                    log('spam report not posted ' + stat + ', err ' + error);
                }
            );
        };
    }
    this.addLike = function(like, callback) {
        if(this.id >0 ) {
            var voice = this;
            var likeObj = {
                                token:app.user.token,
                                like:{
                                    guser:app.user.guser,
                                    voice_id:voice.id,
                                    like:like,
                                }
                          };
            var likeJSON = JSON.stringify(likeObj);
            app.ajax(
                '/likes.json',
                'POST',
                likeJSON,
                //TODO check if like was exist or not
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    //TODO something if not posted
                    log('like not posted ' + stat + ', err ' + error);
                }
            );
        };
    }
    this.removeLike = function(callback) {
        if (this.user_like != null) {
            var voice = this;
            var id = this.user_like.id;
            app.ajax(
                '/likes/' + id + '.json?' + 'token=' + app.user.token,
                'DELETE',
                null,
                //{token:app.user.token},
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    log('like not deleted ' + stat + ', err ' + error);
                }
            );
        }
    }
    this.removeSpamReport = function(callback) {
        if (this.user_spam != null) {
            var voice = this;
            var id = this.user_spam.id;
            app.ajax(
                '/spams/' + id + '.json?' + 'token=' + app.user.token,
                'DELETE',
                null,
                //{token:app.user.token},
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    log('spam not deleted ' + stat + ', err ' + error);
                }
            );
        }
    }
    this.cast = function(voice) {
        for (var key in voice) {
            if (voice.hasOwnProperty(key)) {
                var value = voice[key];
                if ((key == 'spams') && (voice[key] == null)) {
                    value = 0;
                };
                this[key] = value;
            };
        }
    }
    this.remove = function() {
        //log('voice remove ' + this.fileStatus);
        this.unload();
        var voiceJSON = JSON.stringify(this);
        var voice = this;
        app.cordova.execute(
            "VoiceRecorder",
            "removeRecord",
            function(data) {
                if(data.status == 1) {
                    log('voice removed');
                    if(voice.url != null && voice.url != '') {
                        app.recorder.removeSavingTask(voice.url);
                    }
                } else {
                    log('record not removed');
                }
            },
            function(err) {
                log('record remove err ' + err);
            },
            [voiceJSON]
        );
    }
    this.addTags = function(tags) {
        var tags_access = this.tags_access;
        $.each(tags, function(index, tag) {
            tags_access.push({'tag':tag});
        });
    }
    this.stop = function() {
        app.player.stop();
    }
    this.deleteVoice = function(callback) {
        app.ajax(
            '/voices/' + this.id + '.json?token=' + app.user.token,
            'DELETE',
            null,
            function(data) {
                if (typeof callback === 'function'){
                    callback();
                }
            },
            function(stat) {
            },
            function(stat, error) {
                log('voice deleteVoice, not deleted ' + stat + ', err ' + error);
            }
        );
    }
    this.getPath = function() {
        var path = '';
        if(this.url != null && this.url != '') {
            path = this.url + '?consumer_key=' + app.user.system.client_id;
        } else {
            path = this.fileLocation;
        }
        return path;
    }
    this.load = function(onFinish, onLoad) {
        if (app.online && (app.user.system != null) && (app.user.system.client_id != null)) {
            if (!this.loaded && !this.loading) {
                this.loading = true;
                var path = this.getPath();
                this.onLoad = onLoad;
                this.onFinish = onFinish;
                var that = this;
                this.SM = soundManager.createSound(
                        {
                            id: 'mySound' + this.id,
                            url: path,
                            autoLoad: true,
                            onload:function(success) {
                                that.loading=false;
                                that.loaded=true;
                                if (typeof that.onLoad === 'function'){
                                    that.onLoad();
                                }
                            },
                            onfinish:function() {
                                if (typeof that.onFinish === 'function'){
                                    that.onFinish();
                                }
                            }
                        }
                );
            };
        };
    }
    this.unload = function() {
        if (this.SM != null) {
            this.SM.destruct();
        };
        this.loaded = false;
        this.loading = false;
        this.SM = null;
    }
}

