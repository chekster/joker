Page = function() {
    return {
        addIconButton: function(prefix,icon,onClickFunction,holder) {
            var id = prefix + '_' + icon + '_button';
            var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');
            iconButton.unbind("click").click(onClickFunction);
            holder.append(iconButton).trigger('create');
            return iconButton;
        }
    }
}
