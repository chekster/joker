var Playlist =(function(){
	var instantiated;
	var init = function() {
        var that = this;
        var voicesList = [];
        var selectParameters = {};
        var currentVoiceIndex = null;
        var sortingTypes = ['rand','best','new','like'];
        var sortingType = 0;
        var countVoicesForSorting = 0;
        var _loadList = function(onLoadHandler) {
            var diff = (countVoicesForSorting - currentVoiceIndex);
            if ( ( diff > 1 ) || (currentVoiceIndex == null) ) {
                app.ajax(
                    '/voices.json?sort=' + sortingTypes[sortingType] + '&v=' + app.version() + '&from=' + voicesList.length,
                    'GET',
                    {guser:app.user.guser,token:app.user.token},
                    function(data) {
                        //If user enter to mainPage Playlist do clear and load voices
                        //Playlist grow as user go with next button
                        if (countVoicesForSorting == 0) {
                            countVoicesForSorting = data.count;
                        };
                        $.each(data.voices, function(index, voice) {
                            var v = new Voice(voice.url,voice.guser,voice.language);
                            v.cast(voice);
                            voicesList.push(v);
                        });
                        //if onLoadHandler function it means loadList called from MainPage onShow
                        if (typeof onLoadHandler === 'function'){
                            currentVoiceIndex = 0;
                            onLoadHandler(voicesList[currentVoiceIndex]);

                            //set loading 2 first voices
                            var curVoice = voicesList[currentVoiceIndex];
                            if (curVoice != null) {
                                curVoice.load(app.ui.mainPage.playButtonClick);
                            };
                            var nextVoice = voicesList[currentVoiceIndex + 1];
                            if (nextVoice != null) {
                                nextVoice.load(app.ui.mainPage.playButtonClick);
                            };
                        };
                        //TODO if voice list empty show message
                    },
                    function(stat) {
                    },
                    function(stat, error) {
                        log('Playlst _loadList ' + stat + '  :  ' + error);
                        /*
                        if (typeof onLoadHandler === 'function'){
                            onLoadHandler(null);
                        };
                        */
                    }
                );
            };
        };

		return {
            loadList: function(onLoadHandler) {
                _loadList(onLoadHandler);
            },
            clearList: function() {
                countVoicesForSorting = 0;
                currentVoiceIndex = null;
                voicesList = [];
            },
            getVoice: function() {
                if(voicesList.length > 0) {
                    return voicesList[currentVoiceIndex];
                }
                return null;
            },
            getVoiceIndex: function() {
                return currentVoiceIndex;
            },
            anyPreviousVoice: function() {
                if(currentVoiceIndex > 0) {
                    return true;
                }
                return false;
            },
            getPreviousVoice: function() {
                if(currentVoiceIndex > 0) {
                    currentVoiceIndex--;
                }
                that = this;
                return that.getVoice();
            },
            anyNextVoice: function() {
                if((voicesList.length - 1) > currentVoiceIndex) {
                    return true;
                }
                return false;
            },
            anyNextNextVoice: function() {
                if((voicesList.length - 2) > currentVoiceIndex) {
                    return true;
                }
                return false;
            },
            getNextVoice: function() {
                if((voicesList.length - 1) > currentVoiceIndex) {
                    currentVoiceIndex++;
                }
                that = this;
                return that.getVoice();
            },
            getNextVoiceWithoutShift: function() {
                if((voicesList.length - 1) > currentVoiceIndex) {
                    return voicesList[currentVoiceIndex + 1];
                }
                return null;
            },
            getPreviousVoiceWithoutShift: function() {
                if(currentVoiceIndex > 0) {
                    return voicesList[currentVoiceIndex - 1];
                }
                return null;
            },
            getNextNextVoiceWithoutShift: function() {
                if((voicesList.length - 2) > currentVoiceIndex) {
                    return voicesList[currentVoiceIndex + 2];
                }
                return null;
            },
            getPreviousPreviousVoiceWithoutShift: function() {
                if(currentVoiceIndex > 1) {
                    return voicesList[currentVoiceIndex - 2];
                }
                return null;
            },
            getSortingTypeCode: function() {
                return sortingType;
            },
            getSortingType: function() {
                return sortingTypes[sortingType];
            },
            setSortingType: function(type) {
                window.localStorage.setItem("sortingType", type);
                sortingType = type;
            },
            voicesSortingCount: function() {
                return countVoicesForSorting;
            },
            voicesCount: function() {
                return voicesList.length;
            }
		}
	};

    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
