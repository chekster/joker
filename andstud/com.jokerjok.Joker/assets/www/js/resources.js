var Resources =(function(){
	var instantiated;

	var init = function() {

        var getArraysResourcesArray = function(keys) {
            app.cordova.execute(
                "ResourcesProvider",
                "getArraysResourcesArray",
                function(data) {
                    arrays = data.value;
                },
                function(err) {
                    log('Resource getArraysResourcesArray array ' + keys + ' loading, err' + JSON.stringify(err));
                },
                keys
            );
        };

        var getStringResourcesArray = function(keys) {
            app.cordova.execute(
                "ResourcesProvider",
                "getStringResourcesArray",
                function(data) {
                    strings = data.value;
                },
                function(err) {
                    log('err on resources ' + keys + ' loading');
                },
                keys
            );
        };


        var inited = false;
        var strings = {};
        var strings_keys = [];
        //settings
        strings_keys.push('backend_url');
        strings_keys.push('recording_limit');
        strings_keys.push('environment');
        strings_keys.push('version');
        //general
        strings_keys.push('app_name');
        strings_keys.push('ok_btn_caption');
        strings_keys.push('general_fault');
        strings_keys.push('not_online_msg');
        //start
        strings_keys.push('login_btn_caption');
        strings_keys.push('login_wo_registration_btn_caption');
        //registration
        strings_keys.push('password_requirements');
        strings_keys.push('name_requirements');
        strings_keys.push('password_lbl');
        strings_keys.push('password_tip_lbl');
        strings_keys.push('old_password_lbl');
        strings_keys.push('new_password_lbl');
        strings_keys.push('confirm_password_lbl');
        strings_keys.push('email_input_lbl');
        strings_keys.push('registration_btn_caption');
        strings_keys.push('name_tip_lbl');
        strings_keys.push('you_already_registered_msg');
        //login
        strings_keys.push('password_incorrect');
        strings_keys.push('login_fault');
        strings_keys.push('registration_fault');
        strings_keys.push('forgot_password_lbl');
        strings_keys.push('fotgot_pass_succes_msg');
        //menu
        strings_keys.push('home_btn_caption');
        strings_keys.push('record_btn_caption');
        strings_keys.push('my_btn_caption');
        strings_keys.push('settings_btn_caption');
        strings_keys.push('exit_btn_caption');
        strings_keys.push('logout_btn_caption');
        //settings
        strings_keys.push('change_password_btn_caption');
        strings_keys.push('name_input_lbl');
        strings_keys.push('language_input_lbl');
        //change password
        strings_keys.push('old_password_incorrect');
        strings_keys.push('confirm_password_incorrect');
        //my voices
        strings_keys.push('likes_caption');
        strings_keys.push('dislikes_caption');
        strings_keys.push('spams_caption');
        strings_keys.push('delete_voice_msg');
        //main
        strings_keys.push('not_registered_cant_rec_msg');
        strings_keys.push('rand_sort_type_btn_caption');
        strings_keys.push('best_sort_type_btn_caption');
        strings_keys.push('new_sort_type_btn_caption');
        strings_keys.push('like_sort_type_btn_caption');
        strings_keys.push('you_dont_have_likes_msg');
        strings_keys.push('do_you_like_something_msg');
        strings_keys.push('is_no_joks_for_query_msg');
        strings_keys.push('tell_me_some_joke_msg');

        var arrays = {};
        var arrays_keys = [];
        arrays_keys.push('languages');
        arrays_keys.push('languagesCodes');
        var load = function() {
            if(!isAndroid()) {
                //TODO remove it from release version
                arrays['languages'] = ['Russian','Hebrew','English'];
                arrays['languagesCodes'] = ['ru','he','en'];
                //settings
                strings['backend_url']='http://localhost:3000';
                strings['recording_limit']='30';
                strings['environment']='production';
                strings['version']='9';
                //general
                strings['app_name']='Joker';
                strings['ok_btn_caption']='Ok';
                strings['general_fault']='Any error. Try later';
                strings['not_online_msg']='You are not online';
                //start
                strings['login_btn_caption']='Login';
                strings['login_wo_registration_btn_caption']='Enter wo registration';
                //registration
                strings['password_requirements']='pass req';
                strings['name_requirements']='name req';
                strings['password_lbl'] = 'password';
                strings['password_tip_lbl'] = 'invent your password';
                strings['old_password_lbl'] = 'old password';
                strings['new_password_lbl'] = 'new password';
                strings['confirm_password_lbl'] = 'confirm password';
                strings['email_input_lbl']='email';
                strings['registration_btn_caption']='Reg';
                strings['name_tip_lbl']='invent your name';
                strings['you_already_registered_msg']='You already registered';
                //login
                strings['password_incorrect']='pass inc';
                strings['login_fault']='log fault';
                strings['registration_fault']='reg fault';
                strings['forgot_password_lbl']='forg pass';
                strings['fotgot_pass_succes_msg']='forg pass succes';
                //menu
                strings['home_btn_caption']='home';
                strings['record_btn_caption']='recording';
                strings['my_btn_caption']='my';
                strings['settings_btn_caption']='settings';
                strings['exit_btn_caption']='exit';
                strings['logout_btn_caption']='logout';
                //settings
                strings['change_password_btn_caption']='change pass';
                strings['name_input_lbl']='name';
                strings['language_input_lbl']='lang';
                //change password
                strings['old_password_incorrect'] = 'old password incorrect';
                strings['confirm_password_incorrect'] = 'confirm password incorrect';
                //my voices
                strings['likes_caption'] = 'likes';
                strings['dislikes_caption'] = 'dislikes';
                strings['spams_caption'] = 'spams';
                strings['delete_voice_msg'] = 'Do you really want to delete this joke?';
                //main
                strings['not_registered_cant_rec_msg'] = 'You`re not reg and can`t rec';
                strings['rand_sort_type_btn_caption'] = 'Shuffle';
                strings['best_sort_type_btn_caption'] = 'Best joks';
                strings['new_sort_type_btn_caption'] = 'New joks';
                strings['like_sort_type_btn_caption'] = 'Joks i liked';
                strings['you_dont_have_likes_msg'] = 'You don`t have likes';
                strings['do_you_like_something_msg'] = 'You generally like anything should';
                strings['is_no_joks_for_query_msg'] = 'There is no joks,';
                strings['tell_me_some_joke_msg'] = 'Tell me some';

            } else {
                getStringResourcesArray(strings_keys);
                getArraysResourcesArray(arrays_keys);
            };
        };

        return {
            init: function(force) {
                if (!inited) {
                    load();
                    inited = true;
                };
            },
            getInited: function() {
                return inited;
            },
            getArrays: function() {
                return arrays;
            },
            getStrings: function() {
                return strings;
            },
            getArray: function(key) {
                return arrays[key];
            },
            get: function(key) {
                return strings[key];
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
