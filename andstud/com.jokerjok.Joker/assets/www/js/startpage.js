var StartPage =(function(){
	var instantiated;

	var init = function() {
        var page = null;
        var goRegistrationButton = null;
        var goLoginButton = null;
        var loginWithoutRegistrationButton = null;
        var content = null;
        var buttonsContainer = null;

        var _setHeight = function() {
            scroll(0, 0);
            var header = page.find('[data-role="header"]');
            var footer = page.find('[data-role="footer"]');
            var viewport_height = $(window).height();
            var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
            content_height -= (content.outerHeight() - content.height());
            content.height(content_height);

            var buttonsTop = (content_height - buttonsContainer.outerHeight())/2;
            buttonsContainer.css('top', buttonsTop + "px");
        };

        return {
            init: function() {
                page = $('#start_page');
                content = $('#start_page_content');
                buttonsContainer = $('#start_page_buttons');
                var that = this;
                goRegistrationButton = $('<a id="go_registration_page" data-role="button" data-inline="true" data-theme="b" class="input_tag">' + Resources.get('registration_btn_caption') + '</a>');
                goRegistrationButton.click(function() {
                    that.goRegistrationButtonClick();
                });

                goLoginButton = $('<a id="go_login_page" data-role="button" data-inline="true" data-theme="b" class="input_tag">' + Resources.get('login_btn_caption') + '</a>');
                goLoginButton.click(function() {
                    that.goLoginButtonClick();
                });

                loginWithoutRegistrationButton = $('<a id="login_wo_registration" data-role="button" data-inline="true" data-theme="b" class="input_tag">' + Resources.get('login_wo_registration_btn_caption') + '</a>');
                loginWithoutRegistrationButton.click(function() {
                    that.loginWoRegistrationButtonClick();
                });

                buttonsContainer.append(goLoginButton).trigger('create');
                buttonsContainer.append('<br>');
                buttonsContainer.append(goRegistrationButton).trigger('create');
                buttonsContainer.append('<br>');
                buttonsContainer.append(loginWithoutRegistrationButton).trigger('create');
                _setHeight();
            },
            setHeight: function() {
                _setHeight();
            },
            loginWoRegistrationButtonClick: function() {
                AjaxLoader.show();
                var email = app.user.emails[0];
                app.user.initSessionWORegistration(
                        email,
                        function() {
                            app.navigateToPage('main_page');
                        },
                        function() {
                            AjaxLoader.hide();
                            //app.notify(Resources.get('login_fault'));
                        }
                );
            },
            goRegistrationButtonClick: function() {
                AjaxLoader.show();
                app.navigateToPage('registration_page');
            },
            goLoginButtonClick: function() {
                AjaxLoader.show();
                app.navigateToPage('login_page');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
