var LanguageSelect =(function(){
	var instantiated;

	var init = function() {
        var inited = false;
        var languagesOptions = '';

        return {
            init: function() {
                if (!inited) {
                    var languages = Resources.getArray('languages');
                    var languagesCodes = Resources.getArray('languagesCodes');
                    var selected = '';
                    jQuery.each(languages, function(i, val) {
                        if (languagesCodes[i] == app.user.language) {
                            selected = 'selected';
                        } else {
                            selected = '';
                        };
                        languagesOptions = languagesOptions +
                                            "<option " + selected + " value='" + languagesCodes[i] + "'>" +
                                                val +
                                            "</option>";
                    });
                    inited = true;
                };


            },
            getOptions: function() {
                if (!inited) {
                    this.init();
                };
                return languagesOptions;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
