UI = function() {
    //this.currentPage=null;
    this.recordingPage=new RecordingPage();
    this.mainPage=new MainPage();
    this.startPage = StartPage;
    this.loginPage = LoginPage;
    this.registrationPage = RegistrationPage;
    this.myVoicesPage = MyVoicesPage;
    this.init = function() {
        this.recordingPage.init();
        this.mainPage.init();
        this.startPage.init();
        this.loginPage.init();
        this.registrationPage.init();
        this.myVoicesPage.init();
    }
    this.refresh = function() {
        if(this.currentPage == this.recordingPage) {
            this.recordingPage.setHeight();
        }
    }
    this.setRecordingPageActive = function() {
        app.recorder.currentVoice = {};
        //this.currentPage = this.recordingPage;
        this.recordingPage.setHeight();
        this.recordingPage.tags.clear();
    }
    this.setMainPageActive = function() {
        this.currentPage = this.mainPage;
        //mainPage.setHeight();
        //mainPage.tags.clear();
    }
}
