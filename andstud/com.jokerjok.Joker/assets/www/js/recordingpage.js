RecordingPage = function() {
    this.page = $('#recording_page');
    this.playButton = null;
    this.saveButton = null;
    this.recordButton = null;
    this.cancelButton = null;
    this.content = $('#recording_page_content');
    this.controlsBar = $('#controls_bar');
    this.timer = Timer();
    this.tags = Tags();
    this.buttonsInited = false;
    this.heightCalculated = false;
    this.setHeight = function() {
        if (!this.heightCalculated) {
            var landscape = !(window.innerHeight > window.innerWidth);
            if(landscape){
                this.controlsBar.attr('data-type',"vertical");
            } else {
                this.controlsBar.attr('data-type',"horizontal");
            }
            this.initButtons();
            scroll(0, 0);
            var header = this.page.find('[data-role="header"]');
            var footer = this.page.find('[data-role="footer"]');
            var content = this.page.find('[data-role="content"]');
            var viewport_height = $(window).height();
            if(landscape){
                var content_height = viewport_height - header.outerHeight();
                content_height -= (content.outerHeight() - content.height());
                content.height(content_height);
                footer.height(window.innerHeight);

                var btnWidth = this.controlsBar.find('.ui-btn').outerWidth();
                this.controlsBar.css('width',btnWidth + 'px');

                var controlsBarTop = (window.innerHeight - this.controlsBar.outerHeight())/2;
                this.controlsBar.css('top',controlsBarTop + 'px');

                var contentWidth = $(window).width() - footer.outerWidth() - (content.outerWidth() - content.width());
                content.width(contentWidth);
            } else {
                this.controlsBar.css('width','');
                this.controlsBar.css('top','');
                content.css('width','');
                footer.css('height','');
                var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
                content_height -= (content.outerHeight() - content.height());
                content.height(content_height);
            }
            this.heightCalculated = true;
        }
    }
    this.initButtons = function() {
        if(!this.buttonsInited && this.controlsBar.length > 0) {
            this.buttonsInited = true;
            var that = this;
            this.controlsBar.empty();
            if(isAndroid()) {
                this.recordButton = this.addIconButton(
                    'record',
                    function onClickEvent() {
                        that.recordingButtonClick();
                    }
                );
                this.playButton = this.addIconButton(
                    'rplay',
                    function onClickEvent() {
                        //log('rplay');
                        that.playingButtonClick();
                    }
                );
                this.saveButton = this.addIconButton(
                    'save',
                    function onClickEvent() {
                        that.setSaved();
                    }
                );
                this.cancelButton = this.addIconButton(
                    'cancel',
                    function onClickEvent() {
                        that.setCancel();
                    }
                );
            } else {
                this.recordButton = this.addIconButton('record',function onClickEvent() {
                    that.recordingButtonClick();});
                this.playButton = this.addIconButton('rplay',function onClickEvent() {});
                this.saveButton = this.addIconButton('save',function onClickEvent() {});
                this.cancelButton = this.addIconButton('cancel',function onClickEvent() {});
            }
            this.playButton.button('disable');
            this.saveButton.button('disable');
        }
    }
    this.clearRecordedVoice = function() {
        app.recorder.currentVoice = {};
        app.ui.recordingPage.setHeight();
        app.ui.recordingPage.tags.clear();


        app.ui.recordingPage.tags.hide();
        app.ui.recordingPage.timer.clear();


        app.ui.recordingPage.playButton.button('disable');
        app.ui.recordingPage.saveButton.button('disable');
    }
    this.init = function() {
        if(this.content.length > 0) {
            this.timer.bind(this.content);
            this.tags.bind(this.content);
            this.tags.hide();
        }
        var that = this;
        this.page.on(
            'pageshow',
            function(event) {
                //TODO it after exit from recording page
                if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                    if(app.recorder.currentVoice.url != null && app.recorder.currentVoice.url != '') {
                    } else {
                        app.recorder.currentVoice.remove();
                    }
                }
                that.clearRecordedVoice();
                AjaxLoader.hide();
            }
        );
    }
    this.setCancel = function() {
        if(app.recorder.recStatus) {
            this.recordingButtonClick();
        } else if(app.player.playStatus) {
            this.playingButtonClick();
        } else if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
            if(app.recorder.currentVoice.url != null && app.recorder.currentVoice.url != '') {
                app.ui.recordingPage.clearRecordedVoice();
            } else {
                app.recorder.currentVoice.remove();
                app.ui.recordingPage.clearRecordedVoice();
            }
        } else {
            app.ui.recordingPage.clearRecordedVoice();
            app.navigateToPage('main_page');
        }
    }
    this.setSaved = function() {
        //TODO also updating from here
        //log('setSaved');
        if(app.player.playStatus) {
            this.playingButtonClick();
        }
        app.recorder.currentVoice.addTags(this.tags.getTags());
        app.recorder.save();
        this.saveButton.button('disable');
        this.playButton.button('disable');
    }
    this.playingButtonClick = function() {
        if(!app.recorder.recStatus) {
            if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                if(!app.player.playStatus) {
                    //log('setPlayingActive');
                    $('.ui-icon-rplay').removeClass("ui-icon-rplay").addClass("ui-icon-rstopplay");
                    app.ui.recordingPage.recordButton.button('disable');
                    app.ui.recordingPage.timer.clear(app.recorder.currentVoice.duration,'down');
                    //app.recorder.play(app.ui.recordingPage.playingButtonClick, app.ui.recordingPage.timer);
                    app.recorder.currentVoice.load(app.ui.recordingPage.playingButtonClick, null);
                    app.player.play(app.recorder.currentVoice, app.ui.recordingPage.timer, null);
                } else {
                    //log('setPlayingStoped');
                    $('.ui-icon-rstopplay').removeClass("ui-icon-rstopplay").addClass("ui-icon-rplay");
                    app.ui.recordingPage.timer.clear(app.recorder.currentVoice.duration,'down');
                    app.ui.recordingPage.recordButton.button('enable');
                    app.player.stop();
                }
            }
        }
    }
    this.recordingButtonClick = function() {
        if(app.recorder.recStatus) {
            //log('setRecordingStoped 0');
            $('.ui-icon-stoprecord').removeClass("ui-icon-stoprecord").addClass("ui-icon-record");
            this.timer.clear(app.recorder.currentVoice.duration,'down');
            app.recorder.stop();
            if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                this.playButton.button('enable');
                if (app.online) { //chek   only if online can save voice
                    this.saveButton.button('enable');
                }
            }
        } else {
            //log('setRecordingActive');
            $('.ui-icon-record').removeClass("ui-icon-record").addClass("ui-icon-stoprecord");
            //this.tags.show(); //TODO open it when i realise search by tags
            this.tags.clear();
            this.timer.clear();
            app.recorder.start(this.timer);
            this.playButton.button('disable');
            this.saveButton.button('disable');
        }
    }
    this.addIconButton = function(icon,onClickFunction) {
        var id = 'recording_' + icon + '_button';
        var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');
        iconButton.unbind("click").click(onClickFunction);
        this.controlsBar.append(iconButton).trigger('create');
        return iconButton;
    }
    this.enablePlayButton = function() {
        if (this.playButton != null) {
            this.playButton.button('enable');
        };
    }
}
