var MyVoicesPage =(function(){
	var instantiated;

	var init = function() {
        var page = null;
        var content = null;
        var content_voices_list = null;
        var controlsBar = null;
        var playButton = null;
        var deleteButton = null;
        var voice = null;
        var timer = null;
        var heightCalculated = false;
        var showed = false;
        var voicesLoaded = false;
        var runnedFunc = null;

        var content_html = function() {
            var html = "";
            return html;
        };

        var _setHeight = function() {
            if (showed && voicesLoaded) {
                if (!heightCalculated) {
                    scroll(0, 0);
                    var header = page.find('[data-role="header"]');
                    var footer = page.find('[data-role="footer"]');
                    var content = page.find('[data-role="content"]');
                    var viewport_height = $(window).height();
                    var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
                    content_height -= (content.outerHeight() - content.height());
                    content.css('height', content_height);
                    content_voices_list.css('height', content.outerHeight());
                    heightCalculated = true;
                };
                AjaxLoader.hide();
            };
        };

        var _setCurrentVoice = function(v) {
            if (showed && voicesLoaded) {
                if (v != null) {
                    if (voice != null) {
                        $('#voice_' + voice.id).removeClass('active');
                    };
                    voice = v;
                    $('#voice_' + voice.id).addClass('active');
                    //timer.clear(voice.duration,'down');
                };
            };
        };

        var _bindMyVoicesList = function() {
            voicesLoaded = true;
            content_voices_list.empty();
            while ( MyVoicesList.anyNextVoice() ){
                _bindVoice(MyVoicesList.getVoice());
                MyVoicesList.getNextVoice();
            }
            var lastVoice = MyVoicesList.getVoice();
            if (lastVoice != null) {
                _bindVoice(lastVoice);
            };
            content_voices_list.listview( "refresh" );
            content_voices_list.children('li').click(_voiceClick);
            _setCurrentVoice(MyVoicesList.getFirstVoice());
            _setHeight();
        };

        var _voiceClick = function() {
            //TODO try here web worker
            _stopPlaying();
            var id = $(this).attr('id').substr(6);
            _setCurrentVoice(MyVoicesList.getVoiceByID(id));
            return false;
        };

        var _calcTimeFromSeconds = function(seconds) {
            var min = Math.floor(seconds/60);
            var sec = (seconds - (min * 60)).toString();
            if(sec.length == 1) {
                sec = '0' + sec;
            }
            return min + ':' + sec;
        };

        var _bindVoice = function(v) {
            var html_tmpl = '<li id="voice_' + v.id + '">' +
                                '<div class="time_box">' +
                                    '<h1>' +
                                        _calcTimeFromSeconds(v.duration) +
                                    '</h1>' +
                                '</div>' +
                                '<div class="votes_box">' +
                                    '<div class="likes">' +
                                        v.likes + ' ' + Resources.get('likes_caption') +
                                    '</div>' +
                                    '<div class="dislikes">' +
                                        v.dislikes + ' ' + Resources.get('dislikes_caption') +
                                    '</div>' +
                                    '<div class="spams">' +
                                        v.spams + ' ' + Resources.get('spams_caption') +
                                    '</div>' +
                                '</div>' +
                            '</li>';
            content_voices_list.append(html_tmpl);
        };

        var _stopPlaying = function() {
            if(app.player.playStatus) {
                $('.ui-icon-pause').removeClass("ui-icon-pause").addClass("ui-icon-play");
                app.player.stop();
            }
        };

        var _playButtonClick = function(onPlayFinish, onLoad) {
            runnedFunc = 'playButtonClick';
            if(voice != null) {
                //timer.clear(voice.duration,'down');
                if(!app.player.playStatus) {
                    $('.ui-icon-play').removeClass("ui-icon-play").addClass("ui-icon-pause");
                    if (voice.SM == null) {
                        voice.load(onPlayFinish, onLoad);
                    };
                    app.player.play(voice, null, onLoad);
                    //voice.play(onPlayFinish, timer, onLoad);
                } else {
                    _stopPlaying();
                }
            } else {
                log('myvoicespage playButtonClick voice is null');
            }
        };

        var _deleleteVoice = function(button) {
            if (button == 1) {
                voice.deleteVoice(_deleleteVoiceCallback);
            };
        };

        var _deleleteVoiceCallback = function() {
            $('#voice_' + voice.id).remove();
            MyVoicesList.removeVoice(voice);
            _setCurrentVoice(MyVoicesList.getVoice());
            //jQuery.scrollTo('#voice_' + voice.id);
            //content_voices_list.scrollTo('#voice_' + voice.id);
            //content_voices_list.scrollTop(0);
            //content.scrollTop(0);
        };

        var _deleleteButtonClick = function() {
            runnedFunc = 'deleteButtonClick';
            if (!app.testing) {
                if(voice != null) {
                    //content.scrollTop(0);
                    //content_voices_list.scrollTop(0);
                    navigator.notification.confirm(
                        Resources.get('delete_voice_msg'),
                        _deleleteVoice,
                        Resources.get('app_name')
                    );
                } else {
                    log('myvoicespage deleteButtonClick voice is null');
                }
            }
        };

        var _addIconButton = function(holder,icon,onClickFunction) {
            var id = 'myvoices_page_' + icon + '_button';
            var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');

            iconButton.unbind("click").click(onClickFunction);
            holder.append(iconButton).trigger('create');
            return iconButton;
        };

        var _initButtons = function() {
            if (playButton == null) {
                playButton = _addIconButton(
                    controlsBar,
                    'play',
                    function onClickEvent() {
                        _playButtonClick(_playButtonClick);
                    }
                );
            };
            if (deleteButton == null) {
                deleteButton = _addIconButton(
                    controlsBar,
                    //TODO change CSS classes to delete instead cancel
                    'cancel',
                    function onClickEvent() {
                        _deleleteButtonClick();
                    }
                );
            };
        };

        var _onShow = function() {
            showed = true;
            //dlya togo chtoby knopki renderilis' krasivo ih nado renderit'
            //posle 'pageshow' kak na recording page i zdes'
            _initButtons();
            _setCurrentVoice(MyVoicesList.getFirstVoice());
            _setHeight();
        };

        return {
            clear: function() {
                if (page != null) {
                    page.unbind("pageshow");
                };
                if (content != null) {
                    content.empty();
                };
                page = null;
                content = null;
                controlsBar = null;
                playButton = null;
                deleteButton = null;
                voice = null;
                timer = null;
                heightCalculated = false;
                showed = false;
                voicesLoaded = false;
            },
            init: function() {
                page = $('#myvoices_page');
                page.on(
                    'pageshow',
                    function(event) {
                        _onShow();
                    }
                );

                content = $('#myvoices_page_content');
                content_voices_list = $('#myvoices_page_list');
                controlsBar = $('#myvoices_page_controls_bar');
                //timer = Timer();
            },
            onShow: function() {
                _onShow();
            },
            show: function() {
                showed = false;
                voicesLoaded = false;
                MyVoicesList.loadList(MyVoicesPage.bindMyVoicesList);
                app.navigateToPage('myvoices_page');
            },
            isShowed: function() {
                return heightCalculated;
            },
            setHeight: function() {
                _setHeight();
            },
            bindMyVoicesList: function() {
                _bindMyVoicesList();
            },
            getPage: function() {
                return page;
            },
            getContent: function() {
                return content;
            },
            getRunnedFunc: function() {
                return runnedFunc;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
