var LoginPage =(function(){
	var instantiated;

	var init = function() {
        var page = null;
        var content = null;
        var footer = null;
        var heightCalculated = false;
        var showed = false;
        var emailInput = null;
        var passwordInput = null;

        var login_html = function() {
            var html = "<div class='space_btw_inputs'>" +
                               "<form id='login_form'>" +

                                   "<label for='login_email' id='login_email_lbl'>"+
                                        Resources.get('email_input_lbl') +
                                   "</label>" +
                                   "<div id='login_email_select_holder' class='input'></div>" +

                                   "<label for='login_password' id='login_password_lbl'>" +
                                        Resources.get('password_lbl') +
                                   "</label>" +
                                   "<input type='password' id='login_password' class='input'/>" +

                                   "<input type='submit' value='" +
                                        Resources.get('login_btn_caption') +
                                    "' id='login_ok' data-theme='b' />" +

                               "</form>" +
                           "<div/>";
            return html;
        };

        var footer_html = function() {
            var html = "<input type='submit' value='" + Resources.get('forgot_password_lbl') +
                       "' id='login_forgot_password' data-theme='b' />";
            return html;
        };

        var _setHeight = function() {
            if (!heightCalculated) {
                scroll(0, 0);
                var header = page.find('[data-role="header"]');
                var footer = page.find('[data-role="footer"]');
                var content = page.find('[data-role="content"]');
                var viewport_height = $(window).height();
                var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
                content_height -= (content.outerHeight() - content.height());
                content.height(content_height);
                heightCalculated = true;
            };
        };

        var _submit = function(succesCallback,errorCallback) {
            var password = passwordInput.val();
            var email = emailInput.val();
            if ( (password == null) || (password.length < 6) || (password.length > 20) ) {
                app.notify(Resources.get('password_incorrect'));
                if (typeof errorCallback === 'function'){
                    errorCallback();
                }
                return;
            };

            AjaxLoader.show();
            app.user.initSession(
                    email,
                    password,
                    function() {
                        //after login navigation
                        if(isAndroid()) {
                            app.navigateToPage('main_page');
                        } else {
                            app.navigateToPage('main_page');
                            //MyVoicesPage.show();
                        }
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    },
                    function() {
                        passwordInput.val('');
                        AjaxLoader.hide();
                        app.notify(Resources.get('login_fault'));
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                    }
            );
        };

        var _forgotPassword = function() {
            var email = emailInput.val();
            AjaxLoader.show();
            app.user.forgotPassword(
                    email,
                    function() {
                        AjaxLoader.hide();
                        app.notify(Resources.get('fotgot_pass_succes_msg'));
                    },
                    function() {
                        AjaxLoader.hide();
                        app.notify(Resources.get('general_fault'));
                    }
            );
        };

        var _onShow = function() {
            if (!showed) {
                _setHeight();
            }

            AjaxLoader.hide();
            showed = true;
        };

        return {
            init: function() {
                page = $('#login_page');
                page.on(
                    'pageshow',
                    function(event) {
                        _onShow();
                    }
                );
                content = $('#login_page_content');
                footer = $('#login_page_footer');
                content.append(login_html()).trigger('create');
                footer.append(footer_html()).trigger('create');
                var that = this;
                $('#login_ok').unbind("click").click(function() {that.submit();});
                //$('#login_form').unbind("submit").submit(function() {that.submit();});
                $('#login_forgot_password').unbind("click").click(function() {that.forgotPassword();});

                var emailsOptions = '';
                jQuery.each(app.user.emails, function(i, val) {
                    emailsOptions = emailsOptions + "<option value='" + val + "'>" + val + "</option>";
                });
                var select = '<select id="login_email">' + emailsOptions + '</select>';
                $('#login_email_select_holder').append($(select)).trigger('create');

                emailInput = $('#login_email');
                passwordInput = $('#login_password');
            },
            forgotPassword: function() {
                _forgotPassword();
            },
            submit: function(succesCallback,errorCallback) {
                _submit(succesCallback,errorCallback);
            },
            setHeight: function() {
                _setHeight();
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
