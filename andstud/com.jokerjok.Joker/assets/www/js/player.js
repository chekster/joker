Player = function() {
    this.voice = null;
    this.playStatus = false;
    this.IntervalId = 0;
    this.play = function(voice, _timer, onLoad) {
        if (!this.playStatus) {
            this.voice = voice;
            if (this.voice.SM == null) {
                this.voice.load();
            };
            if (true) { //(this.voice.SM.loaded) {
                this.playStatus = true;
                if (onLoad != null) {
                    if (typeof onLoad === 'function'){
                        onLoad();
                    }
                }
                //Start timer after 1 msec playing
                if (this.voice.SM != null) {
                    if (_timer != null) {
                        this.voice.SM.onPosition(1, function(eventPosition) {
                            app.player.IntervalId = setInterval(_timer.tick,1000);
                        });
                        this.voice.SM.play();
                    };
                };
                this.voice.listened = true;
            }
        };
    }
    this.stop = function() {
        if (this.playStatus) {
            if (this.voice.SM != null) {
                this.voice.SM.stop();
                this.voice.SM.clearOnPosition(1);
            };
            this.playStatus = false;
            if (app.player.IntervalId != 0) {
                clearInterval(app.player.IntervalId);
                app.player.IntervalId = 0;
            };
        }
    }
}
