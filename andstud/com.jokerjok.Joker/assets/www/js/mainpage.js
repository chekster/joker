MainPage = function() {
    var page = null;
    var goRecordingButton = null;
    var sortTypeButton = null;
    var sortTypeButtonText = null;
    var playButton = null;
    var nextButton = null;
    var previousButton = null;
    var likeButton = null;
    var unlikeButton = null;
    var spamButton = null;
    var likeCounter = null;
    var unlikeCounter = null;
    var spamCounter = null;
    var voice = null;
    var content = null;
    var timerBar = null;
    var sortBar = null;
    var countBar = null;
    var navigationBar = null;
    var controlsBar = null;
    var voteBar = null;
    var countersBar = null;
    var timer = null;

    var runnedFunc = null;
    var heightCalculated = false;
    var showed = false;
    var needRefresh = true;

    var _bind = function() {
        var that = this;
        timer.bind(timerBar);
        goRecordingButton = $('<a id="go_recording_page" data-role="button" data-theme="b">' + Resources.get('record_btn_caption') + '</a>');
        goRecordingButton.click(function() {
            _goRecordingButtonClick();
        });
        navigationBar.append(goRecordingButton).trigger('create');


        sortTypeButton = $('<a id="main_page_sort_button" data-role="button" data-theme="b">' + Resources.get(Playlist.getSortingType() + '_sort_type_btn_caption') + '</a>');
        sortTypeButton.click(function() {
            _sortTypeButtonClick();
        });
        sortBar.append(sortTypeButton).trigger('create');
        sortTypeButtonText = sortTypeButton.find('.ui-btn-text').first();

        //Rendering of play, next, previous buttons are in _onShow function
        spamButton = _addIconButton(
            voteBar,
            'spam',
            function onClickEvent() {
                _spamButtonClick();
            }
        );
        unlikeButton = _addIconButton(
            voteBar,
            'unlike',
            function onClickEvent() {
                _unlikeButtonClick();
            }
        );
        likeButton = _addIconButton(
            voteBar,
            'like',
            function onClickEvent() {
                _likeButtonClick();
            }
        );
        spamCounter = _addCounter(countersBar,'spam');
        unlikeCounter = _addCounter(countersBar,'unlike');
        likeCounter = _addCounter(countersBar,'like');
    };

    var _addCounter = function(holder,icon) {
        var id = 'main_page' + '_' + icon + '_counter';
        var counter = $('<div class="voteCounter" id="' + id + '" />');

        holder.append(counter);
        return counter;
    };

    var _addIconButton = function(holder,icon,onClickFunction) {
        var id = 'main_page' + '_' + icon + '_button';
        var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');

        iconButton.unbind("click").click(onClickFunction);
        holder.append(iconButton).trigger('create');
        return iconButton;
    };

    var _stopPlaying = function() {
        if(app.player.playStatus) {
            $('.ui-icon-pause').removeClass("ui-icon-pause").addClass("ui-icon-play");
            app.player.stop();
        }
    };

    var _previousButtonClick = function() {
        runnedFunc = 'previousButtonClick';
        _stopPlaying();
        _setCurrentVoice(Playlist.getPreviousVoice());
        //load next voice
        previousVoice = Playlist.getPreviousVoiceWithoutShift();
        if (previousVoice != null) {
            previousVoice.load(_playButtonClick, null);
        };
        //unload second next voice
        nextNextVoice = Playlist.getNextNextVoiceWithoutShift();
        if (nextNextVoice != null) {
            nextNextVoice.unload();
        };
    };

    var _playButtonClick = function(onPlayFinish, onLoad) {
        runnedFunc = 'playButtonClick';
        if(voice != null) {
            timer.clear(voice.duration,'down');
            if(!app.player.playStatus) {
                $('.ui-icon-play').removeClass("ui-icon-play").addClass("ui-icon-pause");
                app.player.play(voice, timer, onLoad);
            } else {
                _stopPlaying();
            }
        } else {
            log('main page playButtonClick voice is null');
        }
    };

    var _nextButtonClick = function() {
        runnedFunc = 'nextButtonClick';
        _stopPlaying();
        _setCurrentVoice(Playlist.getNextVoice());
        //load next voice
        nextVoice = Playlist.getNextVoiceWithoutShift();
        if (nextVoice != null) {
            nextVoice.load(_playButtonClick, null);
        };
        //unload second previous voice
        previousPreviousVoice = Playlist.getPreviousPreviousVoiceWithoutShift();
        if (previousPreviousVoice != null) {
            previousPreviousVoice.unload();
        };
    };

    var _spamButtonClick = function() {
        runnedFunc = 'spamButtonClick';
        if(voice != null) {
            if (voice.user_spam != null) {
                voice.removeSpamReport(app.ui.mainPage.setVotesButtonsState);
            } else {
                voice.addSpamReport(app.ui.mainPage.setVotesButtonsState);
            }
        }
    };

    var _unlikeButtonClick = function() {
        runnedFunc = 'unlikeButtonClick';
        if(voice != null) {
            if (voice.user_like != null) {
                if (!voice.user_like.like) {
                    voice.removeLike(app.ui.mainPage.setVotesButtonsState);
                } else {
                    voice.addLike(false, app.ui.mainPage.setVotesButtonsState);
                }
            } else {
                voice.addLike(false, app.ui.mainPage.setVotesButtonsState);
            }
        }
    };

    var _likeButtonClick = function() {
        runnedFunc = 'likeButtonClick';
        if(voice != null) {
            if (voice.user_like != null) {
                if (voice.user_like.like) {
                    voice.removeLike(app.ui.mainPage.setVotesButtonsState);
                } else {
                    voice.addLike(true, app.ui.mainPage.setVotesButtonsState);
                }
            } else {
                voice.addLike(true, app.ui.mainPage.setVotesButtonsState);
            }
        }
    };

    var _setHeight = function() {
        if (!heightCalculated) {
            scroll(0, 0);
            var header = page.find('[data-role="header"]');
            var footer = page.find('[data-role="footer"]');
            var content = page.find('[data-role="content"]');
            var viewport_height = $(window).height();
            var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
            content_height -= (content.outerHeight() - content.height());
            content.height(content_height);
            heightCalculated = true;
        };
    };

    var _setCurrentVoice = function(v) {
        voice = v;
        if (v != null) {
            timer.getTimerContainer().removeClass("small_font");
            nameBar.addClass("large_font");
            nameBar.removeClass("medium_font");
            nameBar.removeClass("shift");
            timerBar.removeClass("shift");

            timer.clear(voice.duration,'down');
            nameBar.text(voice.user_name);
            countBar.text((Playlist.getVoiceIndex() + 1) + '/' + Playlist.voicesSortingCount());
        } else {
            //TODO zapisat' special'nyi golos kotoryj but vozvrawatsya serverom esli on nichego ne nawel
            //TODO predlozhit' ih zapisat' chuwaku s jokerovskim golosom
            //
            countBar.text('');

            timer.getTimerContainer().addClass("small_font");
            nameBar.removeClass("large_font");
            nameBar.addClass("medium_font");
            nameBar.addClass("shift");
            timerBar.addClass("shift");

            //sort by current user likes
            if (Playlist.getSortingTypeCode() == 3) {
                timer.text(Resources.get('do_you_like_something_msg'));
                nameBar.text(Resources.get('you_dont_have_likes_msg'));
            } else {
                timer.text(Resources.get('is_no_joks_for_query_msg'));
                nameBar.text(Resources.get('tell_me_some_joke_msg'));
            };
        };
        if (!Playlist.anyNextNextVoice()) {
            Playlist.loadList();
        }
        _setButtonsState();
        AjaxLoader.hide();
    };

    var _setVotesButtonsState = function() {
        likeButton.buttonMarkup({theme: 'c'});
        unlikeButton.buttonMarkup({theme: 'c'});
        spamButton.buttonMarkup({theme: 'c'});

        if (voice == null) {
            voteBar.addClass('invisible');
            countersBar.addClass('invisible');
        } else {
            voteBar.removeClass('invisible');
            countersBar.removeClass('invisible');

            spamCounter.text(voice.spams);
            unlikeCounter.text(voice.dislikes);
            likeCounter.text(voice.likes);

            if ( (voice != null) && ( (voice.user_like != null) || (voice.user_spam != null) || voice.listened ) ) {
                _enableVote();

                if (voice.user_like != null) {
                    if (voice.user_like.like) {
                        likeButton.buttonMarkup({theme: 'b'});
                    } else {
                        unlikeButton.buttonMarkup({theme: 'b'});
                    };
                };
                if (voice.user_spam != null) {
                    spamButton.buttonMarkup({theme: 'b'});
                };
            } else {
               _disableVote();
            };
        };
    };

    var _enableVote = function(enable) {
        likeButton.button('enable');
        unlikeButton.button('enable');
        spamButton.button('enable');
    };

    var _disableVote = function(enable) {
        likeButton.button('disable');
        unlikeButton.button('disable');
        spamButton.button('disable');
    };

    var _setButtonsState = function() {
        _setVotesButtonsState();
        if( !app.player.playStatus && (voice != null) ) {
            playButton.button('enable');
        } else {
            playButton.button('disable');
        }
        if (Playlist.anyNextVoice() && (voice != null) ) {
            nextButton.button('enable');
        } else {
            nextButton.button('disable');
        }
        if (Playlist.anyPreviousVoice() && (voice != null) ) {
            previousButton.button('enable');
        } else {
            previousButton.button('disable');
        }
    };

    var _goRecordingButtonClick = function() {
        runnedFunc = 'goRecordingButtonClick';
        _stopPlaying();

        if (app.user.registered) {
            AjaxLoader.show();
            /*
            if (!app.ui.recordingPage.heightCalculated) {
                AjaxLoader.show();
            }
            */
            app.navigateToPage('recording_page');
        } else {
            app.notify(Resources.get('not_registered_cant_rec_msg'));
        };
    };

     var _sortTypeButtonClick = function() {
        runnedFunc = 'sortTypeButtonClick';
        _stopPlaying();
        Sorting.show(app.ui.mainPage.setCurrentSortingType);
    }

    var _onShow = function() {
        //dlya togo chtoby knopki renderilis' krasivo ih nado renderit'
        //posle 'pageshow' kak na recording page i zdes'
        if (needRefresh) {
            Playlist.clearList();
            Playlist.loadList(app.ui.mainPage.setCurrentVoice);
            needRefresh = false;
        };
        if (!showed) {
            previousButton = _addIconButton(
                controlsBar,
                'previous',
                function onClickEvent() {
                    _previousButtonClick();
                }
            );
            playButton = _addIconButton(
                controlsBar,
                'play',
                function onClickEvent() {
                    _playButtonClick(_playButtonClick, _enableVote);
                }
            );
            nextButton = _addIconButton(
                controlsBar,
                'next',
                function onClickEvent() {
                    _nextButtonClick();
                }
            );
            _setHeight();
        } else {
            AjaxLoader.hide();
        };
        showed = true;
    };

    return {
        init: function() {
            page = $('#main_page');
            page.on(
                'pageshow',
                function(event) {
                    _onShow();
                }
            );
            content = $('#main_page_content');
            sortBar = $('#main_page_sort_bar');
            countBar = $('#main_page_count_bar');
            navigationBar = $('#navigation_bar');
            voteBar = $('#main_page_vote_bar');
            timerBar = $('#main_page_timer_bar');
            controlsBar = $('#main_page_controls_bar');
            nameBar = $('#main_page_name_bar');
            countersBar = $('#main_page_votes_counters_bar>div');
            timer = Timer();

            if (window.localStorage.getItem("sortingType") != null) {
                Playlist.setSortingType(window.localStorage.getItem("sortingType"));
            };

            _bind();
        },
        bind: function() {
            _bind();
        },
        setHeight: function() {
            _setHeight();
        },
        sortTypeButtonClick: function() {
            return _sortTypeButtonClick;
        },
        goRecordingButtonClick: function() {
            return _goRecordingButtonClick;
        },
        previousButtonClick: function() {
            _previousButtonClick();
        },
        playButtonClick: function() {
            that = this;
            _playButtonClick(that.playButtonClick, that.enableVote);
        },
        enableVote: function() {
            _enableVote();
        },
        setVotesButtonsState: function(enable) {
            _setVotesButtonsState(enable);
        },
        nextButtonClick: function() {
            _nextButtonClick();
        },
        setCurrentVoice: function(voice) {
            _setCurrentVoice(voice);
        },
        setButtonsState: function(voice) {
            _setButtonsState(voice);
        },
        spamButtonClick: function() {
            _spamButtonClick();
        },
        unlikeButtonClick: function() {
            _unlikeButtonClick();
        },
        likeButtonClick: function() {
            _likeButtonClick();
        },
        getVoice: function() {
            return voice;
        },
        getTimer: function() {
            return timer;
        },
        clearLoadVoicesAndSetCurrentVoice: function() {
            Playlist.clearList();
            Playlist.loadList(app.ui.mainPage.setCurrentVoice);
        },
        setCurrentSortingType: function() {
            sortTypeButtonText.text(Resources.get(Playlist.getSortingType() + '_sort_type_btn_caption'));
            app.ui.mainPage.clearLoadVoicesAndSetCurrentVoice();
        },
        needRefresh: function() {
            needRefresh = true;
        },
        getRunnedFunc: function() {
            return runnedFunc;
        }

    }
}

