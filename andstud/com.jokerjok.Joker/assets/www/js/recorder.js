Recorder = function(lim) {
    this.recordingLimit = lim;
    this.recordingIntervalId = 0;

    this.recStatus = false;
    this.currentVoice = {};
    this.voicesSavings = [];
    this.savingsRunnig = false;
    this.savingsTimerId;
    this.startSavings = function() {
        if(!this.savingsRunnig) {
            this.savingsRunnig = true;
            this.savingsTimerId = setInterval(
                this.checkSavings,
                5000
            );
        }
    }
    this.removeSavingTask = function(voice_url) {
        app.recorder.voicesSavings = jQuery.grep(app.recorder.voicesSavings, function(voice) {
            return voice.url != voice_url;
        });
        if (app.recorder.currentVoice.url == voice_url) {
            //TODO obnulyat' currentVoice posle sohraneniya
            app.ui.recordingPage.enablePlayButton();
        };
        if(app.recorder.voicesSavings.length == 0) {
            app.recorder.stopSavingTasks();
        }
    }
    this.looper = 0;
    this.stopSavingTasks = function() {
        clearInterval(this.savingsTimerId)
        this.savingsRunnig = false;
        this.looper = 0;
    }
    this.checkSavings = function() {
        app.recorder.looper = app.recorder.looper + 1;
        if(app.recorder.looper > 20) {
            app.recorder.stopSavingTasks()
        }
        if(app.recorder.voicesSavings.length == 0) {
            app.recorder.stopSavingTasks()
        }
        $.each(app.recorder.voicesSavings, function(index, voice) {
            voice.unload();
            var voiceJSON = JSON.stringify(voice);
            app.cordova.execute(
                "VoiceRecorder",
                "checkRecordStatus",
                function(data) {
                    if(data.status == 1) {
                        if(data.voice.url != null && data.voice.url != '') {
                            voice.url = data.voice.url;
                            voice.link = data.voice.link;
                            voice.size = data.voice.size;
                            voice.post();
                        }
                    } else {
                        log('record not checked');
                    }
                },
                function(err) {
                    log('check err ' + err);
                },
                [voiceJSON]
            );
        });
    }
    this.timerTick = function() {
        app.recorder.currentVoice.duration++;
        if(app.recorder.currentVoice.duration >= app.recorder.recordingLimit) {
            app.ui.recordingPage.recordingButtonClick();
        }
    }
    this.start = function(timer) {
        if(!this.recStatus) {
            if(!jQuery.isEmptyObject(this.currentVoice)) {
                //Here was this.currentVoice.clear(); and it was call for bug for multiple recodings
                this.currentVoice = null;
                app.ui.recordingPage.tags.clear();
            }
            this.currentVoice = new Voice('',app.user.guser,app.user.language);
            var voiceJSON = JSON.stringify(this.currentVoice);
            app.cordova.execute(
                "VoiceRecorder",
                "startRecording",
                function(statObj) {
                    if( (statObj != null)
                        && (statObj.voice != null)
                        && (statObj.voice.fileStatus != null)
                        && (statObj.voice.fileStatus != '') ) {
                        app.recorder.recordingIntervalId = setInterval(
                            function() {
                                app.recorder.timerTick();
                                if (timer != null) {
                                    timer.tick(app.recorder.currentVoice.duration);
                                }
                            },
                            1000
                        );
                        for (var key in statObj.voice) {
                            if (statObj.voice.hasOwnProperty(key)) {
                                app.recorder.currentVoice[key] = statObj.voice[key]
                            }
                        }
                        app.recorder.recStatus = true;
                    } else {
                        log('not started');
                        //TODO throw some message, recording not started
                        //proverit' prepare
                    }
                },
                function(err) {
                    log('Recorder start err ' + JSON.stringify(err));
                },
                [voiceJSON]
            );
        }
    }
    this.stop = function() {
        if(this.recStatus) {
            app.cordova.execute(
                "VoiceRecorder",
                "stopRecording",
                function(data) {
                    if(data.status == 1) {
                        clearInterval(app.recorder.recordingIntervalId);
                        app.recorder.recStatus = false;
                    } else {
                        log('not stoped');
                    }
                },
                function(err) {
                    log('stop err ' + err);
                },
                []
            );
        }
    }
    this.save = function() {
        if (app.online) {
            if(!this.recStatus) {
                if(!jQuery.isEmptyObject(this.currentVoice)) {
                    app.user.initSCToken();
                    setTimeout(
                        function() {
                            app.recorder.checkSCTokenInitAndThenSave();
                        },
                        1000
                    );
                }
            }
        }
    }
    this.checkSCTokenInitAndThenSave =function() {
        if (app.user.tokenSCInited) {
            this.saveCurrentVoice();
        } else {
            setTimeout(
                function() {
                    app.recorder.checkSCTokenInitAndThenSave();
                },
                1000
            );
        }
    }
    this.saveCurrentVoice =function() {
        app.recorder.currentVoice.unload();
        app.recorder.currentVoice.user_name = app.user.name;
        var voiceJSON = JSON.stringify(app.recorder.currentVoice);
        app.cordova.execute(
            "VoiceRecorder",
            "saveRecording",
            function(data) {
                if(data.status == 1) {
                    //TODO mozhet byt' eto vynesti do execute t.k. currentVoice mozhet byt' uzhe pustym
                    app.recorder.voicesSavings.push(app.recorder.currentVoice);
                    app.recorder.startSavings();
                } else {
                    log('saveCurrentVoice not started');
                }
            },
            function(err) {
                log('saveCurrentVoice err ' + err);
            },
            [voiceJSON]
        );
    }
    this.deleteAllRecordingsFiles = function() {
        //log('deleteAllRecordings');
        app.cordova.execute(
            "VoiceRecorder",
            "deleteAllRecordings",
            function(data) {
                if(data.status == 1) {
                    //log('all recordings deleted');
                } else {
                    log('all recordings are not deleted');
                }
            },
            function(err) {
                log('delete all recordings err ' + err);
            },
            []
        );
    }
}
