User = function() {
    this.session = false;
    this.language = null;
    this.guser = null;
    this.password = null;
    this.emails = null;
    this.tokenSCInited = false;
    this.registered = false;
    this.init = function() {
        this.getLanguage();
        this.getId();
        this.getEmails();
        //TODO dobavit' token kak svoistvo iznachal'no
        this.token = window.localStorage.getItem("token");
        this.guser = window.localStorage.getItem("guser");
        if(isAndroid()) {
            if(app.online) {
                this.initSessionByToken();
            };
        };
    }
    this.create = function(succesCallback,errorCallback) {
        if (this.validate()) {
            var that = this;
            var user_json = JSON.stringify(this);
            app.ajax('/users.json',
                    'POST',
                    user_json,
                    function(data) {
                        app.user.password = that.password;
                        app.user.guser = that.email;
                        app.user.language = that.language;
                        app.user.name = that.name;
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    },
                    function(stat) {
                    },
                    function(stat, error) {
                        if ( (stat == 422) || (stat == '422') ) {
                            app.notify(Resources.get('registration_fault'));
                        } else
                        if ( (stat == 409) || (stat == '409') ) {
                            app.notify(Resources.get('you_already_registered_msg'));
                        };
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                        log('Error on user create ' + stat + ', errors ' + error);
                    }
            );
        } else {
            if (typeof errorCallback === 'function'){
                errorCallback();
            }
        };
    }
    this.validate = function() {
        var valid = false;
        if( (this.guser != undefined) && (this.guser.length > 0) ) {
            if( (this.language != undefined) && (this.language.length > 0) ) {
                if( (this.password != null) && (this.password.length > 5) ) {
                    if( (this.name != null) && (this.name.length > 2) ) {
                        valid = true;
                    } else {
                        log('name no valid');
                    }
                } else {
                    log('password no valid');
                }
            } else {
                log('language no valid');
            }
        } else {
            log('guser no valid');
        }
        return valid;
    }
    this.changePassword = function(oldPassword,newPassword,succesCallback,errorCallback) {
        if ( (newPassword == null) || (newPassword.length < 6) || (newPassword.length > 20) ) {
            app.notify(Resources.get('password_requirements'));
            return;
        };
        if ( (oldPassword == null) || (oldPassword.length < 6) || (oldPassword.length > 20) ) {
            return;
        };

        app.ajax('/users/change_password.json',
                'POST',
                '{"token":"' + app.user.token + '",'+
                 '"oldpassword":"' + oldPassword + '",'+
                 '"newpassword":"' + newPassword + '"}',
                function(data) {
                    if ( data.result ) {
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    } else {
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    if (typeof errorCallback === 'function'){
                        errorCallback();
                    }
                    log('Error on user changePassword ' + stat + ', errors ' + error);
                }
        );
    }
    this.save = function(succesCallback,errorCallback) {
        if( (this.language != undefined) &&
            (this.language.length > 0) &&
            (this.name != null) &&
            (this.name.length > 2) ) {
            app.ajax('/users/0.json',
                    'PUT',
                    '{"token":"' + app.user.token + '",'+
                     '"language":"' + app.user.language + '",'+
                     '"name":"' + app.user.name + '"}',
                    function(data) {
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    },
                    function(stat) {
                    },
                    function(stat, error) {
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                        log('Error on user save ' + stat + ', errors ' + error);
                    }
            );
        } else {
            log('name or language no valid');
            if (typeof errorCallback === 'function'){
                errorCallback();
            }
        }
    }
    this.forgotPassword = function(email,succesCallback,errorCallback) {
        var forgot_json = {
            guser: email
        };
        var user_json = JSON.stringify(forgot_json);
        app.ajax('/users/forgot_password.json',
                'POST',
                user_json,
                function(data) {
                    if (data.result) {
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    } else {
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    if (typeof errorCallback === 'function'){
                        errorCallback();
                    }
                }
        );
    }
    this.initSession = function(email,password,succesCallback,errorCallback) {
        if (this.session != true) {
            if ( (email != null) && (password != null) ) {
                this.guser = email;
                this.password = password;
            }
            var user_json = JSON.stringify(app.user);
            this.requestInitSession(user_json,succesCallback,errorCallback);
        } else {
            if (typeof errorCallback === 'function'){
                errorCallback();
            }
        }
    }
    this.initSessionByToken = function() {
        if (this.token != null) {
            AjaxLoader.show();
            if(isAndroid()) {
                this.initSession(null,
                                null,
                                function() {app.navigateToPage('main_page');},
                                function() {AjaxLoader.hide();});
            } else {
                this.initSession(null,
                                null,
                                function() {app.navigateToPage('main_page');},
                                function() {AjaxLoader.hide();});
            };
        };
    }
    this.initSessionWORegistration = function(email,succesCallback,errorCallback) {
        if (this.session != true) {
            if ( email != null ) {
                this.guser = email;
            }
            var user_json = JSON.stringify(app.user);
            this.requestInitSession(user_json,succesCallback,errorCallback,true);
        } else {
            if (typeof errorCallback === 'function'){
                errorCallback();
            }
        }
    }
    this.requestInitSession = function(user_json, succesCallback, errorCallback, wo_registration) {
        app.ui.mainPage.needRefresh();
        var wo_registration_param = '';
        if (wo_registration != null) {
          wo_registration_param = '&wor=1';
        };
        app.ajax('/users/init_session.json?v=' + app.version() + wo_registration_param,
                'POST',
                user_json,
                function(data) {
                    if (data.result) {
                        app.user.system = {};
                        app.cordova.execute(
                            "UserData",
                            "getClientID",
                            function(data) {
                                app.user.system.client_id = data.client_id;
                                app.user.system.app_account_id = data.app_account_id;
                            },
                            function(err) {
                            },
                            [data.num_arr]
                        );
                        app.user.token = data.token;
                        app.user.language = data.language;
                        app.user.session = true;
                        if (data.name != null) {
                            app.user.name = data.name;
                            app.user.registered = true;
                        } else {
                            app.user.name = null;
                            app.user.registered = false;
                        };
                        if (wo_registration == null) {
                            window.localStorage.setItem("token", app.user.token);
                            window.localStorage.setItem("guser", app.user.guser);
                        }
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        }
                    } else {
                        app.user.token = null;
                        window.localStorage.removeItem("token");
                        window.localStorage.removeItem("guser");
                        window.localStorage.removeItem("sortingType");
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    app.user.token = null;
                    window.localStorage.removeItem("token");
                    window.localStorage.removeItem("guser");
                    window.localStorage.removeItem("sortingType");
                    if (typeof errorCallback === 'function'){
                        errorCallback();
                    }
                }
        );
    }
    this.logout = function(succesCallback,errorCallback) {
        if (app.user.token != null) {
            app.ajax('/users/logout.json',
                    'GET',
                    'token='+app.user.token,
                    function(data) {
                        if (data.result) {
                            window.localStorage.removeItem("token");
                            window.localStorage.removeItem("guser");
                            window.localStorage.removeItem("sortingType");
                            app.user.token = null;
                            app.user.session = false;
                            app.user.system = null;
                            if (typeof succesCallback === 'function'){
                                succesCallback();
                            }
                        } else {
                            app.user.token = null;
                            window.localStorage.removeItem("token");
                            window.localStorage.removeItem("guser");
                            window.localStorage.removeItem("sortingType");
                            if (typeof errorCallback === 'function'){
                                errorCallback();
                            }
                        }
                    },
                    function(stat) {
                    },
                    function(stat, error) {
                        app.user.token = null;
                        window.localStorage.removeItem("token");
                        window.localStorage.removeItem("guser");
                        window.localStorage.removeItem("sortingType");
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                    }
            );
        } else {
            app.user.token = null;
            window.localStorage.removeItem("token");
            window.localStorage.removeItem("guser");
            window.localStorage.removeItem("sortingType");
            if (typeof errorCallback === 'function'){
                errorCallback();
            }
        }
    }
    this.initSCToken = function(succesCallback, faultCallback) {
        //TODO callbacks for succes and fault and tests for it
        app.user.tokenSCInited = false;
        app.cordova.execute(
            "VoiceRecorder",
            "initSCToken",
            function(stat) {
                if(stat.status == 1) {
                    app.user.tokenSCInited = true;
                    if (typeof succesCallback === 'function'){
                        succesCallback();
                    }
                } else {
                    if (typeof faultCallback === 'function'){
                        faultCallback();
                    }
                    log('user initSCToken token not started');
                }
            },
            function(err) {
                if (typeof faultCallback === 'function'){
                    faultCallback();
                }
                log('user initSCToken token getting err ' + err);
            },
            []
        );
    }
    this.getLanguage = function() {
        if(!isAndroid()) {
            app.user.language = 'ru';
        } else {
            app.cordova.execute(
                "UserData",
                "getLanguage",
                function(data) {
                    app.user.language = data.language;
                },
                function(err) {
                    log('User can get languge from device');
                },
                []
            );
        }
    }
    this.getId = function() {
        if(!isAndroid()) {
            app.user.guser = 'chekst@gmail.com';
            //app.user.guser = 'someemail@gmail.com';
        } else {
            app.cordova.execute(
                "UserData",
                "getId",
                function(data) {
                    app.user.guser = data.id;
                    //app.user.initSession();
                },
                function(err) {},
                []
            );
        }
    }
    this.getEmails = function() {
        if(!isAndroid()) {
            app.user.emails = ['someemail@gmail.com','chekst@gmail.com'];
        } else {
            app.cordova.execute(
                "UserData",
                "getEmails",
                function(data) {
                    app.user.emails = data.emails;
                },
                function(err) {},
                []
            );
        }
    }
}
