var LoginPage =(function(){
	var instantiated;

	var init = function() {
        var runnedFunc = null;

        var page = null;
        var content = null;
        var footer = null;
        var header = null;
        var heightCalculated = false;
        var showed = false;
        var emailInput = null;
        var passwordInput = null;
        var menuButton = null;

        var login_html = function() {
            var html = "<div class='space_btw_inputs'>" +
                               "<form id='login_form'>" +

                                   "<label for='login_email' id='login_email_lbl'>"+
                                        Resources.get('email_input_lbl') +
                                   "</label>" +
                                   "<div id='login_email_select_holder' class='input'></div>" +

                                   "<label for='login_password' id='login_password_lbl'>" +
                                        Resources.get('password_lbl') +
                                   "</label>" +
                                   "<input type='password' id='login_password' class='input'/>" +

                                   "<input type='submit' value='" +
                                        Resources.get('login_btn_caption') +
                                    "' id='login_ok' data-theme='b' />" +

                               "</form>" +
                           "<div/>";
            return html;
        };

        var footer_html = function() {
            var html = "<input type='submit' value='" + Resources.get('forgot_password_lbl') +
                       "' id='login_forgot_password' data-theme='b' />";
            return html;
        };

        var _submit = function() {
            runnedFunc = 'submit';
            var password = passwordInput.val();
            var email = emailInput.val();
            if ( (password == null) || (password.length < 6) || (password.length > 20) ) {
                app.notify(Resources.get('password_incorrect'));
                return;
            };

            AjaxLoader.show();
            app.user.initSession(
                    email,
                    password,
                    function() {
                        //after login navigation
                        app.navigateToPage('main_page');
                    },
                    function() {
                        passwordInput.val('');
                        AjaxLoader.hide();
                        app.notify(Resources.get('login_fault'));
                    }
            );
        };

        var _forgotPassword = function() {
            runnedFunc = 'forgotPassword';
            var email = emailInput.val();
            AjaxLoader.show();
            app.user.forgotPassword(
                    email,
                    function() {
                        AjaxLoader.hide();
                        app.notify(Resources.get('fotgot_pass_succes_msg'));
                    },
                    function() {
                        AjaxLoader.hide();
                        app.notify(Resources.get('general_fault'));
                    }
            );
        };

        var _onShow = function() {
            if (!showed) {
                menuButton = BasePage.addIconButton(
                    'login_page',
                    header,
                    'menu',
                    function onClickEvent() {
                        app.menuButtonHandler();
                    }
                );
                if (!heightCalculated) {
                    BasePage.setHeight(page);
                    heightCalculated = true;
                };
            }
            AjaxLoader.hide();
            showed = true;
        };

        return {
            init: function() {
                page = $('#login_page');
                page.on(
                    'pageshow',
                    function(e) {
                        _onShow();
                        if (e != null) {
                            e.preventDefault();
                            e.stopPropagation();
                        };
                    }
                );
                content = $('#login_page_content');
                footer = $('#login_page_footer');
                header = $('#login_page_header');
                content.append(login_html()).trigger('create');
                footer.append(footer_html()).trigger('create');
                $('#login_ok').unbind("click").click(function() {_submit();});
                $('#login_forgot_password').unbind("click").click(function() {_forgotPassword();});

                var emailsOptions = '';
                jQuery.each(app.user.emails, function(i, val) {
                    emailsOptions = emailsOptions + "<option value='" + val + "'>" + val + "</option>";
                });
                var select = '<select id="login_email">' + emailsOptions + '</select>';
                $('#login_email_select_holder').append($(select)).trigger('create');

                emailInput = $('#login_email');
                passwordInput = $('#login_password');
            },
            getRunnedFunc: function() {
                return runnedFunc;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
