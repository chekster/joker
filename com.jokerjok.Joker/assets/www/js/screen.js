var Screen =(function(){
	var instantiated;

	var init = function() {
        var _locked = false;
        var _releaseTimeoutId = 0;

        var _wakeLock = function() {
            if (!_locked) {
                //cordova.require('cordova/plugin/powermanagement').dim(
                //cordova.require('cordova/plugin/powermanagement').partial(
                cordova.require('cordova/plugin/powermanagement').acquire(
                    function() {
                        _locked = true;
                    },
                    function() {
                    }
                );
            } else {
                if (_releaseTimeoutId != 0) {
                    clearTimeout(_releaseTimeoutId);
                    _releaseTimeoutId = 0;
                };
            };
        };

        var _releaseLock = function() {
            _releaseTimeoutId = setTimeout(function() {
                if (_locked) {
                    cordova.require('cordova/plugin/powermanagement').release(
                        function() {
                            _locked = false;
                            _releaseTimeoutId = 0;
                        },
                        function() {
                        }
                    );
                };
            }, 30000);
        };

        return {
            isLocked: function() {
                return _locked;
            },
            wakeLock: function() {
                _wakeLock();
            },
            releaseLock: function() {
                _releaseLock();
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
