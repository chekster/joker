var Sorting =(function(){
	var instantiated;

	var init = function() {
        var inited = false;
        var state = null;

        var callback = null;

        var sorting_container = null;
        var sorting = null;
        var sorting_background = null;

        var bestBtn = null;
        var randomBtn = null;
        var newBtn = null;
        var likeBtn = null;

        var bestBtnContainer = null;
        var randomBtnContainer = null;
        var newBtnContainer = null;
        var likeBtnContainer = null;

        var tagsWidget = null;

        var sorting_html = function() {
            var html = "<div id='sorting_tags_container'></div>" +

                       "<div class='large_buttons space_btw_buttons' >" +


                            "<input type='button' value='" + Resources.get('rand_sort_type_btn_caption') +
                            "' id='sorting_rand_btn' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('best_sort_type_btn_caption') +
                            "' id='sorting_best_btn' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('new_sort_type_btn_caption') +
                            "' id='sorting_new_btn' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('like_sort_type_btn_caption') +
                            "' id='sorting_like_btn' data-theme='b' />" +

                        "<div/>";
            return html;
        };

        var hideButtons = function() {
            bestBtnContainer.closest('div').css('display', 'none');
            randomBtnContainer.closest('div').css('display', 'none');
            newBtnContainer.closest('div').css('display', 'none');
            likeBtnContainer.closest('div').css('display', 'none');
            return true;
        };

        var showButtons = function() {
            bestBtnContainer.closest('div').css('display', 'block');
            randomBtnContainer.closest('div').css('display', 'block');
            newBtnContainer.closest('div').css('display', 'block');
            likeBtnContainer.closest('div').css('display', 'block');
        };

        return {
            clear: function() {
                inited = false;
                state = null;

                if (bestBtn != null) {
                    bestBtn.unbind("click");
                    randomBtn.unbind("click");
                    newBtn.unbind("click");
                    likeBtn.unbind("click");
                };

                bestBtn = null;
                randomBtn = null;
                newBtn = null;
                likeBtn = null;

                bestBtnContainer = null;
                randomBtnContainer = null;
                newBtnContainer = null;
                likeBtnContainer = null;

                if (sorting != null) {
                    sorting.empty();
                };
                sorting = null;
                sorting_background = null;

                tagsWidget = null;
            },
            getState: function() {
                return state;
            },
            getBackground: function() {
                return sorting_background;
            },
            getContainer: function() {
                return sorting_container;
            },
            getSorting: function() {
                return sorting;
            },
            init: function() {
                if (!inited) {
                    sorting_container = $('#select_sorting_type_container');
                    sorting = $('#select_sorting_type');
                    sorting_background = $('#select_sorting_type_background');
                    sorting.append(sorting_html()).trigger('create');

                    bestBtn = $('#sorting_best_btn');
                    randomBtn = $('#sorting_rand_btn');
                    newBtn = $('#sorting_new_btn');
                    likeBtn = $('#sorting_like_btn');

                    bestBtnContainer = bestBtn.closest('div');
                    randomBtnContainer = randomBtn.closest('div');
                    newBtnContainer = newBtn.closest('div');
                    likeBtnContainer = likeBtn.closest('div');

                    var that = this;
                    bestBtn.click(function(e) {that.best(e);});
                    randomBtn.click(function(e) {that.random(e);});
                    newBtn.click(function(e) {that.newj(e);});
                    likeBtn.click(function(e) {that.like(e);});

                    tagsWidget = new EditTags();
                    //tagsWidget.bind($('#sorting_tags_container'), 'sort', that.hideButtons, that.showButtons);
                    tagsWidget.bind($('#sorting_tags_container'),'sort');
                    tagsWidget.show();

                    inited = true;
                };
            },
            best: function(e) {
                this.hide();
                this.setPlaylistSortingType(1);
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                };
            },
            random: function(e) {
                this.hide();
                this.setPlaylistSortingType(0);
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                };
            },
            newj: function(e) {
                this.hide();
                this.setPlaylistSortingType(2);
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                };
            },
            like: function(e) {
                this.hide();
                this.setPlaylistSortingType(3);
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                };
            },
            setPlaylistSortingType: function(type) {
                if ( (Playlist.getSortingTypeCode() != type) || (type != 1) ) {
                    var tags = tagsWidget.getTags();
                    Playlist.setSortingType(type, tags);
                    if (typeof callback === 'function'){
                        callback();
                    };
                };
            },
            show: function(cb) {
                if (!inited) {
                    this.init();
                };
                callback = cb;

                tagsWidget.clear();
                jQuery.each(Playlist.getSortingTags(), function() {
                    tagsWidget.addTag(this);
                });

                state = 'show';
                sorting_container.css('display','block');
            },
            hide: function() {
                state = 'hide';
                sorting_container.css('display','none');
            },
            hideButtons: function() {
                hideButtons();
            },
            showButtons: function() {
                showButtons();
            },
            getTagsWidget: function() {
                return tagsWidget;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
