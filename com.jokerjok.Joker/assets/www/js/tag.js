Tag = function(_id, _tag) {
    var id = _id;
    var tag = _tag;
    return {
        getTag: function() {
            return tag;
        },
        setTag: function(_tag) {
            tag = _tag;
        }
    }
}

