var Settings =(function(){
	var instantiated;

	var init = function() {
        var inited = false;
        var settings_container = null;
        var settings = null;
        var settings_content = null;
        var settings_background = null;
        var state = null;
        var password = null;
        var language = null;
        var name = null;


        var settings_html = function() {
            var html = "<div>";

            if (app.user.registered) {
                html = html +
                            "<label for='settings_name' id='settings_name_lbl'>" +
                                Resources.get('name_input_lbl') +
                            "</label>" +
                            "<input type='text' id='settings_name' value='" + app.user.name + "''/>";
            }

            html = html +
                            "<label for='settings_language' id='settings_language_lbl'>" +
                                Resources.get('language_input_lbl') +
                            "</label>" +
                            "<select id='settings_language'>" + LanguageSelect.getOptions() + "</select>";

            if (app.user.registered) {
                html = html +
                            "<input type='button' value='" + Resources.get('change_password_btn_caption') +
                            "' id='settings_change_password' data-theme='b' />";
            }

            html = html +
                        "<div/>";
            return html;
        };

        var hideButtons = function() {
            $('#settings_change_password').closest('div').css('display', 'none');
        };

        var showButtons = function() {
            $('#settings_change_password').closest('div').css('display', 'block');
        };

        var setSize = function() {
            scroll(0, 0);
            var top = ($(window).height() - $(settings_content).outerHeight())/2;
            $(settings_content).css('top',top + 'px');
        };

        return {
            clear: function() {
                inited = false;
                settings_container = null;
                settings = null;
                settings_background = null;
                state = null;
                $('#settings_change_password').unbind("click");
                $('#settings_change_password').unbind("change");
                $('#settings_change_password').unbind("change");
                $('#settings').empty();
            },
            getState: function() {
                return state;
            },
            getBackground: function() {
                return settings_background;
            },
            getContainer: function() {
                return settings_container;
            },
            getSettings: function() {
                return settings;
            },
            init: function() {
                if (!inited) {
                    settings_container = $('#settings_container');
                    settings = $('#settings');
                    settings_background = $('#settings_background');
                    settings.append(settings_html()).trigger('create');
                    var that = this;
                    password = $('#settings_change_password');
                    language = $('#settings_language');
                    name = $('#settings_name');
                    settings_content = settings.children('div')[0];
                    password.click(function() {that.changePassword();});
                    language.change(function() {that.changeLanguage();});
                    name.change(function() {that.changeName();});
                    setSize();

                    inited = true;
                };
            },
            changeLanguage: function() {
                app.user.language = language.val();
                app.user.save();
            },
            changeName: function() {
                app.user.name = name.val();
                app.user.save();
            },
            changePassword: function() {
                this.hide();
                ChangePassword.show();
            },
            show: function() {
                this.init();
                state = 'show';
                settings_container.css('display','block');
            },
            hide: function() {
                state = 'hide';
                settings_container.css('display','none');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
