var EditVoice =(function(){
	var instantiated;

	var init = function() {
        var _inited = false;
        var _state = null;

        var _voice = null;
        var _changed = false;

        var _edit_voice_container = null;
        var _edit_voice = null;
        var _edit_voice_background = null;

        var _saveBtn = null;
        var _saveBtnContainer = null;

        var _tagsWidget = null;

        var edit_voice_html = function() {
            var html = "<div id='edit_voice_tags_container'></div>" +
                       "<div>" +
                       "<div/>";
            return html;
        };

        return {
            clear: function() {
                _inited = false;
                _state = null;

                if (_saveBtn != null) {
                    _saveBtn.unbind("click");
                };
                _saveBtn = null;
                _saveBtnContainer = null;

                if (_edit_voice != null) {
                    _edit_voice.empty();
                };
                _edit_voice = null;
                _edit_voice_background = null;
                _edit_voice_container = null;

                _tagsWidget = null;
            },
            getState: function() {
                return _state;
            },
            getBackground: function() {
                return _edit_voice_background;
            },
            getContainer: function() {
                return _edit_voice_container;
            },
            getEditVoice: function() {
                return _edit_voice;
            },
            init: function() {
                if (!_inited) {
                    _edit_voice_container = $('#edit_voice_container');
                    _edit_voice = $('#edit_voice');
                    _edit_voice_background = $('#edit_voice_background');
                    _edit_voice.append(edit_voice_html()).trigger('create');

                    _saveBtn = $('#menu_home');
                    _saveBtnContainer = _saveBtn.closest('div');

                    var that = this;
                    _saveBtn.click(function() {that.save();});

                    _tagsWidget = new EditTags();
                    _tagsWidget.bind($('#edit_voice_tags_container'),
                                    'edit_voice',
                                    function() {
                                        _changed = true;
                                    }
                    );
                    _tagsWidget.show();

                    _inited = true;
                };
            },
            show: function(voice) {
                if (!_inited) {
                    this.init();
                };

                _voice = voice;
                _tagsWidget.clear();
                jQuery.each(_voice.tags, function() {
                    _tagsWidget.addTag(this.tag);
                });
                _changed = false;

                _state = 'show';
                _edit_voice_container.css('display','block');
            },
            hide: function() {
                if (_changed) {
                    _voice.addTags(_tagsWidget.getTags());
                    MyVoicesPage.updateCurrentVoice();
                    _voice.put();
                };

                _state = 'hide';
                _edit_voice_container.css('display','none');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
