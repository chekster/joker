var VK =(function(){
	var instantiated;

	var init = function() {
        var _inited = false;
        var _token = null;
        var _tokenExp = null;
        var _userId = null;
        var _tokenDate = null;
        var _inAppBrwsr = null;

        var _logout = function() {
            //TODO place parameters to resources
            //var vkUrl = "https://api.vk.com/oauth/logout?" +
            var vkUrl = "https://oauth.vk.com/logout?" +
                "client_id=3780702&";// +
                //"access_token=" + _token;
                /*
                "scope=wall&" +
                "redirect_uri=https://oauth.vk.com/blank.html&" +
                "display=mobile&" +
                "v=4.103&" +
                "response_type=token";
                */
            console.warn('joker logout url = ' + vkUrl);
            _inAppBrwsr = window.open(vkUrl, '_blank', 'location=yes');
            _inAppBrwsr.addEventListener('loadstop',
                                function(event) {
                                    var url = event.url;
                                    console.warn('joker vk logout stopload ' + url);
                                    document.cookie = '';
                                    console.warn('joker vk logout stopload cookies ' + document.cookie);
                                    var pairs = document.cookie.split(";");
                                    var cookies = {};
                                    for (var i=0; i<pairs.length; i++){
                                        var pair = pairs[i].split("=");
                                        //cookies[pair[0]] = unescape(pair[1]);
                                        console.warn('joker vk logout stopload cookie ' + pair[0] + '#' + pair[1]);
                                    }
                                       // _inAppBrwsr.close();

                                        //console.warn('joker stop uiS: ' + userIdS);
                                        //console.warn('joker stop uiL: ' + userIdL);
                                        //console.warn('joker stop ui: ' + userId);

                                }
            );
        };

        var _isTokenExpired = function() {
            var result = true;
            if (_tokenDate != null) {
                var tokenExpMS = _tokenExp * 1000;
                var expDateMS = _tokenDate.valueOf() + tokenExpMS;
                var expDate = new Date(_tokenDate.valueOf() + tokenExpMS);
                var curDate = new Date();
                if (curDate < expDate) {
                    result = false;
                };
            };
            return result;
        };

        var _postToWall = function() {
            log('VK._postToWall');
            var dwnldUrl = Playlist.getVoice().getDownloadPath();
            app.cordova.execute(
                "VoiceRecorder",
                "downloadVoice",
                function(data) {
                    if(data.status == 1) {
                    } else {
                        log('downloadVoice not started');
                    }
                },
                function(err) {
                    log('downloadVoice err ' + err);
                },
                [dwnldUrl,_token]
            );

            /*
            var postUrl = "https://api.vk.com/method/wall.post?owner_id=" + _userId +
                "&message=" + "test" +
                "&access_token=" + _token;
            app.ajaxBase(
                postUrl,
                'POST',
                {},
                function(data) {
                    log(data);
                },
                function(stat) {
                },
                function(stat, error) {
                }
            );
            */
        };

        var _login = function() {
            //TODO place parameters to resources
            var vkUrl = "https://oauth.vk.com/authorize?" +
                "client_id=3780702&" +
                "scope=wall,audio&" +
                "redirect_uri=https://oauth.vk.com/blank.html&" +
                "display=mobile&" +
                "v=4.103&" +
                "response_type=token";
            _inAppBrwsr = window.open(vkUrl, '_blank', 'location=yes');
            _inAppBrwsr.addEventListener('loadstop',
                                function(event) {
                                    var url = event.url;
                                    if (url.indexOf("#") > -1) {
                                        var paramersStr = url.substr(url.indexOf("#") + 1,
                                                                     url.length - url.indexOf("#") - 1);
                                        var parametersKeyValArr = paramersStr.split('&');
                                        var parametersHash = new Object();
                                        for (var i = 0; i < parametersKeyValArr.length; i++) {
                                            var parameterKeyVal = parametersKeyValArr[i];
                                            var parameterKeyValArr = parameterKeyVal.split('=');
                                            parametersHash[parameterKeyValArr[0]] = parameterKeyValArr[1];
                                        }
                                        var toBreak = false;
                                        for (var key in parametersHash) {
                                            if ( (key == 'error') && (parametersHash[key] == 'access_denied') ) {
                                                for (var key1 in parametersHash) {
                                                    if ( (key1 == 'error_reason') &&
                                                         (parametersHash[key1] == 'user_denied') ) {
                                                        _inAppBrwsr.close();
                                                        toBreak = true;
                                                        break;
                                                    }
                                                }
                                                if (toBreak) {
                                                    break;
                                                };
                                            };
                                            if (key == 'access_token') {
                                                _tokenDate = new Date();
                                                window.localStorage.setItem("vk_token",parametersHash[key]);
                                                window.localStorage.setItem("vk_exp",parametersHash['expires_in']);
                                                window.localStorage.setItem("vk_date",_tokenDate);
                                                window.localStorage.setItem("vk_user",parametersHash['user_id']);
                                                _token = parametersHash[key];
                                                _tokenExp = parametersHash['expires_in'];
                                                _userId = parametersHash['user_id'];
                                                _inAppBrwsr.close();
                                                VK.postToWall();
                                                break;
                                            };
                                        }
                                    };
                                }
            );
        };

        var _init = function() {
            if (!_inited) {
                _token = window.localStorage.getItem("vk_token");
                _tokenExp = window.localStorage.getItem("vk_exp");
                _tokenDate = window.localStorage.getItem("vk_date");
                if (_tokenDate != null) {
                    _tokenDate = new Date(_tokenDate);
                };
                _userId = window.localStorage.getItem("vk_user");
                _inited = true;
            };
        };

        return {
            clear: function() { //need for testing
                _inited = false;
                _state = null;
                _token = null;
                _tokenExp = null;
                _tokenDate = null;
                _userId = null;
                _inAppBrwsr = null;
            },
            getAuthBrwsr: function() { //need for testing
                return _inAppBrwsr;
            },
            isTokenExpired: function() { //need for testing
                return _isTokenExpired();
            },
            postToWall: function() {
                //TODO Pri kazhdom  pervom zapuske za sessiu obnovlyat' token esli ne expired
                _init();
                if ( _isTokenExpired() ) {
                    _login();
                } else {
                    _postToWall();
                };
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
