var AjaxLoader =(function(){
	var instantiated;

	var init = function() {
        var loader = null;
        var state = null;

        return {
            getState: function() {
                return state;
            },
            getLoader: function() {
                return loader;
            },
            init: function() {
                loader = $('#ajax_loader');
            },
            show: function() {
                state = 'show';
                loader.css('display','block');
            },
            hide: function() {
                state = 'hide';
                loader.css('display','none');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
