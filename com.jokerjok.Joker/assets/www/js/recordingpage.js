RecordingPage = function() {
    this.page = $('#recording_page');
    this.playButton = null;
    this.saveButton = null;
    this.recordButton = null;
    this.cancelButton = null;
    var worldButton = null;
    var menuButton = null;
    var homeButton = null;
    this.content = $('#recording_page_content');
    this.header = $('#recording_page_header');
    this.controlsBar = $('#controls_bar');
    this.timer = Timer();
    this.tags = EditTags();
    this.buttonsInited = false;
    this.heightCalculated = false;
    this.setHeight = function() {
        if (!this.heightCalculated) {
            this.initButtons();
            BasePage.setHeight(this.page);
            this.heightCalculated = true;
        }
    }
    this.initButtons = function() {
        if (true) {
        //if(!this.buttonsInited && this.controlsBar.length > 0) {
            this.buttonsInited = true;
            var that = this;
            this.controlsBar.empty();
            this.worldButton = BasePage.addIconButton(
                'recording_page',
                this.header,
                'world',
                function onClickEvent() {
                    AjaxLoader.show();
                    app.navigateToPage('main_page');
                }
            );
            this.menuButton = BasePage.addIconButton(
                'recording_page',
                this.header,
                'menu',
                function onClickEvent() {
                    app.menuButtonHandler();
                }
            );
            homeButton = BasePage.addIconButton(
                'recording_page',
                this.header,
                'home',
                function onClickEvent() {
                    AjaxLoader.show();
                    MyVoicesPage.show();
                }
            );




            this.recordButton = BasePage.addIconButton(
                'recording_page',
                this.controlsBar,
                'record',
                function onClickEvent() {
                    that.recordingButtonClick();
                }
            );
            this.playButton = BasePage.addIconButton(
                'recording_page',
                this.controlsBar,
                'rplay',
                function onClickEvent() {
                    that.playingButtonClick();
                }
            );
            this.saveButton = BasePage.addIconButton(
                'recording_page',
                this.controlsBar,
                'save',
                function onClickEvent() {
                    that.setSaved();
                }
            );
            this.cancelButton = BasePage.addIconButton(
                'recording_page',
                this.controlsBar,
                'cancel',
                function onClickEvent() {
                    that.setCancel();
                }
            );
            this.playButton.button('disable');
            this.saveButton.button('disable');
        }
    }
    this.clearRecordedVoice = function() {
        app.recorder.currentVoice = {};
        app.ui.recordingPage.setHeight();
        app.ui.recordingPage.tags.clear();


        app.ui.recordingPage.tags.hide();
        app.ui.recordingPage.timer.clear();


        app.ui.recordingPage.playButton.button('disable');
        app.ui.recordingPage.saveButton.button('disable');
    }
    this.init = function() {
        if(this.content.length > 0) {
            this.timer.bind(this.content);
            this.tags.bind(this.content,
                            null,
                            function() {
                                app.ui.recordingPage.enableSaveButton();
                            }
            );
            this.tags.hide();
        }
        var that = this;
        this.page.on(
            'pageshow',
            function(event) {
                //TODO it after exit from recording page
                if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                    if(app.recorder.currentVoice.url != null && app.recorder.currentVoice.url != '') {
                    } else {
                        app.recorder.currentVoice.remove();
                    }
                }
                that.clearRecordedVoice();
                AjaxLoader.hide();
            }
        );
    }
    this.setCancel = function() {
        if(app.recorder.recStatus) {
            this.recordingButtonClick();
        } else if(app.player.playStatus) {
            this.playingButtonClick();
        } else if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
            if(app.recorder.currentVoice.url != null && app.recorder.currentVoice.url != '') {
                app.ui.recordingPage.clearRecordedVoice();
            } else {
                app.recorder.currentVoice.remove();
                app.ui.recordingPage.clearRecordedVoice();
            }
        } else {
            app.ui.recordingPage.clearRecordedVoice();
            app.navigateToPage('main_page');
        }
    }
    this.setSaved = function() {
        //TODO also updating from here
        if(app.player.playStatus) {
            this.playingButtonClick();
        }
        app.recorder.currentVoice.addTags(this.tags.getTags());
        app.recorder.save();
        if(app.recorder.currentVoice.url == null || app.recorder.currentVoice.url == '') {
            this.playButton.button('disable');
        };
        this.saveButton.button('disable');
    }
    this.playingButtonClick = function() {
        if(!app.recorder.recStatus) {
            if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                if(!app.player.playStatus) {
                    $('.ui-icon-rplay').removeClass("ui-icon-rplay").addClass("ui-icon-rstopplay");
                    app.ui.recordingPage.recordButton.button('disable');
                    app.ui.recordingPage.timer.clear(app.recorder.currentVoice.duration,'down');
                    //app.recorder.play(app.ui.recordingPage.playingButtonClick, app.ui.recordingPage.timer);
                    app.recorder.currentVoice.load(app.ui.recordingPage.playingButtonClick, null);
                    app.player.play(app.recorder.currentVoice, app.ui.recordingPage.timer, null);
                } else {
                    $('.ui-icon-rstopplay').removeClass("ui-icon-rstopplay").addClass("ui-icon-rplay");
                    app.ui.recordingPage.timer.clear(app.recorder.currentVoice.duration,'down');
                    app.ui.recordingPage.recordButton.button('enable');
                    app.player.stop();
                }
            }
        }
    }
    this.recordingButtonClick = function() {
        if(app.recorder.recStatus) {
            this.tags.show();
            this.tags.clear();
            $('.ui-icon-stoprecord').removeClass("ui-icon-stoprecord").addClass("ui-icon-record");
            this.timer.clear(app.recorder.currentVoice.duration,'down');
            app.recorder.stop();
            if(!jQuery.isEmptyObject(app.recorder.currentVoice)) {
                this.playButton.button('enable');
                if (app.online) { //chek   only if online can save voice
                    this.saveButton.button('enable');
                }
            }
        } else {
            this.tags.hide();
            this.tags.clear();
            $('.ui-icon-record').removeClass("ui-icon-record").addClass("ui-icon-stoprecord");
            this.timer.clear();
            app.recorder.start(this.timer);
            this.playButton.button('disable');
            this.saveButton.button('disable');
        }
    }
    this.addIconButton = function(icon,onClickFunction) {
        var id = 'recording_' + icon + '_button';
        var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');
        iconButton.unbind("click").click(onClickFunction);
        this.controlsBar.append(iconButton).trigger('create');
        return iconButton;
    };
    this.enablePlayButton = function() {
        if (this.playButton != null) {
            this.playButton.button('enable');
        };
    };
    this.enableSaveButton = function() {
        if (this.saveButton != null) {
            this.saveButton.button('enable');
        };
    };
}
