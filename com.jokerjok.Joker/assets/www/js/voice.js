Voice = function(_url,_guser,_language) {
    this.timestamp=getDateTime();
    this.system='soundcloud';
    this.url=_url;
    this.guser=_guser;
    this.user_name='';
    this.language=_language;
    this.duration=0;
    this.size=0;
    this.likes=0;
    this.dislikes=0;
    this.likes_sum=0;
    this.id=getTimeStamp();
    //this.tags_access=[{'tag':'joke'}];
    this.tags_access=[];
    this.SM=null;
    this.loading=false;
    this.loaded=false;
    this.onLoad=null;
    this.onFinish=null;
    this.listened=false;
    this.tags=[];
    this.post = function() {
        this.app_account_id = app.user.system.app_account_id;
        this.token = app.user.token;
        var voiceJSON = JSON.stringify(this);
        var voice = this;
        app.ajax(
            '/voices.json',
            'POST',
            voiceJSON,
            function(data) {
                voice.id = data.id
                voice.remove();
                MyVoicesList.addVoice(voice);
            },
            function(stat) {
            },
            function(stat, error) {
                //TODO something if not posted
                log('voice not posted ' + stat + ', err ' + error);
            }
        );
    };
    this.put = function() {
        this.app_account_id = app.user.system.app_account_id;
        this.token = app.user.token;
        var soundMamagerObj = null;
        if (this.SM != null) {
            soundMamagerObj = this.SM;
            this.SM = null;
        };
        var voiceJSON = JSON.stringify(this);
        var voice = this;
        app.ajax(
            '/voices/' + voice.id + '.json',
            'PUT',
            voiceJSON,
            function(data) {
                voice.SM = soundMamagerObj;
            },
            function(stat) {
            },
            function(stat, error) {
                log('voice not updated ' + stat + ', err ' + error);
            }
        );
    };
    this.addSpamReport = function(callback) {
        if(this.id >0 ) {
            var voice = this;
            var spamReportObj = {
                                token:app.user.token,
                                spam:{
                                    guser:app.user.guser,
                                    voice_id:voice.id
                                }
                          };
            var spamReportJSON = JSON.stringify(spamReportObj);
            app.ajax(
                '/spams.json',
                'POST',
                spamReportJSON,
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    //TODO something if not posted
                    log('spam report not posted ' + stat + ', err ' + error);
                }
            );
        };
    }
    this.addLike = function(like, callback) {
        if(this.id >0 ) {
            var voice = this;
            var likeObj = {
                                token:app.user.token,
                                like:{
                                    guser:app.user.guser,
                                    voice_id:voice.id,
                                    like:like,
                                }
                          };
            var likeJSON = JSON.stringify(likeObj);
            app.ajax(
                '/likes.json',
                'POST',
                likeJSON,
                //TODO check if like was exist or not
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    //TODO something if not posted
                    log('like not posted ' + stat + ', err ' + error);
                }
            );
        };
    }
    this.removeLike = function(callback) {
        if (this.user_like != null) {
            var voice = this;
            var id = this.user_like.id;
            app.ajax(
                '/likes/' + id + '.json?' + 'token=' + app.user.token,
                'DELETE',
                null,
                //{token:app.user.token},
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    log('like not deleted ' + stat + ', err ' + error);
                }
            );
        }
    }
    this.removeSpamReport = function(callback) {
        if (this.user_spam != null) {
            var voice = this;
            var id = this.user_spam.id;
            app.ajax(
                '/spams/' + id + '.json?' + 'token=' + app.user.token,
                'DELETE',
                null,
                //{token:app.user.token},
                function(data) {
                    voice.cast(data);
                    if (typeof callback === 'function'){
                        callback();
                    }
                },
                function(stat) {
                },
                function(stat, error) {
                    log('spam not deleted ' + stat + ', err ' + error);
                }
            );
        }
    }
    this.cast = function(voice) {
        for (var key in voice) {
            if (voice.hasOwnProperty(key)) {
                var value = voice[key];
                if ((key == 'spams') && (voice[key] == null)) {
                    value = 0;
                };
                this[key] = value;
            };
        }
    }
    this.remove = function() {
        this.unload();
        var voiceJSON = JSON.stringify(this);
        var voice = this;
        app.cordova.execute(
            "VoiceRecorder",
            "removeRecord",
            function(data) {
                if(data.status == 1) {
                    log('voice removed');
                    if(voice.url != null && voice.url != '') {
                        app.recorder.removeSavingTask(voice.url);
                    }
                } else {
                    log('record not removed');
                }
            },
            function(err) {
                log('record remove err ' + err);
            },
            [voiceJSON]
        );
    }
    this.addTags = function(newTags) {
        this.tags.length = 0;
        this.tags_access.length = 0;
        var tags_access = this.tags_access;
        var tags = this.tags;
        $.each(newTags, function(index, tag) {
            tags_access.push({'tag':tag});
            tags.push({'tag':tag});
        });
    };
    this.getTagsText = function() {
        var tagsText = '';
        jQuery.each(this.tags, function() {
            if (tagsText == '') {
                tagsText = tagsText + '#' + this.tag;
            } else {
                tagsText = tagsText + ', #' + this.tag;
            };
        });
        return tagsText;
    };
    this.calcTimeFromSeconds = function() {
        var min = Math.floor(this.duration/60);
        var sec = (this.duration - (min * 60)).toString();
        if(sec.length == 1) {
            sec = '0' + sec;
        }
        return min + ':' + sec;
    };
    this.stop = function() {
        app.player.stop();
    };
    this.deleteVoice = function(callback) {
        var voice = this;
        app.ajax(
            '/voices/' + this.id + '.json?token=' + app.user.token,
            'DELETE',
            null,
            function(data) {
                if (typeof callback === 'function'){
                    MyVoicesList.removeVoice(voice);
                    callback();
                }
            },
            function(stat) {
            },
            function(stat, error) {
                log('voice deleteVoice, not deleted ' + stat + ', err ' + error);
            }
        );
    }
    this.getPath = function() {
        var path = '';
        if(this.url != null && this.url != '') {
            path = this.url + '?consumer_key=' + app.user.system.client_id;
        } else {
            path = this.fileLocation;
        }
        return path;
    }
    this.getDownloadPath = function() {
        var path = null;
        if(this.url != null && this.url != '') {
            path = this.url + '?client_id=' + app.user.system.client_id;
        }
        return path;
    }
    this.load = function(onFinish, onLoad) {
        if (app.online && (app.user.system != null) && (app.user.system.client_id != null)) {
            if (!this.loaded && !this.loading) {
                this.loading = true;
                var path = this.getPath();
                this.onLoad = onLoad;
                this.onFinish = onFinish;
                var that = this;
                this.SM = soundManager.createSound(
                        {
                            id: 'mySound' + this.id,
                            url: path,
                            autoLoad: true,
                            onload:function(success) {
                                that.loading=false;
                                that.loaded=true;
                                if (typeof that.onLoad === 'function'){
                                    that.onLoad();
                                }
                            },
                            onfinish:function() {
                                if (typeof that.onFinish === 'function'){
                                    that.onFinish();
                                }
                            }
                        }
                );
            };
        };
    }
    this.unload = function() {
        if (this.SM != null) {
            this.SM.destruct();
        };
        this.loaded = false;
        this.loading = false;
        this.SM = null;
    }
}

