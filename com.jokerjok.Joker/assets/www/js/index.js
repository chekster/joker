var app = {
    enabled: false,
    online: null,
    backendUrl: function() {
        return Resources.get('backend_url')
    },
    goPage: null,
    user: null,
    player: null,
    recorder: null,
    ui: null,
    testing: false,
    //TODO replace all using aff activePage
    activePage: "start_page",
    version: function() {
        return Resources.get('version')
    },
    navigateToPage: function(page_id) {
        AjaxLoader.show();
        app.goPage = page_id;
        $.mobile.changePage("#" + page_id, null, false, true );
    },
    notify: function(msg, callback) {
        if (!app.testing) {
            if (!isAndroid()) {
                alert(msg);
            } else {
                navigator.notification.alert(
                    msg,
                    callback,
                    Resources.get('app_name')
                );
            };
        };
    },
    cordova: {
        execute: function(plugin,action,succes,error,args) {
            if(isAndroid()) {
                Cordova.exec(
                        function(data) {
                            //log('cordova ' + data);
                            var dataObj  = eval('(' + data + ')');
                            succes(dataObj);
                        },
                        function(err) {
                            if (typeof error === 'function'){
                                try{
                                    var errObj  = eval('(' + err + ')');
                                    error(errObj);
                                } catch(e) {
                                    log(err);
                                }
                            }
                        },
                        plugin,
                        action,
                        args
                );
            }
        }
    },
    ajax: function(url, method, jdata, success, complete, error) {
        this.ajaxBase(this.backendUrl() + url, method, jdata, success, complete, error);
    },
    ajaxBase: function(url, method, jdata, success, complete, error) {
        if (this.online) {
            $.ajax({
                url: url,
                type: method,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                cache: false,
                data: jdata,
                //TODO CSRF security
                //beforeSend: function(xhr){xhr.setRequestHeader("X-CSRF-Token", 'dfsgdfgdfgdfg');},
                success: function(data){success(data);},
                complete: function(XMLHttpRequest, textStatus){complete(textStatus);},
                error: function(XMLHttpRequest, textStatus, errorThrown){error(XMLHttpRequest.status,errorThrown);}
            });
        };
    },

    // Application Constructor
    //TODO perenesti eto v Menu
    menuButtonHandler: function(e) {
        if (AjaxLoader.getState() != 'show') {
            if (Menu.getState() == 'show') {
                Menu.hide();
            } else {
                app.hideActiveModal();
                Menu.show();
            };
        };
        if (e != null) {
            e.preventDefault();
            e.stopPropagation();
        };
    },
    hideActiveModal: function() {
        if (EditVoice.getState() == 'show') {
            EditVoice.hide();
        }
        if (Sorting.getState() == 'show') {
            Sorting.hide();
        }
        if (ChangePassword.getState() == 'show') {
            ChangePassword.hide();
        }
        if (Settings.getState() == 'show') {
            Settings.hide();
        }
    },
    backButtonHandler: function(e) {
        if (EditVoice.getState() == 'show') {
            EditVoice.hide();
            return;
        }
        else if (Sorting.getState() == 'show') {
            Sorting.hide();
            return;
        }
        else if (ChangePassword.getState() == 'show') {
            ChangePassword.hide();
            return;
        }
        else if (Menu.getState() == 'show') {
            Menu.hide();
            return;
        }
        else if (Settings.getState() == 'show') {
            Settings.hide();
            return;
        }
        else if(app.activePage == 'main_page'){
            navigator.app.exitApp();
        }
        else if(app.activePage == 'login_page'){
            app.navigateToPage('start_page');
        }
        else if(app.activePage == 'registration_page'){
            if (!app.user.session) {
                app.navigateToPage('start_page');
            }
            if (app.user.session && !app.user.registered) {
                app.navigateToPage('main_page');
            }
        }
        else if(app.activePage == 'start_page'){
            navigator.app.exitApp();
        }
        else if(app.activePage == 'recording_page'){
            app.ui.recordingPage.setCancel();
        }
        else if(app.activePage == 'myvoices_page'){
            MyVoicesPage.backBtnClick();
        }
        if (e != null) {
            e.preventDefault();
            e.stopPropagation();
        }
    },
    initialize: function(testing) {
        if (testing != null) {
            this.testing = testing;
        };
        this.ui = new UI();
        if(!isAndroid()) {
            this.online = true;
            AjaxLoader.init();
            Resources.init();
            //Menu.show();
            this.player = new Player();
            this.user = new User();
            this.user.init();
            //Set maximum seconds for 1 record
            this.recorder = new Recorder(parseInt(Resources.get('recording_limit')));
            this.ui.init();
            enabled = true;
            //Settings.show();
            //ChangePassword.show();
            //MyVoicesPage.show();
            //app.navigateToPage('main_page');

            //In browser pressed ESC
            $(document).keyup(function(e) {
                if (e.keyCode == 27) {
                    app.backButtonHandler(e);
                }
            });
        } else {
            this.bindEvents();
        }
        $(document).ready(function(event) {
            //TODO stop the events, check it
            $(document).bind("pagebeforechange",function(e,data){
                if (app.goPage == null) {
                    return false;
                };
                /*
                TODO navigation for one step
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                */
            });

            $(document).live( 'pageshow',function(e, data){
                if (app.goPage != null) {
                    if (app.goPage == $.mobile.activePage.attr('id')) {
                        //If go out from Recording Page clear the curent recorder voice
                        if (app.activePage == "recording_page") {
                            app.recorder.currentVoice = {};
                        };
                        //if go out from Login Page clear the password
                        if (app.activePage == "login_page") {
                            $('#login_password').val('');
                        };
                        app.activePage = app.goPage
                        app.goPage = null;
                    };
                };
                AjaxLoader.hide();
                /*
                TODO navigation for one step
                if (e != null) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                */
            });

            /*
            $('#recording_page').on(
                'pageshow',
                function(e) {
                    //TODO like over pages
                    app.ui.setRecordingPageActive();
                    //e.preventDefault();
                    //e.stopPropagation();
                }
            );
            */
            //event.preventDefault();
            //event.stopPropagation();
        });
        soundManager.setup(
            {
                url: '../../swf/',
                useFlashBlock: false,
                consoleOnly: false,
                bgColor: '#f6f6f6',
                debugMode: false,
                // enable flash debug output for this page
                debugFlash: false,
                preferFlash: false
            }
        );
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    onOnline: function(e) {
        this.online = true;
        if(this.enabled) {
            if (!this.user.session) {
                this.user.initSessionByToken();
            };
        }
        if (e != null) {
            e.preventDefault();
            e.stopPropagation();
        }
    },
    onOffline: function(e) {
        if ((this.online == null) || (this.online)) {
            this.online = false;
            this.notify(Resources.get('not_online_msg'));
        };
        if (e != null) {
            e.preventDefault();
            e.stopPropagation();
        }
    },
    bindEvents: function() {
        document.addEventListener("online", function(e) { app.onOnline(e);}, false);
        document.addEventListener("offline", function(e) { app.onOffline(e);}, false);
        document.addEventListener('deviceready', function(e) { app.onDeviceReady(e);}, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function(e) {
        //log('onDeviceReady');
        this.receivedEvent('deviceready');
        if (e != null) {
            e.preventDefault();
            e.stopPropagation();
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(event_id) {
        switch (event_id)
        {
        case 'deviceready':
            Resources.init();
            document.addEventListener("backbutton", function(e) { app.backButtonHandler(e);}, false);
            document.addEventListener("menubutton", function(e) { app.menuButtonHandler(e);}, false);
            $(document).click(function(e){return false;});
            AjaxLoader.init();
            this.user = new User();
            this.user.init();
            this.player = new Player();
            //Set maximum seconds for 1 record
            this.recorder = new Recorder(parseInt(Resources.get('recording_limit')));
            this.recorder.deleteAllRecordingsFiles();
            this.ui.init();
            this.enabled = true;
            break;
        case 'load':
            break;
        case 'offline':
            break;
        case 'online':
            break;
        default:
        }
    }
};


function isAndroid() {
    if(navigator.platform != 'Linux i686') {
        return true;
    } else {
        return false;
    }
}

function log(msg, timestamp) {
    if(!isAndroid()) {
        console.log(msg);
    } else {
        //if (true) {
        if (Resources.get('environment') != 'production') {
            if (timestamp != null) {
                console.warn('joker ' + getTimeStamp() + ' ' + msg);
            } else {
                console.warn('joker ' + msg);
            };
        };
    }
}

function getTimeStamp() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "_"
                    + (currentdate.getMonth()+1)  + "_"
                    + currentdate.getFullYear() + "___"
                    + currentdate.getHours() + "_"
                    + currentdate.getMinutes() + "_"
                    + currentdate.getSeconds();
    return datetime
}

function getDateTime() {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth()+1)  + "/"
                    + currentdate.getFullYear() + " @ "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();
    return datetime
}
