var MyVoicesList =(function(){
	var instantiated;
	var init = function() {
        var that = this;
        var loaded = false;
        var changed = true;
        var voicesList = [];
        var selectParameters = {};
        var currentVoiceIndex = null;
        var _voicesListToJSON = function() {
            $.each(voicesList, function(index, voice) {
                voice.SM = null
            });
            var voicesListJSON = JSON.stringify(voicesList);
            return voicesListJSON;
        };
        var _loadList = function(onLoadHandler) {
            //TODO cache voices
            if (!loaded) {
                currentVoiceIndex = null;
                var voicesListJSON = window.localStorage.getItem("myvoices");
                if ( voicesListJSON != null ) {
                    //load my voices from cache
                    var result = JSON.parse(voicesListJSON);
                    $.each(result, function(index, voice) {
                        var v = new Voice(voice.url,voice.guser,voice.language);
                        v.cast(voice);
                        voicesList.push(v);
                    });
                    loaded = true;
                    currentVoiceIndex = 0;
                    if (typeof onLoadHandler === 'function'){
                        onLoadHandler();
                    };
                    changed = false;
                } else {
                    //load my voices from server
                    app.ajax(
                        '/voices/my_voices.json',
                        'GET',
                        {guser:app.user.guser,token:app.user.token},
                        function(data) {
                            $.each(data, function(index, voice) {
                                var v = new Voice(voice.url,voice.guser,voice.language);
                                v.cast(voice);
                                voicesList.push(v);
                            });
                            loaded = true;
                            currentVoiceIndex = 0;
                            window.localStorage.setItem("myvoices", _voicesListToJSON());
                            if (typeof onLoadHandler === 'function'){
                                onLoadHandler();
                            };
                            changed = false;
                        },
                        function(stat) {
                        },
                        function(stat, error) {
                            log(stat + '  :  ' + error);
                            if (typeof onLoadHandler === 'function'){
                                onLoadHandler();
                            };
                        }
                    );
                };
            } else {
                if (typeof onLoadHandler === 'function'){
                    onLoadHandler();
                };
            };
        };

		return {
            loadList: function(onLoadHandler) {
                _loadList(onLoadHandler);
            },
            addVoice: function(v) {
                voicesList.push(v);
                changed = true;
            },
            getVoices: function() {
                return voicesList;
            },
            getVoice: function() {
                if(voicesList.length > 0) {
                    return voicesList[currentVoiceIndex];
                }
                return null;
            },
            anyPreviousVoice: function() {
                if(currentVoiceIndex > 0) {
                    return true;
                }
                return false;
            },
            getPreviousVoice: function() {
                if(currentVoiceIndex > 0) {
                    currentVoiceIndex--;
                }
                return this.getVoice();
            },
            anyNextVoice: function() {
                if((voicesList.length - 1) > currentVoiceIndex) {
                    return true;
                }
                return false;
            },
            getNextVoice: function() {
                if((voicesList.length - 1) > currentVoiceIndex) {
                    currentVoiceIndex++;
                }
                return this.getVoice();
            },
            getFirstVoice: function() {
                currentVoiceIndex = 0;
                return this.getVoice();
            },
            removeVoice: function(voice) {
                var ind = voicesList.indexOf(voice);
                if ( currentVoiceIndex == (voicesList.length - 1) ) {
                    currentVoiceIndex = currentVoiceIndex - 1;
                };
                voicesList.splice(ind,1);
                //currentVoiceIndex = 0;
            },
            isChanged: function() {
                return changed;
            },
            changesAccepted: function() {
                changed = false;
            },
            getVoiceByID: function(id) {
                var tmpCurrentVoiceIndex = currentVoiceIndex;
                currentVoiceIndex = 0;
                while ( ( this.getVoice().id != id ) && ( this.anyNextVoice() ) ){
                    this.getNextVoice();
                }
                if (this.getVoice().id == id) {
                } else {
                    currentVoiceIndex = tmpCurrentVoiceIndex;
                };
                return this.getVoice();
            },
            clearCache: function() {
                voicesList = [];
                loaded = false;
                window.localStorage.removeItem("myvoices");
            },
            voicesCount: function() {
                return voicesList.length;
            }
		}
	};

    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
