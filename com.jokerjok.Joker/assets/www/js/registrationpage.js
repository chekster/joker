var RegistrationPage =(function(){
	var instantiated;

	var init = function() {
        var page = null;
        var content = null;
        var footer = null;
        var header = null;
        var heightCalculated = false;
        var showed = false;
        var passwordInput = null;
        var passwordConfInput = null;
        var nameInput = null;
        var emailInput = null;
        var languageInput = null;
        var menuButton = null;

        var runnedFunc = null;

        var reg_html = function() {
            var html = "<div class='space_btw_inputs'>" +

                               "<label for='registration_email' id='registration_email_lbl'>" +
                                    Resources.get('email_input_lbl') +
                               "</label>" +
                               "<div id='email_select_holder' class='input'></div>" +

                               "<label for='registration_password' id='registration_password_lbl'>" +
                                    Resources.get('password_lbl') +
                               "</label>" +
                               "<label id='registration_password_tip_lbl' class='tip'>" +
                                    Resources.get('password_tip_lbl') +
                               "</label>" +
                               "<input type='password' id='registration_password' class='input'/>" +

                               "<label for='registration_confirm_password' id='registration_confirm_password_lbl'>" +
                                    Resources.get('confirm_password_lbl') +
                               "</label>" +
                               "<input type='password' id='registration_confirm_password' class='input'/>" +

                               "<label for='registration_name' id='registration_name_lbl'>" +
                                    Resources.get('name_input_lbl') +
                               "</label>" +
                               "<label id='registration_name_tip_lbl' class='tip'>" +
                                    Resources.get('name_tip_lbl') +
                               "</label>" +
                               "<input type='text' id='registration_name' class='input'/>" +

                               "<label for='registration_language' id='registration_language_lbl'>" +
                                    Resources.get('language_input_lbl') +
                               "</label>" +
                               "<div id='language_select_holder' class='input'></div>" +

                           "<div/>";
            return html;
        };

        var footer_html = function() {
            var html = "<input type='submit' class='bottom_panel' value='" + Resources.get('registration_btn_caption') + "' id='registration_ok' data-theme='b' />";
            return html;
        };

        var _submit = function(succesCallback,errorCallback) {
            runnedFunc = 'submit';
            var password = passwordInput.val();
            var password_confirm = passwordConfInput.val();
            var name = nameInput.val();
            if ( (password == null) || (password.length < 6) || (password.length > 20) ) {
                app.notify(Resources.get('password_requirements'));
                if (typeof errorCallback === 'function'){
                    errorCallback();
                }
                return;
            };
            if ( password != password_confirm ) {
                app.notify(Resources.get('confirm_password_incorrect'));
                if (typeof errorCallback === 'function'){
                    errorCallback();
                }
                return;
            };
            if ( (name == null) || (name.length < 2) || (name.length > 16) ) {
                app.notify(Resources.get('name_requirements'));
                if (typeof errorCallback === 'function'){
                    errorCallback();
                }
                return;
            };
            var email = emailInput.val();
            var language = languageInput.val();
            var user = new User();
            user.password = password;
            user.guser = email;
            user.language = language;
            user.name = name;

            AjaxLoader.show();
            user.create(
                    function() {
                        if (typeof succesCallback === 'function'){
                            succesCallback();
                        };
                        app.user.initSession(
                            user.guser,
                            user.password,
                            function() {
                                app.navigateToPage('main_page');
                            },
                            function() {
                                AjaxLoader.hide();
                                app.notify(Resources.get('login_fault'));
                            }
                        );
                        user = null;
                    },
                    function() {
                        RegistrationPage.clearPassword();
                        AjaxLoader.hide();
                        if (typeof errorCallback === 'function'){
                            errorCallback();
                        }
                        user = null;
                    }
            );
        };

        var _onShow = function() {
            if (!showed) {
                menuButton = BasePage.addIconButton(
                    'login_page',
                    header,
                    'menu',
                    function onClickEvent(e) {
                        app.menuButtonHandler(e);
                    }
                );
                if (!heightCalculated) {
                    BasePage.setHeight(page);
                    heightCalculated = true;
                };
            }
            passwordInput.val("");
            passwordConfInput.val("");
            nameInput.val("");


            AjaxLoader.hide();
            showed = true;
        };

        return {
            init: function() {
                page = $('#registration_page');
                page.on(
                    'pageshow',
                    function(event) {
                        _onShow();
                    }
                );
                content = $('#registration_page_content');
                footer = $('#registration_page_footer');
                header = $('#registration_page_header');
                content.append(reg_html()).trigger('create');
                footer.append(footer_html()).trigger('create');

                var that = this;
                $('#registration_ok').unbind("click").click(function() {that.submit();});


                var emailsOptions = '';
                jQuery.each(app.user.emails, function(i, val) {
                    emailsOptions = emailsOptions + "<option value='" + val + "'>" + val + "</option>";
                });
                var select = '<select id="registration_email">' + emailsOptions + '</select>';
                $('#email_select_holder').append($(select)).trigger('create');

                var languages = Resources.getArray('languages');
                var languagesCodes = Resources.getArray('languagesCodes');
                var languagesOptions = '';
                jQuery.each(languages, function(i, val) {
                    var selected = '';
                    if (languagesCodes[i] == app.user.language) {
                        selected = 'selected';
                    };
                    languagesOptions = languagesOptions + "<option " + selected + " value='" + languagesCodes[i] + "'>" + val + "</option>";
                });
                var selectLanguage = '<select id="registration_language">' + languagesOptions + '</select>';
                $('#language_select_holder').append($(selectLanguage)).trigger('create');

                passwordInput = $('#registration_password');
                passwordConfInput = $('#registration_confirm_password');
                nameInput = $('#registration_name');
                emailInput = $('#registration_email');
                languageInput = $('#registration_language');
            },
            clearPassword: function() {
                passwordInput.val('');
                passwordConfInput.val('');
            },
            submit: function(succesCallback,errorCallback) {
                _submit(succesCallback,errorCallback);
            },
            getRunnedFunc: function() { //need for testing
                return runnedFunc;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
