Timer = function() {
    var timer = $('<div class="timer_div">0:00<div/>');
    var direction = null;
    var startSeconds = null;
    var calcTimeFromSeconds = function(seconds) {
        var min = Math.floor(seconds/60);
        var sec = (seconds - (min * 60)).toString();
        if(sec.length == 1) {
            sec = '0' + sec;
        }
        return min + ':' + sec;
    };
    return {
        bind: function(holder) {
            holder.append(timer);
        },
        tick: function(secondsCounter) {
            //TODO Popravit' direction dolzhen byt' up chtob neputatsya
            if (direction == null) {
                timer.text(calcTimeFromSeconds(secondsCounter));
            } else if(direction == 'down') {
                startSeconds--;
                timer.text(calcTimeFromSeconds(startSeconds));
            }
        },
        clear: function(initSeconds, dir) {
            //TODO Popravit' direction dolzhen byt' up chtob neputatsya
            if (dir != null) {
                direction = dir;
            } else {
                direction = null;
            }
            if (initSeconds == null) {
                startSeconds = 0;
            } else {
                startSeconds = initSeconds;
            }
            timer.text(calcTimeFromSeconds(startSeconds));
        },
        text: function(text) {
            timer.text(text);
        },
        getTimerContainer: function() {
            return timer;
        }
    }
}
