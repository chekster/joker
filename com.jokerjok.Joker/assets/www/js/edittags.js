EditTags = function() {
    var tags = [];
    var tags_find = [];
    var add_form = null;
    var add_button = null;
    var add_text = null;
    var add_tag_tips = null;
    var tags_field = null;
    var tags_div = null;
    var prefix = null;
    var _editCallback = null;

    var onAddButtonClick = function(e) {
        var tag_text = add_text.val();
        if(tag_text.length >= 3) {
            add_tag_tips.addClass("invisible");
            add_text.val('');
            var is_tag_alredy_exist = false;
            for (var i = 0; i < tags.length; i++) {
                if (tags[i] == tag_text) {
                    is_tag_alredy_exist = true;
                };
            }
            if (!is_tag_alredy_exist) {
                if (_editCallback != null) {
                    _editCallback();
                };
                tags.push(tag_text);
                //add tag button with self removing function onClick
                var tag_button = $('<input type="submit" value="' + tag_text + '" data-mini="true" data-inline="true" data-icon="delete" data-iconpos="right" />');
                tag_button.click(function() {
                    var remove_tag = $(this).val();
                    tags = jQuery.grep(tags, function(tag) {
                        return tag != remove_tag;
                    });
                    $(this).closest('.ui-btn').remove();
                    if (_editCallback != null) {
                        _editCallback();
                    };
                });
                tags_field.prepend(tag_button).trigger('create');
            };
        }
        //add_text.focus();
        return false;
    };

    var onAddTextChange = function(e) {
        var tag_text = add_text.val();
        if(tag_text.length >= 3) {
            add_button.button('enable');
            app.ajax(
                '/tags/find.json',
                'GET',
                {token:app.user.token,findtag:tag_text},
                function(data) {
                    tags_find = [];
                    $.each(data, function(index, tag) {
                        var t = new Tag(tag.id,tag.tag);
                        tags_find.push(t);
                    });
                    if (tags_find.length > 0) {
                        add_tag_tips.empty();
                        add_tag_tips.removeClass("invisible");
                        $.each(tags_find, function(index, tag) {
                            var is_tag_alredy_exist = false;
                            for (var i = 0; i < tags.length; i++) {
                                if (tags[i] == tag.getTag()) {
                                    is_tag_alredy_exist = true;
                                };
                            }
                            if (!is_tag_alredy_exist) {
                                var tip_p = $('<div>' + tag.getTag() + '</div>');
                                tip_p.addClass('center_div');
                                tip_p.addClass('transparent');
                                tip_p.addClass('center_text');
                                tip_p.addClass('medium_font');
                                tip_p.addClass('second_color');
                                tip_p.click(function(e) {
                                                add_text.val(tip_p.text());
                                                add_tag_tips.addClass("invisible");
                                                onAddButtonClick();
                                                if (e != null) {
                                                    e.preventDefault();
                                                    e.stopPropagation();
                                                };
                                            }
                                );
                                add_tag_tips.append(tip_p);
                            };
                        });
                    } else {
                        add_tag_tips.empty();
                        if (!add_tag_tips.hasClass("invisible")) {
                            add_tag_tips.addClass("invisible");
                        };
                    };
                },
                function(stat) {
                },
                function(stat, error) {
                }
            );
        } else {
            add_tag_tips.html('');
            if (!add_tag_tips.hasClass("invisible")) {
                add_tag_tips.addClass("invisible");
            };
            add_button.button('disable');
        }
    };

    var edit_tags_html = function() {
        var html = "";
        if (prefix == null) {
            html = "<div class='invisible add_tag_tips high_layer' id='add_tag_tips'></div>" +

                   "<div class='tags_div invisible border_radius' id='tags_div'>" +
                       "<form id='add_tag_form'>" +

                           "<label for='add_tag_text'>" + Resources.get('tags_caption') + "</label>"+
                           "<input type='text' id='add_tag_text'/>" +

                           "<input type='submit' value='Invisible tex' id='add_tag_button' data-inline='true' data-icon='plus' data-iconpos='notext' />" +

                       "</form>" +
                       "<p id='tags_field'></p>" +
                   "<div/>";
        } else {
            html = "<div class='invisible add_tag_tips high_layer' id='" + prefix + "_tag_tips'></div>" +

                    "<div class='tags_div invisible border_radius' id='" + prefix + "_tags_div'>" +
                       "<p id='" + prefix + "_tags_field'></p>" +
                       "<form id='" + prefix + "_tag_form'>" +

                           "<label for='" + prefix + "_tag_text'>" + Resources.get('tags_caption') + "</label>"+
                           "<input type='text' id='" + prefix + "_tag_text'/>" +

                           "<input type='submit' value='Invisible tex' id='" + prefix + "_tag_button' data-inline='true' data-icon='plus' data-iconpos='notext' />" +

                       "</form>" +
                   "<div/>";
        };
        return html;
    };

    return {
        addButtonClick: function(e) {
            onAddButtonClick(e);
            if (e != null) {
                e.preventDefault();
                e.stopPropagation();
            };
        },
        addTextChange: function(e) {
            onAddTextChange(e);
            if (e != null) {
                e.preventDefault();
                e.stopPropagation();
            };
        },
        bind: function(holder, pref, editCallback) {
            var that = this;
            prefix = pref;
            if (editCallback != null) {
                 _editCallback = editCallback;
            };
            holder.append(edit_tags_html()).trigger('create');
            if (prefix == null) {
                add_form = $("#add_tag_form");
                add_text = $("#add_tag_text");
                add_button = $('#add_tag_button');
                tags_field = $('#tags_field');
                tags_div = $('#tags_div');
                add_tag_tips = $('#add_tag_tips');
            } else {
                add_form = $("#" + prefix + "_tag_form");
                add_text = $("#" + prefix + "_tag_text");
                add_button = $("#" + prefix + "_tag_button");
                tags_field = $("#" + prefix + "_tags_field");
                tags_div = $("#" + prefix + "_tags_div");
                add_tag_tips = $("#" + prefix + "_tag_tips");
            };
            add_button.button('disable');
            add_text.unbind("input").bind('input',function(e) {onAddTextChange(e);});
            add_button.unbind("click").click(function(e) {that.addButtonClick(e);});
            add_form.unbind("submit").submit(function(e) {that.addButtonClick(e);});
        },
        clear: function() {
            add_text.val('');
            tags = [];
            $(tags_field).empty();
        },
        hide: function() {
            tags_div.addClass('invisible');
            if (!add_tag_tips.hasClass("invisible")) {
                add_tag_tips.addClass("invisible");
            };
        },
        show: function() {
            tags_div.removeClass('invisible');
        },
        getTags: function() {
            return tags;
        },
        addTag: function(tag) {
            add_text.val(tag);
            onAddButtonClick();
        }
    }
}

