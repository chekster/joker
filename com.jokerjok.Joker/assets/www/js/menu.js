var Menu =(function(){
	var instantiated;

	var init = function() {
        var inited = false;
        var state = null;

        var menu_container = null;
        var menu = null;
        var menu_background = null;

        var homeBtn = null;
        var recordingBtn = null;
        var myVoicesBtn = null;
        var registrationBtn = null;
        var settingsBtn = null;
        var logoutBtn = null;
        var exitBtn = null;

        var homeBtnContainer = null;
        var recordingBtnContainer = null;
        var myVoicesBtnContainer = null;
        var registrationBtnContainer = null;
        var settingsBtnContainer = null;
        var logoutBtnContainer = null;
        var exitBtnContainer = null;

        var menu_html = function() {
            var html = "<div>" +

                            "<input type='button' value='" + Resources.get('home_btn_caption') +
                            "' id='menu_home' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('record_btn_caption') +
                            "' id='menu_recording' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('my_btn_caption') +
                            "' id='menu_myvoices' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('registration_btn_caption') +
                            "' id='menu_registration' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('settings_btn_caption') +
                            "' id='menu_settings' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('logout_btn_caption') +
                            "' id='menu_logout' data-theme='b' />" +

                            "<input type='button' value='" + Resources.get('exit_btn_caption') +
                            "' id='menu_exit' data-theme='b' />" +

                        "<div/>";
            return html;
        };

        var hideButtons = function() {
            homeBtnContainer.closest('div').css('display', 'none');
            recordingBtnContainer.closest('div').css('display', 'none');
            myVoicesBtnContainer.closest('div').css('display', 'none');
            registrationBtnContainer.closest('div').css('display', 'none');
            settingsBtnContainer.closest('div').css('display', 'none');
            logoutBtnContainer.closest('div').css('display', 'none');
            exitBtnContainer.closest('div').css('display', 'none');
        };

        var showButtons = function() {
            homeBtnContainer.closest('div').css('display', 'block');
            recordingBtnContainer.closest('div').css('display', 'block');
            myVoicesBtnContainer.closest('div').css('display', 'block');
            registrationBtnContainer.closest('div').css('display', 'block');
            settingsBtnContainer.closest('div').css('display', 'block');
            logoutBtnContainer.closest('div').css('display', 'block');
            exitBtnContainer.closest('div').css('display', 'block');
        };

        return {
            clear: function() {
                inited = false;
                state = null;

                if (homeBtn != null) {
                    homeBtn.unbind("click");
                    recordingBtn.unbind("click");
                    myVoicesBtn.unbind("click");
                    registrationBtn.unbind("click");
                    settingsBtn.unbind("click");
                    logoutBtn.unbind("click");
                    exitBtn.unbind("click");
                };

                homeBtn = null;
                recordingBtn = null;
                myVoicesBtn = null;
                registrationBtn = null;
                settingsBtn = null;
                logoutBtn = null;
                exitBtn = null;

                homeBtnContainer = null;
                recordingBtnContainer = null;
                myVoicesBtnContainer = null;
                registrationBtnContainer = null;
                settingsBtnContainer = null;
                logoutBtnContainer = null;
                exitBtnContainer = null;

                $('#menu').empty();
                menu = null;
                menu_background = null;
            },
            getState: function() {
                return state;
            },
            getBackground: function() {
                return menu_background;
            },
            getContainer: function() {
                return menu_container;
            },
            getMenu: function() {
                return menu;
            },
            init: function() {
                if (!inited) {
                    menu_container = $('#menu_container');
                    menu = $('#menu');
                    menu_background = $('#menu_background');
                    menu.append(menu_html()).trigger('create');

                    homeBtn = $('#menu_home');
                    recordingBtn = $('#menu_recording');
                    myVoicesBtn = $('#menu_myvoices');
                    registrationBtn = $('#menu_registration');
                    settingsBtn = $('#menu_settings');
                    logoutBtn = $('#menu_logout');
                    exitBtn = $('#menu_exit');

                    homeBtnContainer = homeBtn.closest('div');
                    recordingBtnContainer = recordingBtn.closest('div');
                    myVoicesBtnContainer = myVoicesBtn.closest('div');
                    registrationBtnContainer = registrationBtn.closest('div');
                    settingsBtnContainer = settingsBtn.closest('div');
                    logoutBtnContainer = logoutBtn.closest('div');
                    exitBtnContainer = exitBtn.closest('div');

                    var that = this;
                    homeBtn.click(function() {that.home();});
                    recordingBtn.click(function() {that.recording();});
                    myVoicesBtn.click(function() {that.myvoices();});
                    registrationBtn.click(function() {that.registration();});
                    settingsBtn.click(function() {that.settings();});
                    logoutBtn.click(function() {that.logout();});
                    exitBtn.click(function() {that.exit();});

                    inited = true;
                };
            },
            home: function() {
                this.hide();
                app.navigateToPage('main_page');
            },
            recording: function() {
                this.hide();
                if (app.user.registered) {
                    app.player.stop();
                    AjaxLoader.show();
                    /*
                    if (!app.ui.recordingPage.heightCalculated) {
                        AjaxLoader.show();
                    }
                    */
                    app.navigateToPage('recording_page');
                } else {
                    app.notify(Resources.get('not_registered_cant_rec_msg'));
                };
            },
            registration: function() {
                this.hide();
                AjaxLoader.show();
                app.navigateToPage('registration_page');
            },
            myvoices: function() {
                this.hide();
                AjaxLoader.show();
                MyVoicesPage.show();
            },
            settings: function() {
                this.hide();
                Settings.show();
            },
            logout: function() {
                this.hide();
                app.user.logout();
                app.navigateToPage('start_page');
            },
            exit: function() {
                this.hide();
                navigator.app.exitApp();
            },
            show: function() {
                if (!inited) {
                    this.init();
                };
                if ('start_page' == app.activePage) {
                    hideButtons();
                    exitBtnContainer.css('display', 'block');
                } else if ('registration_page' == app.activePage) {
                    if (!app.user.session) {
                        hideButtons();
                        exitBtnContainer.css('display', 'block');
                    } else if (app.user.session && !app.user.registered) {
                        showButtons();
                        myVoicesBtnContainer.css('display', 'none');
                        registrationBtnContainer.css('display', 'none');
                    }
                } else if ('login_page' == app.activePage) {
                    hideButtons();
                    exitBtnContainer.css('display', 'block');
                } else if ('main_page' == app.activePage) {
                    showButtons();
                    homeBtnContainer.css('display', 'none');
                    if (!app.user.registered) {
                        myVoicesBtnContainer.css('display', 'none');
                    } else {
                        registrationBtnContainer.css('display', 'none');
                    }
                } else if ('myvoices_page' == app.activePage) {
                    showButtons();
                    myVoicesBtnContainer.css('display', 'none');
                    registrationBtnContainer.css('display', 'none');
                } else if ('recording_page' == app.activePage) {
                    showButtons();
                    recordingBtnContainer.css('display', 'none');
                    registrationBtnContainer.css('display', 'none');
                };
                state = 'show';
                menu_container.css('display','block');
            },
            hide: function() {
                state = 'hide';
                menu_container.css('display','none');
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
