var MyVoicesPage =(function(){
	var instantiated;

	var init = function() {
        var page = null;
        var content = null;
        var header = null;
        var content_voices_list = null;
        //TODO rename it to footer
        var controlsBar = null;
        var playButton = null;
        var editButton = null;
        var menuButton = null;
        var micButton = null;
        var worldButton = null;
        var deleteButton = null;
        var voice = null;
        var timer = null;
        var heightCalculated = false;
        var showed = false;
        var voicesLoaded = false;
        var runnedFunc = null;

        var content_html = function() {
            var html = "";
            return html;
        };

        var _setCurrentVoice = function(v) {
            if (showed && voicesLoaded) {
                if (v != null) {
                    if (voice != null) {
                        $('#voice_' + voice.id).removeClass('active');
                    };
                    voice = v;
                    $('#voice_' + voice.id).addClass('active');
                };
            };
        };

        var _bindMyVoicesList = function() {
            voicesLoaded = true;
            if (MyVoicesList.isChanged()) {
                MyVoicesList.changesAccepted();
                content_voices_list.empty();
                var voicesListItemsHTML = '';
                MyVoicesList.getFirstVoice();
                while ( MyVoicesList.anyNextVoice() ){
                    voicesListItemsHTML = voicesListItemsHTML + _getVoiceHTML(MyVoicesList.getVoice());
                    MyVoicesList.getNextVoice();
                }
                var lastVoice = MyVoicesList.getVoice();
                if (lastVoice != null) {
                    voicesListItemsHTML = voicesListItemsHTML + _getVoiceHTML(lastVoice);
                };
                content_voices_list.append(voicesListItemsHTML);
                content_voices_list.listview( "refresh" );
                content_voices_list.children('li').click(
                        function(e) {
                            _voiceClick(this);
                            if (e != null) {
                                e.preventDefault();
                                e.stopPropagation();
                            };
                        }
                );
                _setCurrentVoice(MyVoicesList.getFirstVoice());
                _setHeight();
            } else {
            };
        };

        var _getVoiceHTML = function(v) {
            if (v != null) {
                var html_tmpl = '<li id="voice_' + v.id + '">' +
                                    '<div class="time_box small_font">' +
                                        v.calcTimeFromSeconds() +
                                    '</div>' +
                                    '<div class="second_color tags">' +
                                        v.getTagsText() +
                                    '</div>' +
                                    /*
                                    '<div class="spams vote">' +
                                        '<div class="small_font">' + v.spams + '</div> ' + '<div/>' +
                                    '</div>' +
                                    */
                                    '<div class="dislikes vote">' +
                                        '<div class="small_font">' + v.dislikes + '</div> ' + '<div/>' +
                                    '</div>' +
                                    '<div class="likes vote">' +
                                        '<div class="small_font">' + v.likes + '</div> ' + '<div/>' +
                                    '</div>' +
                                '</li>';
                return html_tmpl;
            } else {
                log('MyVoicesList _getVoiceHTML null');
                return '';
            };
        }

        var _voiceClick = function(listItem) {
            //TODO try here web worker
            _stopPlaying();
            var id = $(listItem).attr('id').substr(6);
            _setCurrentVoice(MyVoicesList.getVoiceByID(id));
            return false;
        };

        var _stopPlaying = function() {
            if(app.player.playStatus) {
                $('.ui-icon-pause').removeClass("ui-icon-pause").addClass("ui-icon-play");
                app.player.stop();
            }
        };

        var _playButtonClick = function(onPlayFinish, onLoad) {
            runnedFunc = 'playButtonClick';
            if(voice != null) {
                if(!app.player.playStatus) {
                    $('.ui-icon-play').removeClass("ui-icon-play").addClass("ui-icon-pause");
                    if (voice.SM == null) {
                        voice.load(onPlayFinish, onLoad);
                    };
                    app.player.play(voice, null, onLoad);
                } else {
                    _stopPlaying();
                }
            } else {
                log('myvoicespage playButtonClick voice is null');
            }
        };

        var _editButtonClick = function() {
            runnedFunc = 'editButtonClick';
            if(voice != null) {
                EditVoice.show(voice);
            } else {
                log('myvoicespage editButtonClick voice is null');
            }
        };

        var _deleteVoice = function(button) {
            if (button == 1) {
                voice.deleteVoice(_deleteVoiceCallback);
            };
        };

        var _deleteVoiceCallback = function() {
            $('#voice_' + voice.id).remove();
            _setCurrentVoice(MyVoicesList.getVoice());
        };

        var _deleteButtonClick = function() {
            runnedFunc = 'deleteButtonClick';
            if (!app.testing) {
                if(voice != null) {
                    navigator.notification.confirm(
                        Resources.get('delete_voice_msg'),
                        _deleteVoice,
                        Resources.get('app_name')
                    );
                } else {
                    log('myvoicespage deleteButtonClick voice is null');
                }
            }
        };

        var _menuButtonClick = function(e) {
            runnedFunc = 'menuButtonClick';
            app.menuButtonHandler(e);
        };

        var _worldButtonClick = function() {
            runnedFunc = 'worldButtonClick';
            _stopPlaying();
            app.navigateToPage('main_page');
        };

        var _micButtonClick = function() {
            runnedFunc = 'micButtonClick';
            _stopPlaying();
            app.navigateToPage('recording_page');
        };

        var _initIconButtons = function() {
            playButton = BasePage.addIconButton(
                'myvoices_page',
                controlsBar,
                'play',
                function onClickEvent(e) {
                    _playButtonClick(_playButtonClick);
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                }
            );
            editButton = BasePage.addIconButton(
                'myvoices_page',
                controlsBar,
                'edit',
                function onClickEvent(e) {
                    _editButtonClick();
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                }
            );
            deleteButton = BasePage.addIconButton(
                'myvoices_page',
                controlsBar,
                //TODO change CSS classes to delete instead cancel
                'cancel',
                function onClickEvent(e) {
                    _deleteButtonClick();
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                }
            );
            worldButton = BasePage.addIconButton(
                'myvoices_page',
                header,
                'world',
                function onClickEvent(e) {
                    _worldButtonClick();
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                }
            );
            menuButton = BasePage.addIconButton(
                'myvoices_page',
                header,
                'menu',
                function onClickEvent(e) {
                    _menuButtonClick(e);
                }
            );
            micButton = BasePage.addIconButton(
                'myvoices_page',
                header,
                'mic',
                function onClickEvent(e) {
                    _micButtonClick();
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                }
            );
        };

        var _onShow = function() {
            if (!showed) {
                showed = true;
                //dlya togo chtoby knopki renderilis' krasivo ih nado renderit'
                //posle 'pageshow' kak na recording page i zdes'
                _initIconButtons();
                _setHeight();
            };
            //_setCurrentVoice(MyVoicesList.getFirstVoice());
        };

        var _setHeight = function() {
            if (showed && voicesLoaded) {
                if (!heightCalculated) {
                    BasePage.setHeight(page);
                    content_voices_list.css('height', content.outerHeight());
                    heightCalculated = true;
                }
            }
        }

        return {
            clear: function() { //need for testing (maybe for feutere cleaning)
                if (page != null) {
                    page.unbind("pageshow");
                };
                if (content != null) {
                    content.empty();
                };
                page = null;
                content = null;
                controlsBar = null;
                playButton = null;
                deleteButton = null;
                menuButton = null;
                micButton = null;
                worldButton = null;
                voice = null;
                timer = null;
                heightCalculated = false;
                showed = false;
                voicesLoaded = false;
            },
            init: function() {
                page = $('#myvoices_page');
                page.on(
                    'pageshow',
                    function(e) {
                        _onShow();
                        //Do not stop this event e
                    }
                );

                content = $('#myvoices_page_content');
                content_voices_list = $('#myvoices_page_list');
                controlsBar = $('#myvoices_page_controls_bar');
                header = $('#myvoices_page_header');
            },
            onShow: function() { //need for testing
                _onShow();
            },
            show: function() {
                voicesLoaded = false;
                app.navigateToPage('myvoices_page');
                MyVoicesList.loadList(MyVoicesPage.bindMyVoicesList);
            },
            bindMyVoicesList: function() {
                _bindMyVoicesList();
            },
            updateCurrentVoice: function() {
                var tags = voice.getTagsText();
                var tags_div = $('#voice_' + voice.id + '>div.tags').first();
                tags_div.empty();
                tags_div.text(tags);
            },
            backBtnClick: function() {
                _worldButtonClick();
            },
            getRunnedFunc: function() { //need for testing
                return runnedFunc;
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
