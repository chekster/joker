var BasePage =(function(){
	var instantiated;

	var init = function() {

        var _addIconButton = function(pageName,holder,icon,onClickFunction) {
            var id = pageName + '_' + icon + '_button';
            var iconButton = $('<input type="submit" value="Invisible tex" id="' + id + '" data-inline="true" data-icon="' + icon + '" data-iconpos="notext" />');
            iconButton.click(function(e) {
                                if (e != null) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                };
                                onClickFunction();
                            });
            holder.append(iconButton).trigger('create');
            return iconButton;
        };

        var _setHeight = function(page) {
            scroll(0, 0);
            var header = page.find('[data-role="header"]');
            var footer = page.find('[data-role="footer"]');
            var content = page.find('[data-role="content"]');
            var viewport_height = $(window).height();
            var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
            content_height -= (content.outerHeight() - content.height());
            content.height(content_height);
        };

        return {
            addIconButton: function(pageName,holder,icon,onClickFunction) {
                return _addIconButton(pageName,holder,icon,onClickFunction);
            },
            setHeight: function(page) {
                _setHeight(page);
            }
        }
	};


    var getInstance = function() {
        if (!instantiated){
            instantiated = init();
        }
        return instantiated;
    }

	return getInstance();
})();
