MainPage = function() {
    var page = null;
    var goRecordingButton = null;
    var sortTypeButton = null;
    var sortTypeButtonText = null;
    var sortTagsCaption = null;
    var playButton = null;
    var nextButton = null;
    var previousButton = null;
    var likeButton = null;
    var unlikeButton = null;
    var menuButton = null;
    var micButton = null;
    var homeButton = null;
    var spamButton = null;
    var likeCounter = null;
    var unlikeCounter = null;
    var spamCounter = null;
    var voice = null;
    var content = null;
    var header = null;
    //TODO rename it to footer
    var controlsBar = null; //footer
    var timerBar = null;
    var tagsBar = null;
    var sortBar = null;
    var countBar = null;
    var voteBar = null;
    var countersBar = null;
    var timer = null;

    var playModes = ['man','semauto','auto'];
    var playMode = 1;

    var runnedFunc = null;
    var heightCalculated = false;
    var showed = false;
    var needRefresh = true;
    var _isUserVotedNow = false;
    var _moveNextAfterVoteTimeoutId = 0;

    var _bind = function() {
        var that = this;
        timer.bind(timerBar);

        sortTagsCaption = $("<div id='main_page_sort_tags'></div>");
        sortBar.append(sortTagsCaption);
        sortTagsCaption.addClass('center_text');
        sortTagsCaption.addClass('small_font');
        sortTagsCaption.addClass('second_color');
        sortTagsCaption.addClass('break_line');
        sortTagsCaption.addClass('whole_width');

        sortTypeButton = $('<a id="main_page_sort_button" data-role="button" data-theme="b">' + Resources.get(Playlist.getSortingType() + '_sort_type_btn_caption') + '</a>');
        sortTypeButton.click(function(e) {
            _sortTypeButtonClick();
            if (e != null) {
                e.preventDefault();
                e.stopPropagation();
            };
        });
        sortBar.append(sortTypeButton).trigger('create');
        sortTypeButtonText = sortTypeButton.find('.ui-btn-text').first();

        //Rendering of play, next, previous buttons are in _onShow function
        spamButton = BasePage.addIconButton(
            'main_page',
            voteBar,
            'spam',
            function onClickEvent() {
                _spamButtonClick();
            }
        );
        unlikeButton = BasePage.addIconButton(
            'main_page',
            voteBar,
            'unlike',
            function onClickEvent() {
                _unlikeButtonClick();
            }
        );
        likeButton = BasePage.addIconButton(
            'main_page',
            voteBar,
            'like',
            function onClickEvent() {
                _likeButtonClick();
            }
        );
        spamCounter = _addCounter(countersBar,'spam');
        unlikeCounter = _addCounter(countersBar,'unlike');
        likeCounter = _addCounter(countersBar,'like');
    };

    var _addCounter = function(holder,icon) {
        var id = 'main_page' + '_' + icon + '_counter';
        var counter = $('<div class="voteCounter" id="' + id + '" />');

        holder.append(counter);
        return counter;
    };

    var _stopPlaying = function(pressedManualy) {
        if(app.player.playStatus) {
            $('.ui-icon-pause').removeClass("ui-icon-pause").addClass("ui-icon-play");
            app.player.stop();
            if (!pressedManualy) {
                if (playMode == 1) {
                    _moveNextVoiceIfUserVoted();
                } else if (playMode == 2) {
                    _nextButtonClick();
                };
            };
        }
    };

    var _previousButtonClick = function() {
        runnedFunc = 'previousButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        _stopPlaying(true);//if move to previous in auto mode - stop playing
        _setCurrentVoice(Playlist.getPreviousVoice());
        //load next voice
        previousVoice = Playlist.getPreviousVoiceWithoutShift();
        if (previousVoice != null) {
            previousVoice.load(_playButtonClick, null);
        };
        //unload second next voice
        nextNextVoice = Playlist.getNextNextVoiceWithoutShift();
        if (nextNextVoice != null) {
            nextNextVoice.unload();
        };

    };

    var _playButtonClick = function(pressedManualy, onStartPlay) {
        runnedFunc = 'playButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        if(voice != null) {
            timer.clear(voice.duration,'down');
            if(!app.player.playStatus) {
                $('.ui-icon-play').removeClass("ui-icon-play").addClass("ui-icon-pause");
                app.player.play(voice, timer, onStartPlay);
            } else {
                _stopPlaying(pressedManualy);
            }
        } else {
            log('main page playButtonClick voice is null');
        }
    };

    var _nextButtonClick = function() {
        runnedFunc = 'nextButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        _stopPlaying(true);
        _setCurrentVoice(Playlist.getNextVoice());
        //load next voice
        nextVoice = Playlist.getNextVoiceWithoutShift();
        if (nextVoice != null) {
            nextVoice.load(_playButtonClick, null);
        };
        //unload second previous voice
        previousPreviousVoice = Playlist.getPreviousPreviousVoiceWithoutShift();
        if (previousPreviousVoice != null) {
            previousPreviousVoice.unload();
        };
        //if playMode is 'semauto' or 'auto', play imideatly after switching to next voice
        if (playMode > 0) {
            _playButtonClick(false, _enableVote);
        };
    };

    var _spamButtonClick = function() {
        runnedFunc = 'spamButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        if(voice != null) {
            if (voice.user_spam != null) {
                voice.removeSpamReport(app.ui.mainPage.setVotesButtonsState);
            } else {
                voice.addSpamReport(app.ui.mainPage.setVotesButtonsState);
            }
            _isUserVotedNow = true;
        }
    };

    var _unlikeButtonClick = function() {
        runnedFunc = 'unlikeButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        if(voice != null) {
            if (voice.user_like != null) {
                if (!voice.user_like.like) {
                    voice.removeLike(app.ui.mainPage.setVotesButtonsState);
                } else {
                    voice.addLike(false, app.ui.mainPage.setVotesButtonsState);
                }
            } else {
                voice.addLike(false, app.ui.mainPage.setVotesButtonsState);
            }
            _isUserVotedNow = true;
        }
    };

    var _likeButtonClick = function() {
        runnedFunc = 'likeButtonClick';
        // if waiting to move next voice - stop waiting
        _stopWaitingMoveNextVoice();
        if(voice != null) {
            if (voice.user_like != null) {
                if (voice.user_like.like) {
                    voice.removeLike(app.ui.mainPage.setVotesButtonsState);
                } else {
                    voice.addLike(true, app.ui.mainPage.setVotesButtonsState);
                }
            } else {
                voice.addLike(true, app.ui.mainPage.setVotesButtonsState);
            }
            _isUserVotedNow = true;
        }
    };

    var _setCurrentVoice = function(v) {
        voice = v;
        if (v != null) {
            timer.getTimerContainer().removeClass("small_font");
            //nameBar.addClass("large_font");
            //nameBar.removeClass("medium_font");
            nameBar.removeClass("shift");
            timerBar.removeClass("shift");

            timer.clear(voice.duration,'down');
            //fill tags
            if ( (voice.tags != null) && (voice.tags.length > 0) ) {
                tagsBar.empty();
                jQuery.each(voice.tags, function() {
                    var tag_span = "<span id='tag_" + this.id + "'>#" + this.tag + " </span>";
                    tagsBar.append(tag_span);
                    var tags = [ this.tag ];
                    $('#tag_' + this.id).on('click',function(e){
                        Playlist.setSortingType(Playlist.getSortingTypeCode(), tags);
                        app.ui.mainPage.setCurrentSortingType();
                        if (e != null) {
                            e.preventDefault();
                            e.stopPropagation();
                        };
                    });
                });
                timerBar.removeClass("center_text");
                timerBar.removeClass("whole_width");
                tagsBar.removeClass("zero_width");
            } else {
                tagsBar.empty();
                tagsBar.text('');
                timerBar.addClass("center_text");
                timerBar.addClass("whole_width");
                tagsBar.addClass("zero_width");
            };
            nameBar.text(voice.user_name);
            countBar.text((Playlist.getVoiceIndex() + 1) + '/' + Playlist.voicesSortingCount());
        } else {
            //TODO zapisat' special'nyi golos kotoryj but vozvrawatsya serverom esli on nichego ne nawel
            //TODO predlozhit' ih zapisat' chuwaku s jokerovskim golosom
            //
            countBar.text('');
            tagsBar.text('');

            timer.getTimerContainer().addClass("small_font");
            //nameBar.removeClass("large_font");
            //nameBar.addClass("medium_font");
            nameBar.addClass("shift");
            timerBar.addClass("shift");
            timerBar.addClass("center_text");
            timerBar.addClass("whole_width");
            tagsBar.addClass("zero_width");

            //sort by current user likes

            if ( (Playlist.getSortingTypeCode() == 3) && (Playlist.getSortingTags().length == 0)) {
                nameBar.text(Resources.get('you_dont_have_likes_msg'));
                timer.text(Resources.get('do_you_like_something_msg'));
            } else {
                nameBar.text(Resources.get('tell_me_some_joke_msg'));
                timer.text(Resources.get('is_no_joks_for_query_msg'));
            };
        };

        if (!Playlist.anyNextNextVoice()) {
            //na prod ne podgruzhet voices esli etogo loga zdes' net ????
            console.warn('joker');
            Playlist.loadList();
        }
        _setButtonsState();
        _show();
    };

    var _setVotesButtonsState = function() {
        likeButton.buttonMarkup({theme: 'c'});
        unlikeButton.buttonMarkup({theme: 'c'});
        spamButton.buttonMarkup({theme: 'c'});

        if (voice == null) {
            voteBar.addClass('invisible');
            countersBar.addClass('invisible');
        } else {
            voteBar.removeClass('invisible');
            countersBar.removeClass('invisible');

            spamCounter.text(voice.spams);
            unlikeCounter.text(voice.dislikes);
            likeCounter.text(voice.likes);

            if ( (voice != null) && ( (voice.user_like != null) || (voice.user_spam != null) || voice.listened ) ) {
                _enableVote();

                if (voice.user_like != null) {
                    if (voice.user_like.like) {
                        likeButton.buttonMarkup({theme: 'b'});
                    } else {
                        unlikeButton.buttonMarkup({theme: 'b'});
                    };
                };
                if (voice.user_spam != null) {
                    spamButton.buttonMarkup({theme: 'b'});
                };
                //after user voted wait half sec and move to next voice
                if(!app.player.playStatus) {
                    if (playMode == 1) {
                        _moveNextVoiceIfUserVoted();
                    };
                };
            } else {
               _disableVote();
            };
        };
    };

    var _enableVote = function(enable) {
        likeButton.button('enable');
        unlikeButton.button('enable');
        spamButton.button('enable');
    };

    var _disableVote = function(enable) {
        likeButton.button('disable');
        unlikeButton.button('disable');
        spamButton.button('disable');
    };

    var _setButtonsState = function() {
        _setVotesButtonsState();
        if( !app.player.playStatus && (voice != null) ) {
            playButton.button('enable');
        } else {
            playButton.button('disable');
        }
        if (Playlist.anyNextVoice() && (voice != null) ) {
            nextButton.button('enable');
        } else {
            nextButton.button('disable');
        }
        if (Playlist.anyPreviousVoice() && (voice != null) ) {
            previousButton.button('enable');
        } else {
            previousButton.button('disable');
        }
    };

    var _goRecordingButtonClick = function(e) {
        runnedFunc = 'goRecordingButtonClick';
        _stopPlaying();

        if (app.user.registered) {
            app.navigateToPage('recording_page');
        } else {
            app.notify(Resources.get('not_registered_cant_rec_msg'));
        };
    };

     var _sortTypeButtonClick = function() {
        runnedFunc = 'sortTypeButtonClick';
        _stopPlaying();
        Sorting.show(app.ui.mainPage.setCurrentSortingType);
    }

    var _onShow = function() {
        //dlya togo chtoby knopki renderilis' krasivo ih nado renderit'
        //posle 'pageshow' kak na recording page i zdes'
        if (needRefresh) {
            Playlist.clearList();
            Playlist.loadList(app.ui.mainPage.setCurrentVoice);
            needRefresh = false;
        };
        if (!showed) {
            homeButton = BasePage.addIconButton(
                'main_page',
                header,
                'home',
                function onClickEvent() {
                    if (app.user.registered) {
                        MyVoicesPage.show();
                    } else {
                        app.notify(Resources.get('not_registered'));
                    };
                }
            );
            menuButton = BasePage.addIconButton(
                'main_page',
                header,
                'menu',
                function onClickEvent(e) {
                    app.menuButtonHandler(e);
                }
            );
            micButton = BasePage.addIconButton(
                'main_page',
                header,
                'mic',
                function onClickEvent() {
                    _goRecordingButtonClick();
                }
            );
            previousButton = BasePage.addIconButton(
                'main_page',
                controlsBar,
                'previous',
                function onClickEvent() {
                    _previousButtonClick();
                }
            );
            playButton = BasePage.addIconButton(
                'main_page',
                controlsBar,
                'play',
                function onClickEvent() {
                    _playButtonClick(true, _enableVote);
                }
            );
            nextButton = BasePage.addIconButton(
                'main_page',
                controlsBar,
                'next',
                function onClickEvent() {
                    _nextButtonClick();
                }
            );
            if (!heightCalculated) {
                BasePage.setHeight(page);
                heightCalculated = true;
            };
        } else {
            _show();
        };
        showed = true;
    };

    var _show = function() {
    };

    var _stopWaitingMoveNextVoice = function() {
        if (_moveNextAfterVoteTimeoutId != 0) {
            clearTimeout(_moveNextAfterVoteTimeoutId);
            _moveNextAfterVoteTimeoutId = 0;
        };
    };

    var _moveNextVoiceIfUserVoted = function() {
        if (_isUserVotedNow) {
            _isUserVotedNow = false;
            _moveNextAfterVoteTimeoutId = setTimeout(function() {
                _nextButtonClick();
            }, 500);
        };
    };

    return {
        init: function() {
            page = $('#main_page');
            page.on(
                'pageshow',
                function(e) {
                    _onShow();
                    //Do not stop this event e
                }
            );
            content = $('#main_page_content');
            header = $('#main_page_header');
            sortBar = $('#main_page_sort_bar');
            countBar = $('#main_page_count_bar');
            voteBar = $('#main_page_vote_bar');
            timerBar = $('#main_page_timer_bar');
            tagsBar = $('#main_page_tags_bar');
            timerBar.addClass('medium_font');
            controlsBar = $('#main_page_controls_bar');
            nameBar = $('#main_page_name_bar');
            countersBar = $('#main_page_votes_counters_bar>div');
            timer = Timer();

            if (window.localStorage.getItem("sortingType") != null) {
                Playlist.setSortingType(window.localStorage.getItem("sortingType"));
            };

            _bind();
        },
        playButtonClick: function() {
            that = this;
            _playButtonClick(false, that.enableVote);
        },
        setVotesButtonsState: function(enable) {
            _setVotesButtonsState(enable);
        },
        setCurrentVoice: function(voice) {
            _setCurrentVoice(voice);
        },
        getTimer: function() {
            return timer;
        },
        clearLoadVoicesAndSetCurrentVoice: function() {
            Playlist.clearList();
            Playlist.loadList(app.ui.mainPage.setCurrentVoice);
        },
        setCurrentSortingType: function() {
            sortTypeButtonText.text(Resources.get(Playlist.getSortingType() + '_sort_type_btn_caption'));
            sortTagsCaption.empty();
            jQuery.each(Playlist.getSortingTags(), function() {
                var tag_span = "<span id='tag_" + this + "'>#" + this + " </span>"
                sortTagsCaption.append(tag_span);
                var tags = [ this ];
                $('#tag_' + this).on('click',function(e){
                    Playlist.setSortingType(Playlist.getSortingTypeCode(), tags);
                    app.ui.mainPage.setCurrentSortingType();
                    if (e != null) {
                        e.preventDefault();
                        e.stopPropagation();
                    };
                });
            });
            app.ui.mainPage.clearLoadVoicesAndSetCurrentVoice();
        },
        needRefresh: function() {
            needRefresh = true;
        },
        getRunnedFunc: function() {
            return runnedFunc;
        }
    }
}

