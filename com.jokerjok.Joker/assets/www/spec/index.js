/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
//TODO add voice tests

describe('app', function() {

    if(isAndroid()) {
        describe('initialize', function() {
            beforeEach(function() {
                loadFixtures("index.html");
                $("#main_page").attr("data-role","page").trigger("create");
                $("#recording_page").attr("data-role","page").trigger("create");
            });
            it('should bind deviceready', function() {
                runs(function() {
                    spyOn(app, 'onDeviceReady');
                    app.initialize();
                    helper.trigger(window.document, 'deviceready');
                });

                waitsFor(function() {
                    return (app.onDeviceReady.calls.length > 0);
                }, 'onDeviceReady should be called once', 500);

                runs(function() {
                    expect(app.onDeviceReady).toHaveBeenCalled();
                });
            });

            it('should bind online', function() {
                runs(function() {
                    spyOn(app, 'onOnline');
                    app.initialize();
                    helper.trigger(window.document, 'online');
                });

                waitsFor(function() {
                    return (app.onOnline.calls.length > 0);
                }, 'onOnline should be called once', 500);

                runs(function() {
                    expect(app.onOnline).toHaveBeenCalled();
                });
            });

            it('should bind offline', function() {
                runs(function() {
                    spyOn(app, 'onOffline');
                    app.initialize();
                    helper.trigger(window.document, 'offline');
                });

                waitsFor(function() {
                    return (app.onOffline.calls.length > 0);
                }, 'onOffline should be called once', 500);

                runs(function() {
                    expect(app.onOffline).toHaveBeenCalled();
                });
            });
        });
        describe('onDeviceReady', function() {
            it('should report that it fired', function() {
                spyOn(app, 'receivedEvent');
                var e = document.createEvent('Event');
                e.initEvent('deviceready', true, true);
                app.onDeviceReady(e);
                expect(app.receivedEvent).toHaveBeenCalledWith('deviceready');
            });
        });
    }
    it('should exist', function() {
        expect(!jQuery.isEmptyObject(app)).toEqual(true);
    });
    it('should content 24 methods', function() {
        var len = Object.keys(app).length;
        expect(len).toEqual(24);
    });


    describe('recording page', function() {
        beforeEach(function() {
            loadFixtures("index.html");
            app.initialize();
        });
        describe('initialization', function() {
            it('page should exist', function() {
                expect(!jQuery.isEmptyObject(app.ui.recordingPage)).toEqual(true);
            });
            it('controls bar should exist', function() {
                expect(!jQuery.isEmptyObject(app.ui.recordingPage.controlsBar)).toEqual(true);
            });
            it('header should exist', function() {
                expect(app.ui.recordingPage.header.length).not.toEqual(0);
            });
            it('controls bar should not contain buttons before page activation', function() {
                expect($('#controls_bar').children().length).toEqual(0);
            });


            it('timer should exist', function() {
                expect(!jQuery.isEmptyObject(app.ui.recordingPage.timer)).toEqual(true);
            });

            it('tags widget should exist', function() {
                expect(!jQuery.isEmptyObject(app.ui.recordingPage.tags)).toEqual(true);
            });
            it('tags widget should be invisible before recording', function() {
                expect($('.tags_div').hasClass('invisible')).toEqual(true);
            });
        });
        describe('activation', function() {
            beforeEach(function() {
                app.ui.setRecordingPageActive();
            });
            describe('widgets state', function() {
                it('controls bar should contain 4 buttons after page activation', function() {
                    expect($('#controls_bar').children().length).toEqual(4);
                });
                it('header should contain 3 buttons after page activation', function() {
                    expect($('#recording_page_header').children().length).toEqual(3);
                });
                it('timer should be seted for "0:00"', function() {
                    expect($('#recording_page_content .timer_div')[0].innerText).toEqual('0:00');
                });
                it('player status should be false', function() {
                    expect(app.player.playStatus).toEqual(false);
                });
                it('player status should be false', function() {
                    expect(app.recorder.recStatus).toEqual(false);
                });
            });
            describe('buttons state', function() {
                it('recording should be enabled', function() {
                    expect($('#recording_page_record_button').closest('div').attr('aria-disabled')).toEqual('false');
                    expect($('#recording_page_record_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-record')).toEqual(true);
                });
                it('play should be disabled', function() {
                    expect($('#recording_page_rplay_button').closest('div').attr('aria-disabled')).toEqual('true');
                });
                it('save should be disabled', function() {
                    expect($('#recording_page_save_button').closest('div').attr('aria-disabled')).toEqual('true');
                });
                it('cancel should be enabled', function() {
                    expect($('#recording_page_cancel_button').closest('div').attr('aria-disabled')).toEqual('false');
                });
                it('record button must be record button', function() {
                    expect($('#recording_page_record_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-record')).toEqual(true);
                });
                it('home should be enabled', function() {
                    expect($('#recording_page_home_button').closest('div').attr('aria-disabled')).toEqual('false');
                });
                it('world should be enabled', function() {
                    expect($('#recording_page_world_button').closest('div').attr('aria-disabled')).toEqual('false');
                });
                it('menu should be enabled', function() {
                    expect($('#recording_page_menu_button').closest('div').attr('aria-disabled')).toEqual('false');
                });
            });
            describe('buttons events', function() {
                it('record button should call recordingButtonClick func', function() {
                    runs(function() {
                        spyOn(app.ui.recordingPage, 'recordingButtonClick');
                        $('#recording_page_record_button').click();
                    });
                    waits(500);
                    runs(function() {
                        expect(app.ui.recordingPage.recordingButtonClick).toHaveBeenCalled();
                    });
                });
                it('cancel button should call setCancel func', function() {
                    runs(function() {
                        spyOn(app.ui.recordingPage, 'setCancel');
                        $('#recording_page_cancel_button').click();
                    });
                    waits(500);
                    runs(function() {
                        expect(app.ui.recordingPage.setCancel).toHaveBeenCalled();
                    });
                });
                it('call to recordingButtonClick func should set recorder status to "true", twice to "false" and currentVoice should be not empty', function() {
                    runs(function() {
                        app.ui.recordingPage.recordingButtonClick();
                    });
                    waits(1100);
                    runs(function() {
                        expect($('#recording_page_record_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-stoprecord')).toEqual(true);
                        expect(app.recorder.recStatus).toEqual(true);
                        expect(app.recorder.currentVoice.duration).toBeGreaterThan(0);
                        app.ui.recordingPage.recordingButtonClick();
                    });
                    waits(100);
                    runs(function() {
                        expect(app.recorder.recStatus).toEqual(false);
                    });
                });
                it('call to recordingButtonClick func should set recorder status to "true" after that call to setCancel func should set recorder status to "false" and currentVoice should be not empty', function() {
                    runs(function() {
                        app.ui.recordingPage.recordingButtonClick();
                    });
                    waits(100);
                    runs(function() {
                        expect(app.recorder.recStatus).toEqual(true);
                        app.ui.recordingPage.setCancel();
                    });
                    waits(100);
                    runs(function() {
                        expect(app.recorder.recStatus).toEqual(false);
                    });
                });
            });
            describe('recordingButtonClick twice running', function() {
                beforeEach(function() {
                    runs(function() {
                        app.ui.recordingPage.recordingButtonClick();
                        waits(1500);
                    });
                    runs(function() {
                        app.ui.recordingPage.recordingButtonClick();
                    });
                });
                describe('buttons events', function() {
                    it('save button should call setSaved func', function() {
                        runs(function() {
                            spyOn(app.ui.recordingPage, 'setSaved');
                            $('#recording_page_save_button').click();
                        });
                        waits(500);
                        runs(function() {
                            expect(app.ui.recordingPage.setSaved).toHaveBeenCalled();
                        });
                    });
                    it('play button should call playingButtonClick func', function() {
                        runs(function() {
                            spyOn(app.ui.recordingPage, 'playingButtonClick');
                            $('#recording_page_rplay_button').click();
                        });
                        waits(500);
                        runs(function() {
                            expect(app.ui.recordingPage.playingButtonClick).toHaveBeenCalled();
                        });
                    });
                });
                describe('buttons state', function() {
                    it('recording should be enabled', function() {
                        expect($('#recording_page_record_button').closest('div').attr('aria-disabled')).toEqual('false');
                    });
                    it('play should be enabled', function() {
                        expect($('#recording_page_rplay_button').closest('div').attr('aria-disabled')).toEqual('false');
                    });
                    it('save should be enabled', function() {
                        expect($('#recording_page_save_button').closest('div').attr('aria-disabled')).toEqual('false');
                    });
                    it('cancel should be enabled', function() {
                        expect($('#recording_page_cancel_button').closest('div').attr('aria-disabled')).toEqual('false');
                    });
                    it('play button must be play button', function() {
                        expect($('#recording_page_rplay_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-rplay')).toEqual(true);
                    });
                });
                describe('widgets state', function() {
                    it('timer should be seted for "0:01"', function() {
                        expect($('#recording_page_content .timer_div')[0].innerText).toEqual('0:01');
                    });
                    //TODO check it why visible not work
                    //it('tags widget should be visible after recording', function() {
                    //    expect($('#recording_page_content .tags_div').hasClass('invisible')).not.toEqual(true);
                    //});
                    it('recorder.currentVoice should not be empty size', function() {
                        expect(jQuery.isEmptyObject(app.recorder.currentVoice)).toEqual(false);
                    });
                    it('recorder.currentVoice.url should be empty', function() {
                        var res = false;
                        if(app.recorder.currentVoice.url == "" || app.recorder.currentVoice.url == null) {
                            res = true;
                        }
                        expect(res).toEqual(true);
                    });
                    it('recorder.currentVoice.duration should be 1', function() {
                        expect(app.recorder.currentVoice.duration).toEqual(1);
                    });
                    it('recorder.currentVoice.duration size be 0', function() {
                        expect(app.recorder.currentVoice.size).toEqual(0);
                    });
                });
                //TODO it according to new behavior of recorder
                //describe('setSaved func', function() {
                //    beforeEach(function(){
                //        runs(function() {
                //            app.ui.recordingPage.setSaved();
                //            waits(7000);
                //        });
                //    });
                //    it('should disable save button', function() {
                //        expect($('#recording_save_button').closest('div').attr('aria-disabled')).toEqual('true');
                //    });
                //    it('should change recorder.currentVoice.url', function() {
                //        var res = true;
                //        if(app.recorder.currentVoice.url == "" || app.recorder.currentVoice.url == null) {
                //            res = false;
                //        }
                //        expect(res).toEqual(true);
                //    });
                //    it('recorder.currentVoice.duration size greater than 0', function() {
                //        expect(app.recorder.currentVoice.size).toBeGreaterThan(0);
                //    });
                //});
                describe('playingButtonClick func', function() {
                    beforeEach(function() {
                        runs(function() {
                            app.ui.recordingPage.playingButtonClick();
                        });
                    });
                    it('should change play button to stop', function() {
                        expect($('#recording_page_rplay_button').closest('div').children('span').children('span.ui-icon').hasClass('ui-icon-rstopplay')).toEqual(true);
                    });
                    it('player status should be true', function() {
                        expect(app.player.playStatus).toEqual(true);
                    });
                });
                describe('setCancel func', function() {
                    beforeEach(function() {
                        runs(function() {
                            app.ui.recordingPage.setCancel();
                        });
                    });
                    describe('widgets state', function() {
                        it('should clear currentVoice', function() {
                            expect(jQuery.isEmptyObject(app.recorder.currentVoice)).toEqual(true);
                        });
                        it('should clear timer', function() {
                            expect($('#recording_page_content .timer_div')[0].innerText).toEqual('0:00');
                        });
                        it('tags widget should be invisible', function() {
                            expect($('#recording_page_content .tags_div').hasClass('invisible')).toEqual(true);
                        });
                    });
                    describe('buttons state', function() {
                        it('recording should be enabled', function() {
                            expect($('#recording_page_record_button').closest('div').attr('aria-disabled')).toEqual('false');
                        });
                        it('play should be disabled', function() {
                            expect($('#recording_page_rplay_button').closest('div').attr('aria-disabled')).toEqual('true');
                        });
                        it('save should be disabled', function() {
                            expect($('#recording_page_save_button').closest('div').attr('aria-disabled')).toEqual('true');
                        });
                        it('cancel should be enabled', function() {
                            expect($('#recording_page_cancel_button').closest('div').attr('aria-disabled')).toEqual('false');
                        });
                    });
                });
            });
        });
        describe('timer widget', function() {
            it('timer should content 5 methods', function() {
                var len = Object.keys(app.ui.recordingPage.timer).length;
                expect(len).toEqual(5);
            });
            it('bind method should insert 1 div with class "timer_div"', function() {
                var res = false;
                app.ui.recordingPage.content.empty();
                if(app.ui.recordingPage.content.children().length == 0) {
                    app.ui.recordingPage.timer.bind(app.ui.recordingPage.content);
                    if(app.ui.recordingPage.content.children().length == 1) {
                        res = $(app.ui.recordingPage.content.children()[0]).hasClass('timer_div');
                    }
                }
                expect(res).toEqual(true);
            });
            it('tick(78) method should change inner text to "1:18"', function() {
                app.ui.recordingPage.timer.clear();
                app.ui.recordingPage.timer.tick(78);
                expect(app.ui.recordingPage.content.children()[0].innerText).toEqual('1:18');
            });
            it('tick(78) method should change inner text to "1:18"', function() {
                app.ui.recordingPage.timer.clear(78,'down');
                expect(app.ui.recordingPage.content.children()[0].innerText).toEqual('1:18');
                app.ui.recordingPage.timer.tick();
                expect(app.ui.recordingPage.content.children()[0].innerText).toEqual('1:17');
            });
        });
        describe('tags widget', function() {
            it('should content 8 methods', function() {
                var len = Object.keys(app.ui.recordingPage.tags).length;
                expect(len).toEqual(8);
            });
            it('input should exist', function() {
                expect(!jQuery.isEmptyObject($('#add_tag_text'))).toEqual(true);
            });
            it('tags must be clear', function() {
                expect($('#tags_field').children().length).toEqual(0);
                expect(app.ui.recordingPage.tags.getTags().length).toEqual(0);
            });
            it('bind method should insert 1 div with class "tags_div"', function() {
                var res = false;
                app.ui.recordingPage.content.empty();
                if(app.ui.recordingPage.content.children().length == 0) {
                    app.ui.recordingPage.tags.bind(app.ui.recordingPage.content);
                    res = $(app.ui.recordingPage.content.children()[1]).hasClass('tags_div');
                }
                expect(res).toEqual(true);
            });
            it('submit button must call addButtonClick func', function() {
                runs(function() {
                    spyOn(app.ui.recordingPage.tags, 'addButtonClick');
                    var input = app.ui.recordingPage.content.find(':input[type=submit]').first();
                    input.click();
                });
                waits(500);
                runs(function() {
                    expect(app.ui.recordingPage.tags.addButtonClick).toHaveBeenCalled();
                });
            });
            it('submit on form must call addButtonClick func', function() {
                runs(function() {
                    spyOn(app.ui.recordingPage.tags, 'addButtonClick');
                    var form = app.ui.recordingPage.content.find('form').first();
                    form.submit();
                });
                waits(500);
                runs(function() {
                    expect(app.ui.recordingPage.tags.addButtonClick).toHaveBeenCalled();
                });
            });
            it('call to addButtonClick func with tag less then 3 symb should  not insert tag', function() {
                runs(function() {
                    $("#add_tag_text").val('fd');
                    app.ui.recordingPage.tags.addButtonClick();
                });
                waits(100);
                runs(function() {
                    expect($('#tags_field').children().length).toEqual(0);
                    expect(app.ui.recordingPage.tags.getTags().length).toEqual(0);
                });
            });
            it('call to addButtonClick func should insert one tag', function() {
                runs(function() {
                    $("#add_tag_text").val('fdgdfgdfg');
                    app.ui.recordingPage.tags.addButtonClick();
                });
                waits(100);
                runs(function() {
                    expect($('#tags_field').children().length).toEqual(1);
                    expect(app.ui.recordingPage.tags.getTags().length).toEqual(1);
                    expect(app.ui.recordingPage.tags.getTags()[0]).toEqual('fdgdfgdfg');
                });
            });
        });
    });

});
