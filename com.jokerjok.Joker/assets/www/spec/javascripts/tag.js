describe('Tag', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            waits(500);
        });
    });
    it('should content 2 methods', function() {
        var tag = new Tag(1, 'tag');
        var len = Object.keys(tag).length;
        expect(len).toEqual(2);
    });
    it('getTag should return tag name', function() {
        var tag = new Tag(1, 'tag');
        expect(tag.getTag()).toEqual('tag');
    });


});
