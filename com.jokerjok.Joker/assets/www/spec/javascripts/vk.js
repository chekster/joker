describe('VK', function() {
    it('should content 4 methods', function() {
        runs(function() {
            var len = Object.keys(VK).length;
            expect(len).toEqual(4);
        });
    });
    describe('after postToWall', function() {
        beforeEach(function() {
            runs(function() {
                VK.clear();
                VK.postToWall();
                waits(2000);
            });
        });
        it('authBrwsr should not be a null if token expired and backward', function() {
            runs(function() {
                if ( VK.isTokenExpired() ) {
                    expect(VK.getAuthBrwsr()).not.toEqual(null);
                } else {
                    expect(VK.getAuthBrwsr()).toEqual(null);
                };
            });
        });
        afterEach(function() {
            if (VK.getAuthBrwsr() != null) {
                VK.getAuthBrwsr().close();
            };
        });
    });
});
