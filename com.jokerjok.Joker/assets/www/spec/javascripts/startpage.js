describe('startpage', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize();
    });
    it('should content 5 methods', function() {
        waits(3000);
        runs(function() {
            var len = Object.keys(StartPage).length;
            expect(len).toEqual(5);
        });
    });
    it('go_registration_page should exist', function() {
        expect(helper.isEmptyObject($('#go_registration_page'))).toEqual(false);
    });
    it('go_login_page should exist', function() {
        expect(helper.isEmptyObject($('#go_login_page'))).toEqual(false);
    });
    it('login_wo_registration should exist', function() {
        expect(helper.isEmptyObject($('#login_wo_registration'))).toEqual(false);
    });



    it('login button should call goLoginButtonClick func', function() {
        runs(function() {
            spyOn(StartPage, 'goLoginButtonClick');
            $('#go_login_page').click();
        });
        runs(function() {
            expect(StartPage.goLoginButtonClick).toHaveBeenCalled();
        });
    });
    it('registration button should call goRegistrationButtonClick func', function() {
        runs(function() {
            spyOn(StartPage, 'goRegistrationButtonClick');
            $('#go_registration_page').click();
        });
        runs(function() {
            expect(StartPage.goRegistrationButtonClick).toHaveBeenCalled();
        });
    });
    it('login w/o registration button should call loginWoRegistrationButtonClick func', function() {
        runs(function() {
            spyOn(StartPage, 'loginWoRegistrationButtonClick');
            $('#login_wo_registration').click();
        });
        runs(function() {
            expect(StartPage.loginWoRegistrationButtonClick).toHaveBeenCalled();
        });
    });


});
