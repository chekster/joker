describe('user', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize(true);
            waits(3000);
        });
    });
    /*
    it('should content 20 or 19 (depend on token) methods after app initialization', function() {
        waits(3000);
        runs(function() {
            var len = Object.keys(app.user).length;
            if (app.user.token == null) {
                expect(len).toEqual(19);
            } else {
                expect(len).toEqual(20);
            }
        });
    });
    */
    it('emails should content only "chekst@gmail.com" and "israel.gavriel@gmail.com" ', function() {
        waits(3000);
        runs(function() {
            expect(app.user.emails.length).toEqual(2);
            expect(app.user.emails[0]).toEqual('chekst@gmail.com');
            expect(app.user.emails[1]).toEqual('israel.gavriel@gmail.com');
        });
    });

    it('forgotPassword should call succes callback', function() {
        var suc = false;
        var fal = false;

        runs(function() {
            app.user.forgotPassword('chekst@gmail.com',
                                function() {suc = true;},
                                function() {fal = true;});
            waits(9000);
        });
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
        });
    });

    it('forgotPassword should call fault callback', function() {
        var suc = false;
        var fal = false;

        runs(function() {
            app.user.forgotPassword('ddvgdfvdvgdf',
                                function() {suc = true;},
                                function() {fal = true;});
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
        });
    });


    it('initSession should call succes callback and init session from password', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;

        runs(function() {
            app.user.session = false;
            app.user.initSession('chekst@gmail.com',
                                'qwerty',
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).not.toEqual(null);
            expect(app.user.token).not.toEqual(previous_token);
            var len = Object.keys(app.user).length;
            expect(len).toEqual(25);
        });
    });
    it('initSession should call succes callback and init session from token', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;

        runs(function() {
            app.user.session = false;
            app.user.initSession(null,
                                null,
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).not.toEqual(null);
            expect(app.user.token).toEqual(previous_token);
        });
    });

    it('changePassword should change password', function() {
        var suc = false;
        var fal = false;
        var token = app.user.token;

        waits(3000);
        runs(function() {
           app.user.changePassword(
                            'qwerty',
                            'qwerty',
                            function() {suc = true;},
                            function() {fal = true;}
            );

        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
        });
    });

    it('initSession should call succes callback, init session from token and logout', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;
        log('TOKEN ' + previous_token);
        waits(3000);
        runs(function() {
            app.user.session = false;
            app.user.initSession(null,
                                null,
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).not.toEqual(null);
            expect(app.user.token).toEqual(previous_token);

            suc = false;
            fal = false;
            app.user.session = false;
            previous_token = app.user.token;

            app.user.logout(
                            function() {suc = true;},
                            function() {fal = true;});
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).toEqual(null);
            expect(app.user.token).not.toEqual(previous_token);
        });
    });
    it('initSession should call fail callback', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;

        waits(3000);
        runs(function() {
            app.user.session = false;
            app.user.initSession('chekst@gmail.com',
                                '11111',
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
            expect(app.user.token).toEqual(null);
            expect(app.user.token).toEqual(previous_token);
        });
    });
    it('logout should call fail callback', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;
        waits(3000);
        runs(function() {
            app.user.logout(
                            function() {suc = true;},
                            function() {fal = true;});
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
            expect(app.user.token).toEqual(null);
            expect(app.user.token).toEqual(previous_token);
        });
    });
    it('initSession should call succes callback and after that fail callback and erase session', function() {
        var suc = false;
        var fal = false;
        var previous_token = app.user.token;

        waits(3000);
        runs(function() {
            app.user.session = false;
            app.user.initSession('chekst@gmail.com',
                                'qwerty',
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).not.toEqual(null);
            expect(app.user.token).not.toEqual(previous_token);


            previous_token = app.user.token;
            suc = false;
            fal = false;
            app.user.session = false;
            app.user.initSession('chekst@gmail.com',
                                '11111',
                                function() {suc = true;},
                                function() {fal = true;},
                                false);
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
            expect(app.user.token).toEqual(null);
            expect(app.user.token).not.toEqual(previous_token);
        });
    });
    it('user creating should fail, because of incorrect password', function() {
        var suc = false;
        var fal = false;

        var user = new User();
        user.password = 'sa';
        user.guser = 'xczxc@svxvx.com';
        user.language = 'ru';
        user.name = 'sadcc';

        user.create(
                function() {suc = true;},
                function() {fal = true;}
        );
        waits(3000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
        });
    });

    it('should create user', function() {
        var suc = false;
        var fal = false;

        var user = new User();
        user.password = helper.getRandomString(8);
        user.guser = helper.getRandomString(8) + '@gmail.com';
        user.language = 'ru';
        var name = helper.getRandomString(5);
        user.name = name;

        user.create(
                function() {suc = true;},
                function() {fal = true;}
        );
        waits(3000);
        runs(function() {
            expect(app.user.name).toEqual(name);
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
        });
    });

    it('initSessionWORegistration should call succes callback, init session from email', function() {
        var suc = false;
        var fal = false;
        waits(3000);
        runs(function() {
            app.user.session = false;
            var mail = helper.getRandomString(5) + '@gmail.com';
            app.user.initSessionWORegistration(mail,
                                                function() {suc = true;},
                                                function() {fal = true;});
        });
        waits(3000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
            expect(app.user.token).not.toEqual(null);
        });
    });


    describe('logged in', function() {
        beforeEach(function() {
            runs(function() {
                if (!app.user.session) {
                    app.user.initSession('chekst@gmail.com','qwerty');
                    waits(3000);
                };
            });
        });
        it('should save user', function() {
            var suc = false;
            var fal = false;
            var language = app.user.language;
            var name = helper.getRandomString(5);

            app.user.name = name;
            if (app.user.language != 'en') {
                app.user.language = 'en';
            } else {
                app.user.language = 'ru';
            };

            app.user.save(
                    function() {suc = true;},
                    function() {fal = true;}
            );
            waits(3000);
            runs(function() {
                expect(app.user.name).toEqual(name);
                expect(app.user.language).not.toEqual(language);
                expect(suc).toEqual(true);
                expect(fal).toEqual(false);
            });
        });
    });

});
