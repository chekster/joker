describe('Ajax loader', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
        });
    });
    it('should content 5 methods', function() {
        runs(function() {
            var len = Object.keys(AjaxLoader).length;
            expect(len).toEqual(5);
        });
    });
    describe('after app initialization', function() {
        beforeEach(function() {
            runs(function() {
                app.initialize();
            });
        });
        it('loader should not be a null', function() {
            waits(3000);
            runs(function() {
                expect(AjaxLoader.getLoader()).not.toEqual(null);
            });
        });
        it('loader show should set css dispaly to block', function() {
            waits(3000);
            runs(function() {
                AjaxLoader.show();
                expect(AjaxLoader.getLoader().css('display')).toEqual('block');
                expect(AjaxLoader.getState()).toEqual('show');
            });
        });
        it('loader hide should set css dispaly to none', function() {
            waits(3000);
            runs(function() {
                AjaxLoader.hide();
                expect(AjaxLoader.getLoader().css('display')).toEqual('none');
                expect(AjaxLoader.getState()).toEqual('hide');
            });
        });
    });
});
