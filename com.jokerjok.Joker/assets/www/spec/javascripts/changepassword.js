describe('ChangePassword', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            waits(500);
        });
    });
    it('should content 9 methods', function() {
        runs(function() {
            var len = Object.keys(ChangePassword).length;
            expect(len).toEqual(9);
        });
    });
    describe('after showing', function() {
        beforeEach(function() {
            runs(function() {
                app.initialize(true);
                waits(3000);
            });
            runs(function() {
                if (!app.user.session) {
                    app.user.initSession('chekst@gmail.com','qwerty');
                    waits(3000);
                };
            });
            runs(function() {
                ChangePassword.clear();
                ChangePassword.show();
                waits(500);
            });
        });
        it('password panel should not be a null', function() {
            runs(function() {
                expect(ChangePassword.getPasswordPanel()).not.toEqual(null);
            });
        });
        it('show should set css dispaly to block', function() {
            runs(function() {
                expect(ChangePassword.getPasswordPanel().css('display')).toEqual('block');
                expect(ChangePassword.getState()).toEqual('show');
            });
        });
        it('hide should set css dispaly to none', function() {
            runs(function() {
                ChangePassword.hide();
            });
            waits(500);
            runs(function() {
                expect(ChangePassword.getContainer().css('display')).toEqual('none');
                expect(ChangePassword.getState()).toEqual('hide');
            });
        });
        it('change password button should call changePassword func', function() {
            runs(function() {
                spyOn(ChangePassword, 'changePassword');
                $('#change_password_ok').click();
            });
            runs(function() {
                expect(ChangePassword.changePassword).toHaveBeenCalled();
            });
        });
        it('changePassword func should return false on incorrect confirmation', function() {
            $('#change_password_password').val('qwewqe');
            $('#change_password_old_password').val('ffghgf');
            $('#change_password_confirm_password').val('dgdfgdf');
            var res = ChangePassword.changePassword();
            expect(res).toEqual(false);
        });
    });
});
