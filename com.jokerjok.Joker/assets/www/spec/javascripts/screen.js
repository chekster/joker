describe('Screen', function() {
    beforeEach(function() {
    });
    it('should content 3 methods', function() {
        runs(function() {
            var len = Object.keys(Screen).length;
            expect(len).toEqual(3);
        });
    });
    it('wakeLock should lock screen', function() {
        runs(function() {
            Screen.wakeLock();
            expect(Screen.isLocked()).toEqual(true);
        });
    });
    it('releaseLock should unlock screen after 30 sec', function() {
        runs(function() {
            Screen.releaseLock();
            expect(Screen.isLocked()).toEqual(true);
            waits(31000)
        });
        runs(function() {
            expect(Screen.isLocked()).toEqual(false);
        });
    });
});
