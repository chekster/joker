describe('EditTags', function() {
    var tags = EditTags();
    var callbackChecking = false;
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            //app.initialize(true);
        });
        waits(500);
    });
    it('should content 8 methods', function() {
        var len = Object.keys(tags).length;
        expect(len).toEqual(8);
    });

    describe('bind', function() {
        beforeEach(function() {
            runs(function() {
                tags.bind($('#recording_page_content'),
                                null,
                                function() {
                                    callbackChecking = true;
                                }
                );
                waits(500);
            });
        });
        afterEach(function() {
            $('#recording_page_content').empty();
        });
        it('should place html in holder', function() {
            expect($("#add_tag_form").length).toEqual(1);
            expect($("#add_tag_text").length).toEqual(1);
            expect($("#add_tag_button").length).toEqual(1);
            expect($("#tags_field").length).toEqual(1);
            expect($("#tags_div").length).toEqual(1);
        });
        it('should place buttons events', function() {
            runs(function() {
                spyOn(tags, 'addButtonClick');
                $('#add_tag_button').click();
            });
            runs(function() {
                expect(tags.addButtonClick).toHaveBeenCalled();
            });
        });
        it('show hide testing', function() {
            tags.hide();
            expect($("#tags_div").hasClass('invisible')).toEqual(true);
            tags.show();
            expect($("#tags_div").hasClass('invisible')).toEqual(false);
            tags.hide();
            expect($("#tags_div").hasClass('invisible')).toEqual(true);
        });
        it('enable tag more than 3 symbols and run callback', function() {
            $("#add_tag_text").val('1');
            tags.addButtonClick();
            expect($("#add_tag_text").val()).toEqual('1');
            expect(tags.getTags().length).toEqual(0);
            $("#add_tag_text").val('111');
            callbackChecking = false;
            tags.addButtonClick();
            expect($("#add_tag_text").val()).toEqual('');
            expect(tags.getTags().length).toEqual(1);
            expect(callbackChecking).toEqual(true);
        });

        describe('tips loading', function() {
            beforeEach(function() {
                runs(function() {
                    app.initialize(true);
                });
                waits(1000);
                runs(function() {
                    app.user.initSession('chekst@gmail.com','qwerty',null,null);
                });
                waits(3000);
            });

            it('on tag "rus" must find 3 variants', function() {
                runs(function() {
                    expect(tags.getTags().length).toEqual(1);
                    expect($("#add_tag_tips").hasClass('invisible')).toEqual(true);
                    $("#add_tag_text").val('rus');
                    tags.addTextChange();
                });
                waits(2000);
                runs(function() {
                    expect($("#add_tag_tips").hasClass('invisible')).toEqual(false);
                    expect($("#add_tag_tips>div").length).toEqual(3);
                    $("#add_tag_tips>div").first().click();
                    expect(tags.getTags().length).toEqual(2);
                });
            });
        });
    });


    describe('sorting widget', function() {
        beforeEach(function() {
            runs(function() {
                Sorting.show();
                waits(500);
            });
        });
        afterEach(function() {
            Sorting.clear();
        });

        it('should place html in holder', function() {
            expect($("#sort_tag_form").length).toEqual(1);
            expect($("#sort_tag_text").length).toEqual(1);
            expect($("#sort_tag_button").length).toEqual(1);
            expect($("#sort_tags_field").length).toEqual(1);
            expect($("#sort_tags_div").length).toEqual(1);
            expect($("#sort_tag_tips").length).toEqual(1);
        });
        it('should place buttons events', function() {
            runs(function() {
                expect($("#sort_tag_button").length).toEqual(1);
                spyOn(Sorting.getTagsWidget(), 'addButtonClick');
                $('#sort_tag_button').click();
            });
            runs(function() {
                expect(Sorting.getTagsWidget().addButtonClick).toHaveBeenCalled();
            });
        });
        it('show hide testing', function() {
            Sorting.getTagsWidget().hide();
            expect($("#sort_tags_div").hasClass('invisible')).toEqual(true);
            Sorting.getTagsWidget().show();
            expect($("#sort_tags_div").hasClass('invisible')).toEqual(false);
            Sorting.getTagsWidget().hide();
            expect($("#sort_tags_div").hasClass('invisible')).toEqual(true);
        });
        it('enable tag more than 3 symbols', function() {
            $("#sort_tag_text").val('1');
            Sorting.getTagsWidget().addButtonClick();
            expect($("#sort_tag_text").val()).toEqual('1');
            expect(Sorting.getTagsWidget().getTags().length).toEqual(0);
            $("#sort_tag_text").val('111');
            Sorting.getTagsWidget().addButtonClick();
            expect($("#sort_tag_text").val()).toEqual('');
            expect(Sorting.getTagsWidget().getTags().length).toEqual(1);
        });

        describe('tips loading', function() {
            beforeEach(function() {
                runs(function() {
                    app.initialize(true);
                });
                waits(1000);
                runs(function() {
                    app.user.initSession('chekst@gmail.com','qwerty',null,null);
                });
                waits(3000);
            });

            it('on tag "rus" must find 3 variants', function() {
                runs(function() {
                    expect(Sorting.getTagsWidget().getTags().length).toEqual(0);
                    expect($("#sort_tag_tips").hasClass('invisible')).toEqual(true);
                    $("#sort_tag_text").val('rus');
                    Sorting.getTagsWidget().addTextChange();
                });
                waits(2000);
                runs(function() {
                    expect($("#sort_tag_tips").hasClass('invisible')).toEqual(false);
                    expect($("#sort_tag_tips>div").length).toEqual(3);
                    $("#sort_tag_tips>div").first().click();
                    expect(Sorting.getTagsWidget().getTags().length).toEqual(1);
                });
            });
        });

    });





});
