describe('Player', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize();
            waits(1000);//Load ajax response for voices
        });
    });
    it('should exist', function() {
        expect(!jQuery.isEmptyObject(app.player)).toEqual(true);
    });
    it('should content 5 methods', function() {
        var len = Object.keys(app.player).length;
        expect(len).toEqual(5);
    });

    it('player.play should on load voice must call timer.tick, onLoad, onFinish', function() {
        var timer = Timer();
        var onFinish = false;
        var onLoad = false;
        var onVoiceLoad = false;
        var v = new Voice('https://api.soundcloud.com/tracks/89575917/stream','chekst@gmail.com', 'ru');
        runs(function() {
            spyOn(timer, 'tick');
            //2 seconds voice
            //var v = new Voice('https://api.soundcloud.com/tracks/82010830/stream?consumer_key=251d9dee7a5cfecd850d1df56b9f4477','chekst@gmail.com', 'ru');
            var voice = {};

            voice.system='soundcloud';
            voice.user_name='chek';
            voice.duration=7;
            voice.size=0.0064;
            voice.likes=0;
            voice.dislikes=0;
            voice.likes_sum=0;
            voice.id=6;
            voice.tags_access=[{'tag':'joke'}];

            v.cast(voice);
            v.load(function() {onFinish = true;}, function() {onVoiceLoad = true;});

            waits(5000);//Load voices for playing and finish
        });
        waits(5000);//Load voices for playing and finish
        runs(function() {
            waits(5000);//Load voices for playing and finish
            expect(onVoiceLoad).toEqual(true);

            app.player.play(v, timer, function() {onLoad = true;});
            dur = 9000;
            waits(dur);

        });
        //waits(5000);//Load voices for playing and finish
        runs(function() {
            expect(onLoad).toEqual(true);
            expect(onFinish).toEqual(true);
            expect(app.player.IntervalId).toBeGreaterThan(0);
            expect(timer.tick).toHaveBeenCalled();
            app.player.stop();
        });
    });
});
