describe('Recorder', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize();
            waits(3000);//Load ajax response for voices
        });
        runs(function() {
            app.user.initSession('chekst@gmail.com','qwerty',null,null);
        });
    });
    it('should exist', function() {
        expect(!jQuery.isEmptyObject(app.recorder)).toEqual(true);
    });
    it('should content 18 methods', function() {
        var len = Object.keys(app.recorder).length;
        expect(len).toEqual(18);
    });
    it('recorder.start should on load voice must call timer.tick, stop and save', function() {
        var timer = Timer();
        var voice = null;
        runs(function() {
            spyOn(timer, 'tick');
            app.recorder.start(timer);
            waits(3000);
        });
        runs(function() {
            expect(app.recorder.recordingIntervalId).toBeGreaterThan(0);
            expect(timer.tick).toHaveBeenCalled();
            expect(app.recorder.recStatus).toEqual(true);
            app.recorder.stop();
            waits(50);
        });
        runs(function() {
            expect(app.recorder.recStatus).toEqual(false);
            expect(app.user.tokenSCInited).toEqual(false);
            voice = app.recorder.currentVoice;
            expect(voice.fileLocation).not.toEqual(null);
            expect(voice.fileLocation).not.toEqual('');
            app.recorder.save();
            waits(10000); //wait for full saving cycle
        });
        runs(function() {
            expect(app.recorder.recordingIntervalId).toBeGreaterThan(0);
            expect(app.recorder.currentVoice.fileLocation).not.toEqual(null);
            expect(app.recorder.currentVoice.fileLocation).not.toEqual('');

            expect(app.user.tokenSCInited).toEqual(true);

            expect(voice.url).not.toEqual(null);
            expect(voice.url).not.toEqual('');
            expect(voice.link).not.toEqual(null);
            expect(voice.link).not.toEqual('');
        });
    });
    it('recorder should be enabled for recording imidetaly after recording', function() {
        var voice = null;
        runs(function() {
            app.recorder.start(null);
            waits(3000);
        });
        runs(function() {
            expect(app.recorder.recordingIntervalId).toBeGreaterThan(0);
            expect(app.recorder.recStatus).toEqual(true);
            app.recorder.stop();
            waits(50);
        });
        runs(function() {
            expect(app.recorder.recStatus).toEqual(false);
            expect(app.user.tokenSCInited).toEqual(false);
            voice = app.recorder.currentVoice;
            expect(voice.fileLocation).not.toEqual(null);
            expect(voice.fileLocation).not.toEqual('');
            app.recorder.save();

            waits(3000); //wait for full saving cycle *
        });
        runs(function() {
            app.recorder.start();
            waits(3000); //wait for record second voice
        });
        runs(function() {
            expect(app.user.tokenSCInited).toEqual(true);
            expect(app.recorder.recStatus).toEqual(true);
            expect(app.recorder.recordingIntervalId).toBeGreaterThan(0);
            expect(app.recorder.currentVoice.fileLocation).not.toEqual(null);
            expect(app.recorder.currentVoice.fileLocation).not.toEqual('');

            app.recorder.stop();
            waits(6000); //wait for full saving cycle *
        });
        runs(function() {
            expect(app.recorder.recStatus).toEqual(false);
            expect(voice.url).not.toEqual(null);
            expect(voice.url).not.toEqual('');
            expect(voice.link).not.toEqual(null);
            expect(voice.link).not.toEqual('');

            app.recorder.save();
            waits(10000); //wait for full saving cycle
        });
        runs(function() {
            expect(app.recorder.currentVoice.url).not.toEqual(null);
            expect(app.recorder.currentVoice.url).not.toEqual('');
            expect(app.recorder.currentVoice.link).not.toEqual(null);
            expect(app.recorder.currentVoice.link).not.toEqual('');
        });
    });
});
