describe('registrationpage', function() {
    beforeEach(function() {
        loadFixtures("index.html");
        app.initialize(true);
    });
    it('should content 4 methods', function() {
        waits(3000);
        runs(function() {
            var len = Object.keys(RegistrationPage).length;
            expect(len).toEqual(4);
        });
    });
    it('register button should call submit func', function() {
        $('#registration_ok').click();
        expect(RegistrationPage.getRunnedFunc()).toEqual('submit');
    });
    it('registration_email_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_email_lbl'))).toEqual(true);
    });
    it('registration_email should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_email'))).toEqual(true);
    });
    it('registration_password_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_password_lbl'))).toEqual(true);
    });
    it('registration_password should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_password'))).toEqual(true);
    });
    it('registration_name_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_name_lbl'))).toEqual(true);
    });
    it('registration_confirm_password_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_confirm_password_lbl'))).toEqual(true);
    });
    it('registration_confirm_password should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_confirm_password'))).toEqual(true);
    });



    it('registration_name should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_name'))).toEqual(true);
    });
    it('registration_language_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_language_lbl'))).toEqual(true);
    });
    it('registration_language_lbl should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_language_lbl'))).toEqual(true);
    });
    it('registration_language should exist', function() {
        expect(!jQuery.isEmptyObject($('registration_language'))).toEqual(true);
    });
    it('on submit should show ajax loader, run callbacks, hide ajax loader', function() {
        var suc = false;
        var fal = false;
        runs(function() {
            var email = helper.getRandomString(8);
            var option = $('<option value="'+email+'@gmail.com" selected="selected">'+email+'@gmail.com</option>');
            $("#registration_email").append( option );
            $('#registration_password').val('qwerty');
            $('#registration_confirm_password').val('qwerty');
            $('#registration_name').val('dsfd');
            RegistrationPage.submit(function () {suc = true;}, function () {fal = true;});
            expect(AjaxLoader.getState()).toEqual('show');
        });
        waits(6000);
        runs(function() {
            expect(suc).toEqual(true);
            expect(fal).toEqual(false);
        });
    });
    it('on submit should show ajax loader, and fail because of incorrect password confirmation', function() {

        var suc = false;
        var fal = false;
        runs(function() {
            var email = helper.getRandomString(8);
            var option = $('<option value="'+email+'@gmail.com" selected="selected">'+email+'@gmail.com</option>');
            $("#registration_email").append( option );
            $('#registration_password').val('qwerty');
            $('#registration_confirm_password').val('qwertyfdhg');
            $('#registration_name').val('dsfd');
            RegistrationPage.submit(function () {suc = true;}, function () {fal = true;});
            expect(AjaxLoader.getState()).toEqual('show');
        });
        waits(6000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
        });
    });
    it('on submit should show ajax loader, run callbacks, hide ajax loader, creating should fail', function() {
        var suc = false;
        var fal = false;
        runs(function() {
            $('#registration_password').val('qsfhhfg');
            RegistrationPage.submit(function () {suc = true;}, function () {fal = true;});
        });
        waits(6000);
        runs(function() {
            expect(suc).toEqual(false);
            expect(fal).toEqual(true);
        });
    });
    it('clearPassword should clean password inputs', function() {
        runs(function() {
            $('#registration_password').val('qwerty');
            $('#registration_confirm_password').val('qwerty');
        });
        runs(function() {
            expect($('#registration_password').val()).toEqual('qwerty');
            expect($('#registration_confirm_password').val()).toEqual('qwerty');
            RegistrationPage.clearPassword();
        });
        runs(function() {
            expect($('#registration_password').val()).toEqual('');
            expect($('#registration_confirm_password').val()).toEqual('');
        });
    });


});
