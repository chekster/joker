describe('voice', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize();
        });
        waits(500);
        runs(function() {
            app.user.initSession('chekst@gmail.com','qwerty',null,null);
        });
        waits(3000);
        runs(function() {
            Playlist.loadList(function() {checking = true;});
            MyVoicesList.loadList();
        });
        waits(3000);
    });
    it('should content 49 methods', function() {
        runs(function() {
            voice = Playlist.getVoice();
            var len = Object.keys(voice).length;
            expect(len).toEqual(49);
        });
    });
    it('addSpamReport and removeSpamReport should run callback', function() {
        var checking = false
        var user_spam = null;
        runs(function() {
            voice = Playlist.getVoice();
            user_spam = voice.user_spam;
            if (user_spam == null) {
                voice.addSpamReport(function() {checking = true;});
            } else {
                voice.removeSpamReport(function() {checking = true;});
            };

        });
        waits(3000)
        runs(function() {
            expect(checking).toEqual(true);
        });
        runs(function() {
            if (user_spam == null) {
                voice.removeSpamReport(function() {checking = false;});
            } else {
                voice.addSpamReport(function() {checking = false;});
            };
        });
        waits(3000)
        runs(function() {
            expect(checking).toEqual(false);
        });
    });
    it('addLike and removeLike should run callback', function() {
        var checking = false
        var user_like = null;
        runs(function() {
            voice = Playlist.getVoice();
            user_like = voice.user_like;
            if (user_like == null) {
                voice.addLike(true, function() {checking = true;});
            } else {
                voice.removeLike(function() {checking = true;});
            };
        });
        waits(3000);
        runs(function() {
            expect(checking).toEqual(true);
        });
        runs(function() {
            if (user_like == null) {
                voice.removeLike(function() {checking = false;});
            } else {
                voice.addLike(true, function() {checking = false;});
            };
        });
        waits(3000);
        runs(function() {
            expect(checking).toEqual(false);
        });
    });
    it('addLike should change previous like', function() {
        runs(function() {
            voice = Playlist.getVoice();
            if (voice.user_like == null) {
                voice.addLike(true);
            };
        });
        waits(3000);
        var like = null;
        runs(function() {
            if (voice.user_like.like) {
                like = false;
                voice.addLike(false);
            } else {
                like = true;
                voice.addLike(true);
            };
        });
        waits(3000)
        runs(function() {
            if (like) {
                expect(voice.user_like.like).toEqual(true);
            } else {
                expect(voice.user_like.like).toEqual(false);
            };
        });
    });
    it('remove should run callback', function() {
        var checking = false
        var len = MyVoicesList.voicesCount();
        runs(function() {
            var voice = MyVoicesList.getVoice();
            if (len > 0) {
                voice.deleteVoice(function() {checking = true;});
            };
        });
        waits(3000)
        runs(function() {
            if (len > 0) {
                expect(checking).toEqual(true);
            };
            //expect(MyVoicesList.voicesCount()).toEqual(len - 1);
            //len = MyVoicesList.voicesCount();
            MyVoicesList.loadList();
        });
        waits(3000)
        runs(function() {
            if (len > 0) {
                expect(MyVoicesList.voicesCount()).toEqual(len - 1);
            };
        });
    });
    it('load should run callback', function() {
        var finish = false;
        var load = false;
        var duration = 0;
        var dur = 0;
        var voice = null;
        runs(function() {
            //voice = Playlist.getVoice();

            voice = MyVoicesList.getVoice();
            if (voice != null) {
                duration = voice.duration;
                voice.load(function() {finish = true;}, function() {load = true;});
                expect(voice.loading).toEqual(true);
                expect(voice.loaded).toEqual(false);
                waits(10000)
            };
        });
        runs(function() {
            if (voice != null) {
                expect(voice.SM).not.toEqual(null);
                expect(finish).toEqual(false);
                expect(load).toEqual(true);
                expect(voice.loading).toEqual(false);
                expect(voice.loaded).toEqual(true);
                app.player.play(voice, null, null);

                dur = (duration * 1000) + 2000;
                waits(dur);
            };
        });
        runs(function() {
            if (voice != null) {
                expect(finish).toEqual(true);
                voice.unload();
                waits(1000);
            };
        });
        runs(function() {
            if (voice != null) {
                expect(voice.SM).toEqual(null);
            };
        });
    });

});
