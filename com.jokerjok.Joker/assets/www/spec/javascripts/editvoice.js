describe('EditVoice', function() {
    //var v = null;
    var v = new Voice('https://api.soundcloud.com/tracks/89575917/stream','chekst@gmail.com', 'ru');
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            app.initialize();
            log('init',true);
        });
        /*
        waits(2000);
        runs(function() {
            app.user.initSession('chekst@gmail.com','qwerty',null,null);
            log('initSess',true);
        });
        waits(5000);
        runs(function() {
            Playlist.loadList();
            log('loadList',true);
        });
        waits(5000)
        runs(function() {
            log('getVoice',true);
            v = Playlist.getVoice();
            log(JSON.stringify(v),true);
        });
        */
    });
    it('should content 8 methods', function() {
        runs(function() {
            var len = Object.keys(EditVoice).length;
            expect(len).toEqual(8);
        });
    });
    describe('after showing', function() {
        beforeEach(function() {
            runs(function() {
                EditVoice.clear();
                EditVoice.show(v);
                waits(1000);
            });
        });
        it('EditVoice should not be a null', function() {
            runs(function() {
                expect(EditVoice.getEditVoice()).not.toEqual(null);
            });
        });
        it('_edit_voice_background should not be a null', function() {
            runs(function() {
                expect(EditVoice.getBackground()).not.toEqual(null);
            });
        });
        it('show should set css dispaly to block', function() {
            runs(function() {
                EditVoice.show(v);
            });
            waits(1000);
            runs(function() {
                expect(EditVoice.getEditVoice().css('display')).toEqual('block');
                expect(EditVoice.getState()).toEqual('show');
            });
        });
        it('hide should set css dispaly to none', function() {
            runs(function() {
                EditVoice.hide();
            });
            waits(1000);
            runs(function() {
                expect(EditVoice.getContainer().css('display')).toEqual('none');
                expect(EditVoice.getState()).toEqual('hide');
            });
        });
    });
});
