describe('my voices list', function() {
    it('should exist', function() {
        expect(!jQuery.isEmptyObject(MyVoicesList)).toEqual(true);
    });
    it('should content 14 methods', function() {
        var len = Object.keys(MyVoicesList).length;
        expect(len).toEqual(14);
    });
    describe('functionality', function() {
        beforeEach(function() {
            runs(function() {
                loadFixtures("index.html");
                app.initialize();
                waits(2000);
            });
            runs(function() {
                app.user.initSession('chekst@gmail.com','qwerty',null,null);
                waits(2000);
            });
        });
        it('load should run callback', function() {
            var checking = false
            runs(function() {
                MyVoicesList.loadList(function() {checking = true;});
            });
            waits(3000)
            runs(function() {
                expect(checking).toEqual(true);
            });
        });
        it('voicesCount, getVoice, getNextVoice, anyPreviousVoice, anyNextVoice, getFirstVoice, getVoiceByID', function() {
            //expect(Playlist.voicesCount()).toBeGreaterThan(0);
            expect(MyVoicesList.anyPreviousVoice()).toEqual(false);
            if (MyVoicesList.voicesCount() > 0) {
                expect(MyVoicesList.anyNextVoice()).toEqual(true);
            } else {
                expect(MyVoicesList.anyNextVoice()).toEqual(false);
            }
            var v = new Voice('','','');
            var voice = MyVoicesList.getNextVoice();
            if (MyVoicesList.voicesCount() > 0) {
                expect(voice.constructor).toEqual(v.constructor);
                expect(MyVoicesList.anyPreviousVoice()).toEqual(true);
                // > 2 because alredy moved one time for next voice
                if (MyVoicesList.voicesCount() > 2) {
                    expect(MyVoicesList.anyNextVoice()).toEqual(true);
                } else {
                    expect(MyVoicesList.anyNextVoice()).toEqual(false);
                }
            } else {
                expect(voice).toEqual(null);
                expect(MyVoicesList.anyPreviousVoice()).toEqual(false);
                expect(MyVoicesList.anyNextVoice()).toEqual(false);
            }
            for (var i = 0; i < (MyVoicesList.voicesCount() - 2); i++) {
                voice = MyVoicesList.getNextVoice();
            }
            expect(MyVoicesList.anyNextVoice()).toEqual(false);
            if (MyVoicesList.voicesCount() > 0) {
                expect(MyVoicesList.anyPreviousVoice()).toEqual(true);
            } else {
                expect(MyVoicesList.anyPreviousVoice()).toEqual(false);
            }
            voice = MyVoicesList.getFirstVoice();
            expect(MyVoicesList.anyPreviousVoice()).toEqual(false);
            if (voice != null) {
                expect(MyVoicesList.getVoiceByID(voice.id).id).toEqual(voice.id);
            };
        });
        it('removeVoice should remove voice from list', function() {
            var voice = MyVoicesList.getFirstVoice();
            var len = MyVoicesList.voicesCount();
            if (len > 0) {
                MyVoicesList.removeVoice(voice);
                expect(MyVoicesList.voicesCount()).toEqual(len - 1);
            };
        });
    });
});


