describe('Settings', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
            waits(500);
        });
    });
    it('should content 11 methods', function() {
        runs(function() {
            var len = Object.keys(Settings).length;
            expect(len).toEqual(11);
        });
    });

    describe('behavior for registered user', function() {
        beforeEach(function() {
            runs(function() {
                app.initialize(true);
                waits(3000);
            });
            runs(function() {
                Settings.clear();
                app.user.registered = true;
                waits(500);
            });
            runs(function() {
                Settings.show();
                waits(500);
            });
        });
        it('settings should not be a null', function() {
            runs(function() {
                expect(Settings.getSettings()).not.toEqual(null);
            });
        });
        it('show should set css dispaly to block', function() {
            runs(function() {
                expect(Settings.getSettings().css('display')).toEqual('block');
                expect(Settings.getState()).toEqual('show');
            });
        });
        it('hide should set css dispaly to none', function() {
            runs(function() {
                Settings.hide();
            });
            waits(500);
            runs(function() {
                expect(Settings.getContainer().css('display')).toEqual('none');
                expect(Settings.getState()).toEqual('hide');
            });
        });
        it('change password button should call changePassword func', function() {
            runs(function() {
                spyOn(Settings, 'changePassword');
                $('#settings_change_password').click();
            });
            runs(function() {
                expect(Settings.changePassword).toHaveBeenCalled();
            });
        });
        it('change language should call changeLanguage func', function() {
            runs(function() {
                spyOn(Settings, 'changeLanguage');
                $('#settings_language').change();
            });
            runs(function() {
                expect(Settings.changeLanguage).toHaveBeenCalled();
            });
        });
        it('change name should call changeName func', function() {
            runs(function() {
                spyOn(Settings, 'changeName');
                $('#settings_name').change();
            });
            runs(function() {
                expect(Settings.changeName).toHaveBeenCalled();
            });
        });
    });

    describe('behavior for unregistered user', function() {
        beforeEach(function() {
            runs(function() {
                app.initialize(true);
                waits(3000);
            });
            runs(function() {
                Settings.clear();
                app.user.registered = false;
                waits(500);
            });
            runs(function() {
                Settings.show();
                waits(500);
            });
        });
        it('settings should not be a null', function() {
            runs(function() {
                expect(Settings.getSettings()).not.toEqual(null);
            });
        });
        it('show should set css dispaly to block', function() {
            runs(function() {
                expect(Settings.getSettings().css('display')).toEqual('block');
                expect(Settings.getState()).toEqual('show');
            });
        });
        it('hide should set css dispaly to none', function() {
            runs(function() {
                Settings.hide();
            });
            waits(500);
            runs(function() {
                expect(Settings.getContainer().css('display')).toEqual('none');
                expect(Settings.getState()).toEqual('hide');
            });
        });
        it('change language should call changeLanguage func', function() {
            runs(function() {
                spyOn(Settings, 'changeLanguage');
                $('#settings_language').change();
            });
            runs(function() {
                expect(Settings.changeLanguage).toHaveBeenCalled();
            });
        });
        it('settings_name should not exist', function() {
            expect(helper.isEmptyObject($('#settings_name'))).toEqual(true);
        });
        it('settings_change_password should not exist', function() {
            expect(helper.isEmptyObject($('#settings_change_password'))).toEqual(true);
        });
    });
});
