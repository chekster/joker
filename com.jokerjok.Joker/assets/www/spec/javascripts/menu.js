describe('Menu', function() {
    beforeEach(function() {
        runs(function() {
            loadFixtures("index.html");
        });
    });
    it('should content 15 methods', function() {
        runs(function() {
            var len = Object.keys(Menu).length;
            expect(len).toEqual(15);
        });
    });
    describe('after showing', function() {
        beforeEach(function() {
            runs(function() {
                Menu.clear();
                Menu.show();
                waits(1000);
            });
        });
        it('menu should not be a null', function() {
            runs(function() {
                expect(Menu.getMenu()).not.toEqual(null);
            });
        });
        it('menu_background should not be a null', function() {
            runs(function() {
                expect(Menu.getBackground()).not.toEqual(null);
            });
        });
        it('menu show should set css dispaly to block', function() {
            runs(function() {
                Menu.show();
            });
            waits(1000);
            runs(function() {
                expect(Menu.getMenu().css('display')).toEqual('block');
                expect(Menu.getState()).toEqual('show');
            });
        });
        it('menu hide should set css dispaly to none', function() {
            runs(function() {
                Menu.hide();
            });
            waits(1000);
            runs(function() {
                expect(Menu.getContainer().css('display')).toEqual('none');
                expect(Menu.getState()).toEqual('hide');
            });
        });
        it('home button should call home func', function() {
            runs(function() {
                spyOn(Menu, 'home');
                $('#menu_home').click();
            });
            runs(function() {
                expect(Menu.home).toHaveBeenCalled();
            });
        });
        it('recording button should call recording func', function() {
            runs(function() {
                spyOn(Menu, 'recording');
                $('#menu_recording').click();
            });
            runs(function() {
                expect(Menu.recording).toHaveBeenCalled();
            });
        });
        it('my button should call myvoices func', function() {
            runs(function() {
                spyOn(Menu, 'myvoices');
                $('#menu_myvoices').click();
            });
            runs(function() {
                expect(Menu.myvoices).toHaveBeenCalled();
            });
        });
        it('settings button should call settings func', function() {
            runs(function() {
                spyOn(Menu, 'settings');
                $('#menu_settings').click();
            });
            runs(function() {
                expect(Menu.settings).toHaveBeenCalled();
            });
        });
        it('logout button should call logout func', function() {
            runs(function() {
                spyOn(Menu, 'logout');
                $('#menu_logout').click();
            });
            runs(function() {
                expect(Menu.logout).toHaveBeenCalled();
            });
        });
        it('exit button should call exit func', function() {
            runs(function() {
                spyOn(Menu, 'exit');
                $('#menu_exit').click();
            });
            runs(function() {
                expect(Menu.exit).toHaveBeenCalled();
            });
        });
        it('registration button should call registration func', function() {
            runs(function() {
                spyOn(Menu, 'registration');
                $('#menu_registration').click();
            });
            runs(function() {
                expect(Menu.registration).toHaveBeenCalled();
            });
        });
    });
    describe('registered user menu showing', function() {
        beforeEach(function() {
            runs(function() {
                app.user.session = true;
                app.user.registered = true;
                Menu.clear();
                waits(1000);
            });
        });

        it('show on start page shold consist only exit button', function() {
            app.activePage = 'start_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('none');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('none');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        /*
         * For registered user register page is inaccessible
        it('show on registration_page shold consist only exit button', function() {
            app.activePage = 'registration_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_my').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('none');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('none');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        */
        it('show on login_page shold consist only exit button', function() {
            app.activePage = 'login_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('none');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('none');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on main_page shold exclude home button and registration', function() {
            app.activePage = 'main_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('block');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('block');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on recording_page shold exclude recording button', function() {
            app.activePage = 'recording_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('block');
            expect($('#menu_home').closest('div').css('display')).toEqual('block');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on myvoices_page shold exclude recording button', function() {
            app.activePage = 'myvoices_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('block');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('block');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });

    });
    describe('unregistered user menu showing', function() {
        beforeEach(function() {
            runs(function() {
                app.user.session = true;
                app.user.registered = false;
                Menu.clear();
                waits(1000);
            });
        });

        it('show on start page shold consist only exit button', function() {
            app.activePage = 'start_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('none');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('none');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on registration_page shold consist exit,home,logout,recording,settings buttons', function() {
            app.activePage = 'registration_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('block');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('block');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on login_page shold consist only exit button', function() {
            app.activePage = 'login_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('none');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('none');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('show on main_page shold exclude home button and my voices', function() {
            app.activePage = 'main_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('none');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('block');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('block');
        });
        it('recording page not reacheble for unregistered user so, it behave for test like registered', function() {
            app.activePage = 'recording_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('block');
            expect($('#menu_home').closest('div').css('display')).toEqual('block');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('none');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });
        it('myvoices page not reacheble for unregistered user so, it behave for test like registered', function() {
            app.activePage = 'myvoices_page';
            Menu.show();

            expect($('#menu_exit').closest('div').css('display')).toEqual('block');
            expect($('#menu_myvoices').closest('div').css('display')).toEqual('none');
            expect($('#menu_home').closest('div').css('display')).toEqual('block');
            expect($('#menu_logout').closest('div').css('display')).toEqual('block');
            expect($('#menu_recording').closest('div').css('display')).toEqual('block');
            expect($('#menu_settings').closest('div').css('display')).toEqual('block');
            expect($('#menu_registration').closest('div').css('display')).toEqual('none');
        });

    });
    describe('after initiate and showing', function() {
        beforeEach(function() {
            runs(function() {
                app.initialize(true);
                waits(3000);
            });
            runs(function() {
                if (!app.user.session) {
                    app.user.initSession('chekst@gmail.com','qwerty');
                    waits(3000);
                };
            });
            runs(function() {
                Menu.clear();
                Menu.show();
                waits(500);
            });
        });
        it('settings button should call settings func and put settings visible', function() {
            runs(function() {
                $('#menu_settings').click();
            });
            waits(1000);
            runs(function() {
                expect(Settings.getState()).toEqual('show');
            });
        });
    });
});
