package com.jokerjok;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;


public class VoiceRecorder extends CordovaPlugin {
	private MediaRecorder mRecorder = null;
	private static final String LOG_TAG = "VoiceRecorder";
	private String voiceStorageDir;	
	private static ArrayList<JSONObject> voicesList = new ArrayList();
	public static String system = null;
	public static String client_id = null;
	public static String client_secret = null;
	public static String login = null;
	public static String password = null;
	public static String app_account_id = null;
	
    @SuppressLint("NewApi")
	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    	voiceStorageDir = Environment.getExternalStoragePublicDirectory(
    			Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        if (action.equals("startRecording")) {
            this.startRecording(callbackContext, args);
        } else if (action.equals("stopRecording")) {
            this.stopRecording(callbackContext);
        } else if (action.equals("deleteAllRecordings")) {
            this.deleteAllRecordings(callbackContext);
        } else if (action.equals("saveRecording")) {
            this.saveRecording(callbackContext, args);
        } else if (action.equals("checkRecordStatus")) {
            this.checkRecordStatus(callbackContext, args);
        } else if (action.equals("removeRecord")) {
            this.removeRecord(callbackContext, args);
        } else if (action.equals("initSCToken")) {
            SoundCloudWrapper.initSCToken(args,callbackContext,cordova.getActivity().getApplicationContext());
        } else if (action.equals("downloadVoice")) {
            this.downloadVoice(callbackContext, args);
		}
        
        return false;
    }
            
    private void  createStatusFile(String fileName, String voiceJson) {
    	//Log.e(LOG_TAG, "createStatusFile start");
    	FileOutputStream fos;
		try {
			fos = cordova.getActivity().getApplicationContext().openFileOutput(fileName, cordova.getActivity().getApplicationContext().MODE_PRIVATE);
			fos.write(voiceJson.getBytes());
	    	fos.close();
	    	
			//String[] files = cordova.getActivity().getApplicationContext().fileList();
			//int size = files.length;
	    	//Log.e(LOG_TAG, "createStatusFile files " + size);
	    	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void deleteVoiceFiles(String fileName) {
    	//Log.e(LOG_TAG, "deleteVoiceFiles start " + fileName);
       	cordova.getActivity().getApplicationContext().deleteFile(fileName);
       	File file = new File(voiceStorageDir + "/" + fileName);
       	file.delete();
    }    
    
    private void startRecording(CallbackContext callbackContext, JSONArray args) {
    	//Log.e(LOG_TAG, "startRecording start");
    	boolean started = true;
        String fileName = "";
        String mFileName = "";
        String voiceJson = "";
    	int max = 0;
    	
		if (args.length() >= 1) {
			fileName = UUID.randomUUID().toString() + ".amr";			
			mFileName = voiceStorageDir + "/" + fileName;
	    	//Log.e(LOG_TAG, "startRecording " + fileName);
			mRecorder = new MediaRecorder();
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			mRecorder.setOutputFile(mFileName);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			started = true;
			try {
				mRecorder.prepare();
			} catch (IOException e) {
				started = false;
				Log.e(LOG_TAG, "prepare() failed");
			}
			try {
				JSONObject voiceJsonObj = new JSONObject(args.get(0).toString());
				voiceJsonObj.put("fileLocation",mFileName);
				voiceJsonObj.put("fileStatus",fileName);
				voiceJson = voiceJsonObj.toString();
				voicesList.add(voiceJsonObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        if(started){
        	mRecorder.start();
        	createStatusFile(fileName, voiceJson);
            callbackContext.success("{'voice':" + voiceJson + "}");
        } else {
            callbackContext.error("{'voice':{'status':" + String.valueOf(max) + ", 'fileLocation':'" + mFileName + "', 'fileStatus':'" + fileName + "'}}");
        }                        
    }
    
    private void saveRecording(CallbackContext callbackContext, JSONArray args) {
		try {
			JSONObject voiceJsonObj = new JSONObject(args.get(0).toString());
			/*
	    	try {
				Log.e(LOG_TAG, "saveRecording " + voiceJsonObj.getString("fileStatus"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			*/
			boolean found = false;
			for (int i = 0; i < voicesList.size(); i++) {
				if (voiceJsonObj.getString("fileStatus").equals(voicesList.get(i).getString("fileStatus"))) {
					found = true;
					break;
				}				
			}			
	    	if(found){
	        	//Log.e(LOG_TAG, "saveRecording 1");
		        SoundCloudWrapper.uploadFile(voiceJsonObj,callbackContext,cordova.getActivity().getApplicationContext());
	        } else {
	        	callbackContext.error("{'status':0}");
	        } 			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void checkRecordStatus(CallbackContext callbackContext, JSONArray args) {
		try {
			JSONObject voiceJsonObj = new JSONObject(args.get(0).toString());
			/*
	    	try {
				Log.e(LOG_TAG, "checkRecordStatus " + voiceJsonObj.getString("fileStatus"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			*/
			boolean found = false;
			for (int i = 0; i < voicesList.size(); i++) {
				if (voiceJsonObj.getString("fileStatus").equals(voicesList.get(i).getString("fileStatus"))) {
					voiceJsonObj = voicesList.get(i);
					found = true;
					break;
				}				
			}		
			
			
	    	if(found){
	    		String voiceJson = voiceJsonObj.toString();
				callbackContext.success("{'status':1,'voice':" + voiceJson + "}");
	        } else {
	        	callbackContext.error("{'status':0}");
	        } 			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void removeRecord(CallbackContext callbackContext, JSONArray args) {
    	//Log.e(LOG_TAG, "removeRecord start");
		try {
			JSONObject voiceJsonObj = new JSONObject(args.get(0).toString());			
			boolean found = false;
			for (int i = 0; i < voicesList.size(); i++) {
				if (voiceJsonObj.getString("fileStatus").equals(voicesList.get(i).getString("fileStatus"))) {
					voiceJsonObj = voicesList.get(i);
					deleteVoiceFiles(voiceJsonObj.getString("fileStatus"));
					found = true;
					break;
				}				
			}		
			
			
	    	if(found){
				callbackContext.success("{'status':1}");
	        } else {
	        	callbackContext.error("{'status':0}");
	        } 			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void stopRecording(CallbackContext callbackContext) {
    	//Log.e(LOG_TAG, "stopRecording");
        mRecorder.stop();
        mRecorder.release();
        //mRecorder = null;
        
        if (true) { 
        	callbackContext.success("{'status':1}");
        } else {
            callbackContext.error("{'status':0}");
        }        
    }        
       
    private void deleteAllRecordings(CallbackContext callbackContext) {
        String[] files = cordova.getActivity().getApplicationContext().fileList();
        int size = files.length;
        for (int i=0; i<size; i++)
        {
        	deleteVoiceFiles(files[i]);
        }        
        if (true) { 
        	callbackContext.success("{'status':1}");
        } else {
            callbackContext.error("{'status':0}");
        }        
    }
    
    public static void updateVoiceList(JSONObject voiceObj) {
    	/*
    	try {
			Log.e(LOG_TAG, "updateVoiceList " + voiceObj.getString("fileStatus"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		try {
			for (int i = 0; i < voicesList.size(); i++) {
				if (voiceObj.getString("fileStatus").equals(voicesList.get(i).getString("fileStatus"))) {
					voicesList.get(i).put("url", voiceObj.getString("url"));
					voicesList.get(i).put("link", voiceObj.getString("link"));
					break;
				}
			}		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
    }
         
    private void downloadVoice(CallbackContext callbackContext, JSONArray args) {
		try {
	    	Object[] voiceParams = new Object[3];
	    	voiceParams[0] = args.get(0).toString(); //dwnld url	    		
	    	voiceParams[1] = voiceStorageDir + "/" + UUID.randomUUID().toString() + ".amr"; //file path
	    	voiceParams[2] = args.get(1).toString(); //token
			
        	//Log.e(LOG_TAG, "downloadVoice " + mFileName);
		    SoundCloudWrapper.downloadVoice(callbackContext,cordova.getActivity().getApplicationContext(),voiceParams);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }    
}