package com.jokerjok;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.UUID;

import org.apache.cordova.api.CallbackContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Endpoints;
import com.soundcloud.api.Params;
import com.soundcloud.api.Request;
import com.soundcloud.api.Token;

import com.soundcloud.api.Http;

public class SoundCloudWrapper {
	private static final String LOG_TAG = "SoundCloudWrapper";
	private static Token token = null;
	private static Integer tokenExperingSeconds = null;
	private static Date tokenStartTime = null;
	private static ApiWrapper wrapper;
	private static JSONObject voiceObj;
	
	public static boolean checkToken() {
    	if (token != null) {
    		return true;	
		} else {
    		return false;	
		}		
	}
	
    public static void initSCToken(JSONArray args, CallbackContext callbackContext, Context ctx) {
    	//Log.e(LOG_TAG, "initSCToken");
    	if (tokenExperingSeconds == null) {
    		Resources r = ctx.getApplicationContext().getResources();
    		String environment = r.getString(R.string.environment);
    		if (environment.equals("staging")) {
    			tokenExperingSeconds = Integer.parseInt(r.getString(R.string.token_expiring_staging));
    		} else {
    			tokenExperingSeconds = Integer.parseInt(r.getString(R.string.token_expiring_staging));
    		}
		}
    	boolean argsOk = true;
    	Object[] params = new Object[5];
		params[0] = VoiceRecorder.client_id;
		params[1] = VoiceRecorder.client_secret;
		params[2] = VoiceRecorder.login;
		params[3] = VoiceRecorder.password;
		params[4] = ctx;
		
    	if(argsOk){
    		if (tokenStartTime != null) {
    			Date newTokenStartTime = new Date();
    			long diff = newTokenStartTime.getTime() - tokenStartTime.getTime();
    			long diffSeconds = diff / 1000 % 60;
    			if (diffSeconds > tokenExperingSeconds) {
                	new GetTokenTask().execute(params);
    			}
    		} else{
            	new GetTokenTask().execute(params);
    		}
    		callbackContext.success("{'status':1}");
        } else {
        	callbackContext.error("{'status':0}");
        } 
    }	
        
    //private static class GetTokenTask extends AsyncTask<String, Void, String> {
	private static class GetTokenTask extends AsyncTask<Object, Void, String> {
		//protected String doInBackground(String... params) {
	    protected String doInBackground(Object... params) {
			String result = "1";
			// final ApiWrapper
			wrapper = new ApiWrapper(params[0].toString() /* client_id */,
									 params[1].toString() /* client_secret */, 
									 null /* redirect URI */, 
									 null /* token */);
			try {
				//Esli zaprawivat' token sliwkom bystro to on mozhet ne uspet' prinyat' znacjenie i zapustitsya ewe raz
				token = wrapper.login(params[2].toString() /* login */, params[3].toString() /* password */);
				tokenStartTime = new Date();
	    		//Context ctx = (Context)params[4];
				//Log.e(LOG_TAG, "GetTokenTask token  " + token);
				//wrapper.toFile(WRAPPER_SER);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = "0";
			}
			return result;
	    }

	    protected void onPostExecute(String err) {
	    	if(err.equals("0")) {
	    		token = null;
	    		tokenStartTime = null;
	    	}
	    }
	}    

	private static class UploadFileTask extends AsyncTask<Object, Void, HttpResponse> {
	    protected HttpResponse doInBackground(Object... params) {
	    	/*
	    	try {
				Log.e(LOG_TAG, "UploadFileTask " + voiceObj.getString("fileStatus"));
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			*/	    	
	    	HttpResponse result=null;
	    	try {
	        	
	    		Context ctx = (Context)params[0];	    		
	    		File file = null;
	    		String title = "";
				try {					
					file = new File(voiceObj.getString("fileLocation"));
					title = voiceObj.getString("user_name") + " " + voiceObj.getString("duration");
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
	            if (!file.exists()) throw new IOException("The file `"+file+"` does not exist");
	            if (title == "") {
                    title = file.getName();
				}


				
	            //System.err.println("Uploading " + title);

	             
	            result = wrapper.post(Request.to(Endpoints.TRACKS)
	                        .add(Params.Track.TITLE, title)
	                        //.add(Params.Track.TAG_LIST, "demo upload")
	                        .add(Params.Track.DOWNLOADABLE, "1")	                        
	                        .withFile(Params.Track.ASSET_DATA, file)
	                        .setProgressListener(new Request.TransferProgressListener() {
	                            @Override public void transferred(long amount) {
	                                System.err.print(".");
	                            }
	                        }));

	            String json = Http.formatJSON(Http.getString(result));	            
	        	try {
					JSONObject jsonResponseObj = new JSONObject(json);
					voiceObj.put("url", jsonResponseObj.get("stream_url"));
					voiceObj.put("link", jsonResponseObj.get("permalink_url"));
					double bytes = file.length();
					double kilobytes = (bytes / 1024);
					double megabytes = (kilobytes / 1024);	            
					voiceObj.put("size", megabytes);
					FileOutputStream fos;
		        	ctx.deleteFile(voiceObj.getString("fileStatus"));
					fos = ctx.openFileOutput(voiceObj.getString("fileStatus"), ctx.MODE_PRIVATE);
	    			fos.write(voiceObj.toString().getBytes());
	    	    	fos.close();
	    	    	VoiceRecorder.updateVoiceList(voiceObj);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
	    		} catch (FileNotFoundException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		} catch (IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	    	} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return result;
	    }

	    protected void onPostExecute(HttpResponse resp) {
        }
	}		

    public static void uploadFile(JSONObject voiceJsonObj, CallbackContext callbackContext, Context ctx) {
    	boolean argsOk = true;
    	voiceObj = voiceJsonObj;
    	/*
    	try {
			Log.e(LOG_TAG, "uploadFile " + voiceJsonObj.getString("fileStatus"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
    	Object[] params = new Object[1];
		params[0] = ctx;	    		
    	if(argsOk){
        	new UploadFileTask().execute(params);
    		callbackContext.success("{'status':1}");
        } else {
        	callbackContext.error("{'status':0}");
        } 
    }	

	private static class downloadVoiceTask extends
			AsyncTask<Object, Void, HttpResponse> {
		protected HttpResponse doInBackground(Object... params) {

			try {
				//Download voice from SC
				Log.e(LOG_TAG, "downloadVoiceTask ");
				String mFileName = (String) params[1];
				URL url = new URL((String) params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();

				// download the file
				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(mFileName);

				byte data[] = new byte[1024];
				int count;
				while ((count = input.read(data)) != -1) {
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();
				Log.e(LOG_TAG, "downloadVoiceTask finished");
				
				
				/*
				//Get VK upload server
				Log.e(LOG_TAG, "downloadVoiceTask get VK server");				
	            String urlStr = "https://api.vk.com/method/audio.getUploadServer?&access_token=" + (String) params[2];
				url = new URL(urlStr);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
		 
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}
		 
				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
		 
				JSONObject uploadSerevrUrlRespJSON = new JSONObject(br.readLine());
				String uploadServerUrl = uploadSerevrUrlRespJSON.getJSONObject("response").getString("upload_url");
				conn.disconnect();				
				Log.e(LOG_TAG, "downloadVoiceTask get VK server finished");
				
				
				//upload voice to VK
				Log.e(LOG_TAG, "downloadVoiceTask upload audio to VK server");				
				String boundary;
				String LINE_FEED = "\r\n";
				HttpURLConnection httpConn;
				OutputStream outputStream;
   			    PrintWriter writer;				
			    String charset = "UTF-8";
		        File uploadFile = new File(mFileName);
		        
		        boundary = "===" + System.currentTimeMillis() + "===";
		         
		        url = new URL(uploadServerUrl);
		        httpConn = (HttpURLConnection) url.openConnection();
		        httpConn.setDoOutput(true); // indicates POST method
		        httpConn.setDoInput(true);
		        httpConn.setRequestProperty("Content-Type",
		                "multipart/form-data; boundary=" + boundary);
		        outputStream = httpConn.getOutputStream();
		        writer = new PrintWriter(new OutputStreamWriter(outputStream, charset),true);
		        
		        String fileName = uploadFile.getName();
		        writer.append("--" + boundary).append(LINE_FEED);
		        writer.append(
		                "Content-Disposition: form-data; name=\"" + "file"
		                        + "\"; filename=\"" + fileName + "\"")
		                .append(LINE_FEED);
		        writer.append(
		                "Content-Type: "
		                        + URLConnection.guessContentTypeFromName(fileName))
		                .append(LINE_FEED);
		        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
		        writer.append(LINE_FEED);
		        writer.flush();
		 
		        FileInputStream inputStream = new FileInputStream(uploadFile);
		        byte[] buffer = new byte[4096];
		        int bytesRead = -1;
		        while ((bytesRead = inputStream.read(buffer)) != -1) {
		            outputStream.write(buffer, 0, bytesRead);
		        }
		        outputStream.flush();
		        inputStream.close();
		         
		        writer.append(LINE_FEED);
		        writer.flush(); 	
		        
		        
		        
		        
		        writer.append(LINE_FEED).flush();
		        writer.append("--" + boundary + "--").append(LINE_FEED);
		        writer.close();
		 
		        // checks server's status code first
		        int status = httpConn.getResponseCode();
				String server;		            
				String audio;		            
				String hash;		            
		        if (status == HttpURLConnection.HTTP_OK) {
		            BufferedReader reader = new BufferedReader(new InputStreamReader(
		                    httpConn.getInputStream()));
		            String line = null;
		            String response123 = reader.readLine();
		            reader.close();
		            Log.e(LOG_TAG, response123);
		            JSONObject uploadVoiceRespJSON = new JSONObject(response123);
					server = uploadVoiceRespJSON.getString("server");		            
					audio = uploadVoiceRespJSON.getString("audio");		            
					hash = uploadVoiceRespJSON.getString("hash");		            
		            httpConn.disconnect();
		        } else {
		            throw new IOException("Server returned non-OK status: " + status);
		        }
		 		        
		        
				
				Log.e(LOG_TAG, "downloadVoiceTask upload audio to VK server finished");
				
				
				
				//save uploaded voice to VK
				Log.e(LOG_TAG, "downloadVoiceTask get VK save");				
	            urlStr = "https://api.vk.com/method/audio.save?&access_token=" + (String) params[2] +
	            		"&server=" + server + "&hash=" + hash + "&audio=" + audio;
				
				url = new URL(urlStr);
				conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
		 
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}
		 
				br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
				Log.e(LOG_TAG, br.readLine());
					
				//JSONObject uploadSerevrUrlRespJSON = new JSONObject(br.readLine());
				//String uploadServerUrl = uploadSerevrUrlRespJSON.getJSONObject("response").getString("upload_url");
				conn.disconnect();				
				Log.e(LOG_TAG, "downloadVoiceTask get VK save finished");
				*/
				
				
			} catch (Exception e) {
			}
			return null;
		}

		protected void onPostExecute(HttpResponse resp) {
			Log.e(LOG_TAG, "downloadVoiceTask onPostExecute");
		}
	}		
        
    public static void downloadVoice(CallbackContext callbackContext, Context ctx, Object... voiceParams) {
       	new downloadVoiceTask().execute(voiceParams);
   		callbackContext.success("{'status':1,'fileName':'" + voiceParams[1] + "'}");
    }    
}